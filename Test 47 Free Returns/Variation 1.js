function showModal () {
    var html = "<div class='opt-overlay'></div><div class='opt-modal-wrap'><div class='opt-modal'> <div class='opt-close'>X</div><div class='opt-modal-content'> <h1>RETURNS</h1> <div class=\"returns-container\"> <p>Things not quite right? All good!&nbsp;We know that sometimes an item may not fit or be right.</p><p>If you're not 100% satisfied with your purchase, you can return your item(s) in-store or online within 30 days of the order receipt and we will be happy to provide you with an exchange, store credit or refund.</p><div class=\"widget block block-static-block\"> <style type=\"text/css\">.returns-container-account img{max-width:45px}.returns-container-account .center{text-align:center}.returns-container-account h3{margin:20px 0 20px 15px}.grey-bg{background-color:#f9f9f9;padding:4% 2% 2% 2%;margin:1%;width:31%;min-height:270px}@media screen and (max-width:686px){.grey-bg{background-color:#f9f9f9;padding:4% 2% 2% 2%;margin:2% 1%;width:96%}}</style> <div class=\"row\" style=\"margin-bottom: 30px;\"> <div class=\"returns-container-account\"> <h3>3 SIMPLE CONDITIONS FOR A SUCCESSFUL RETURN</h3> <div class=\"center\"> <div class=\"column large-4 small-6 medium-6 grey-bg\"> <img style=\"margin-bottom: 15px\" src=\"/media/wysiwyg/icons/x1488795514_number-one.png.pagespeed.ic.X0q6mijgaJ.png\"> <p><strong>RETURNED WITHIN 30 DAYS OF PURCHASE</strong></p><p>Item(s) were purchased in the last 30 days and proof of purchase can be provided.</p></div><div class=\"column large-4 small-6 medium-6 grey-bg\"> <img style=\"margin-bottom: 15px\" src=\"/media/wysiwyg/icons/x1488795518_number-two.png.pagespeed.ic.E0WdoKW2wu.png\"> <p><strong>ITEM(S) ARE UNWORN &amp; UNDAMAGED</strong></p><p>Item(s) must be unworn, unwashed, or otherwise undamaged with original tags attached</p></div><div class=\"column large-4 small-6 medium-6 grey-bg\"> <img style=\"margin-bottom: 15px\" src=\"/media/wysiwyg/icons/x1488795523_number-three.png.pagespeed.ic.3neMWOKekU.png\"> <p><strong>IN ORIGINAL PACKAGING AND CONDITION</strong></p><p>Item(s) are in original packaging or shoe box in their original condition</p></div></div></div></div></div></div></div></div></div>";

    jQuery('body').append(html);
    jQuery('.opt-close,.opt-overlay').click(function () {
        jQuery('.opt-overlay, .opt-modal-wrap').remove();
    });
    jQuery('.opt-modal .row').after('<button id="optReadMore" class="action tocart primary">READ MORE</button>');
    jQuery('#optReadMore').click(function (e) {
        e.preventDefault();
        window.location = "https://www.platypusshoes.com.au/returns/";
    });
}

function showTitle () {
  var message = '30 DAY RETURNS ONLINE & IN-STORE';
    
  jQuery('.top-container .list').append('<li class="large-12 medium-12 small-12 optSplit"><img src="//placehold.it/16x14">'+message+'</li>');
  jQuery('.top-container .list li').removeClass('large-12 medium-12 small-12').addClass('large-6 medium-6 small-6');

  if (jQuery('.top-container ul li').length == 1) {
      jQuery('.top-container ul li').addClass('optCenter');
  }

  if (jQuery('.header.trust').length >= 0) {
      jQuery('.header.trust').append('<span class="optUnder"><img src="//placehold.it/16x14">'+message+'</span>');
  }

  jQuery('.optSplit, .optUnder').click(function () {
      showModal();
  });

}

function showUnder () {
    var html = ' <div class="optContainer"> <div class="optTop"> <img src="//placehold.it/28x24"> <p>Simple Returns</p></div><div class="optBottom"> <p>30 DAY RETURNS ONLINE & IN-STORE</p></div></div>';

    jQuery('.product-add-form').after(html);
    jQuery('.optContainer').click(function () {
        showModal();
    });
}  

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

function start() {

    if (jQuery('body').hasClass('catalog-product-view')) {
        defer(showUnder, '.product-add-form');
    }

    showTitle();

}

defer(start, 'body');