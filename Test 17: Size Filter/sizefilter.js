function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector) }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector) }, 50);
    }    
}

jQuery.fn.ck_replace = function(val1, val2){
    var text = jQuery(this).text().split(val1);
    if(text.length > 1){
        text = text.join(val2);
    } else {
        text = text[0];
    }
    jQuery(this).text(text);
}

//replace val1 with val2, val1 can be a string array.
jQuery.fn.ck_replaceAll = function(val1, val2){
    var text = $(this).text();
    if(val1.length > 1){
        for(var i = 0; i < val1.length; i++){
            $(this).ck_replace(val1[i], val2);
        }
    } else {
        $(this).ck_replace(val1, val2);
    }
};

var midCategoryCull = [
    "Footwear"
];

//stores filter data
var filterData = [];

function parseMidCategory(){
    jQuery(".over-levels.level-1 a").each(function(){
        jQuery(this).ck_replaceAll(midCategoryCull, "");
    });
}

function apply(){
    jQuery(".js-apply-filter").tap();
    checkForLoading();
}

function syncFilter(){
    jQuery(".opt-container .over-levels.level-0 a").each(function(index){
        jQuery(this).attr("class", jQuery("#narrow-by-list .over-levels.level-0 a:eq("+index+")").attr("class"));
        jQuery(this).attr("style", jQuery("#narrow-by-list .over-levels.level-0 a:eq("+index+")").attr("style"));
    });
    
    jQuery(".opt-container .over-levels.level-1 a").each(function(index){
        jQuery(this).attr("class", jQuery("#narrow-by-list .over-levels.level-1 a:eq("+index+")").attr("class"));
        jQuery(this).attr("style", jQuery("#narrow-by-list .over-levels.level-1 a:eq("+index+")").attr("style"));
    });

    jQuery(".opt-container .size-filter-tabs > div").each(function(index){
        jQuery(this).attr("class", jQuery("#narrow-by-list .size-filter-tabs > div:eq("+index+")").attr("class"));
        jQuery(this).attr("style", jQuery("#narrow-by-list .size-filter-tabs > div:eq("+index+")").attr("style"));
        jQuery(this).attr("aria-hidden", jQuery("#narrow-by-list .size-filter-tabs > div:eq("+index+")").attr("aria-hidden"));
    });

    jQuery(".opt-container .size-filter-tabs a").each(function(index){
        jQuery(this).attr("class", jQuery("#narrow-by-list .size-filter-tabs  a:eq("+index+")").attr("class"));
        jQuery(this).attr("style", jQuery("#narrow-by-list .size-filter-tabs  a:eq("+index+")").attr("style"));
    });
}

function bindClicks(bindi, binder){
    $(bindi).click(function(){
        console.log("click relayed");
        $(binder).click();
        syncFilter();
    })
}

function bindTap(bindi, binder){
    $(bindi).click(function(){
        console.log("click relayed", binder);
        $(binder).tap();
        apply();
    })
}

function checkForLoading(){
    jQuery("body").addClass("opt-loading");
    if(jQuery(".loading-mask").css("display") != "none"){
        //still loading
        setTimeout(function(){
            checkForLoading();
        }, 100);
    } else {
        jQuery("body").removeClass("opt-loading");
        console.log("loaded");
        setTimeout(function(){
            buildFilter();
        },200);
    }
}

function buildFilter(){
    jQuery(".opt-container").remove();
    parseMidCategory();
    jQuery("#amasty-shopby-product-list").before('<div class="opt-container"></div>');
    jQuery(".opt-container").append('<div class="opt-top-container"></div>');
    jQuery(".opt-container").append('<div class="opt-bottom-container"></div>');

    jQuery(".opt-top-container").append('<div class="opt-desc"></div>');
    jQuery(".opt-top-container").append('<div class="opt-swatch-container"></div>');

    jQuery(".opt-desc").append('<span>Size:</span>');
    jQuery(".opt-desc").append(' <div id="opt-filter-desc "><span></span><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" class="svg-icon svg-icon-arrow-up"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-arrow-up"></use></svg></span></div>');

    jQuery(".opt-bottom-container").append('<div class="opt-top-cat"></div>');
    jQuery(".opt-bottom-container").append('<div class="opt-mid-cat"></div>');

    jQuery(".opt-top-cat").append(jQuery(".js-filters-size .over-levels.level-0:eq(0)").clone());
    jQuery(".opt-mid-cat").append(jQuery(".js-filters-size .over-levels.level-1:eq(0)").clone());
    jQuery(".opt-bottom-container").append(jQuery(".js-filters-size .size-filter-tabs:eq(0)").clone());
    syncFilter();
    addFilterCallbacks();
}

function addFilterCallbacks(){
    jQuery(".opt-container .over-levels.level-0 a").each(function(index){
        bindClicks(jQuery(this), jQuery("#narrow-by-list .over-levels.level-0 a:eq("+index+")"))
    });
    jQuery(".opt-container .over-levels.level-1 a").each(function(index){
        bindClicks(jQuery(this), jQuery("#narrow-by-list .over-levels.level-1 a:eq("+index+")"))
    });
    jQuery(".opt-container .size-filter-tabs a").each(function(index){
        bindTap(jQuery(this), jQuery("#narrow-by-list .size-filter-tabs a:eq("+index+")"))
    });
}

function start(){
    
    buildFilter();
}

defer(function(){
    jQuery("body").addClass("opt-17");
    start();
}, ".product.size");