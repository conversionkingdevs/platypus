function defer(method, selector) {
    if (window.jQuery && window.checkoutConfig) {
        if (jQuery(selector).length > 0) {
            method();
        } else {
            setTimeout(function () {
                defer(method, selector);
            }, 50);
        }
    } else {
        setTimeout(function () {
            defer(method, selector);
        }, 50);
    }
}
function elementDefer(method, arrayOfSelectors) {
    var valid = 0;
    for (var i = 0; i < arrayOfSelectors.length; i++) {
        if (jQuery(arrayOfSelectors[i]).length > 0) {
            valid++;
        }
    }
    if (valid == arrayOfSelectors.length) {
        method();
    } else {
        setTimeout(function () {
            elementDefer(method, arrayOfSelectors);
        }, 50);
    }
}


function afterDefer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length == 0) {
            method();
        } else {
            setTimeout(function () {
                afterDefer(method, selector);
            }, 50);
        }
    } else {
        setTimeout(function () {
            afterDefer(method, selector);
        }, 50);
    }
}

function watcher(loadingIdentifier, receivingcallback) {
    (function (open) {
        XMLHttpRequest.prototype.open = function (m, u, a, us, p) {
            this.addEventListener('readystatechange', function () {
                try {
                    if (this.responseURL.indexOf(loadingIdentifier) >= 0 && this.readyState == 4) {
                        receivingcallback();
                    }
                } catch (e) {}
            }, false);
            open.call(this, m, u, a, us, p);
        };
    })(XMLHttpRequest.prototype.open);
}

function updateHeader() {
    if (jQuery('.opt-back-link').length <= 0) {  
        jQuery('.header.content').prepend('<div class="opt-back-link"><a href="/checkout/cart/"><span class="opt-tablethide">CONTINUE SHOPPING</span><span class="opt-tabletshow">Back</span></a></div>');
    }
}

function addEditItems(container) {
    jQuery(container + ' .opt-form-title').append('<div class="opt-Edit-Button"><p class="opt-a-Edit">Edit</p></div>');
    jQuery(container + ' .opt-a-Edit').click(function () {
        if (jQuery('body').hasClass('opt-loggedin') && !jQuery('body').hasClass('opt-loggedin-delivery')) {
            document.location = document.location.origin + '/customer/account/';
        } else {
            jQuery('.opt-initial').removeClass('opt-initial');
            showHideAreas(jQuery(this).closest('.opt-step-container').attr('id'));
        }
    });
}
function deliveryFunctionality() {
    jQuery('.shipping-address-items').on('click', '.shipping-address-item:not(.opt-added-click)', function (e) {
        e.stopPropagation();
        jQuery(this).find('button.action-select-shipping-item').triggerHandler('click');
    }).addClass('opt-added-click');
}

function modal() {
    elementDefer(function () {
        jQuery("body").append("<div class='opt-overlay' style='display: none;'><div class='opt-modal'><div class='opt-modal-header'><div class='opt-modal-head'></div><span>This will take you back to your cart.</span></div><div class='opt-modal-buttons'><button class='btn btn-primary opt-stay-here'>STAY HERE</button><button href='/checkout/cart/' class='btn btn-primary opt-modal-back-to-cart'>BACK TO CART</button></div></div></div></div>");

        jQuery('.opt-modal-back-to-cart').click(function () {
            document.location = document.location.origin + jQuery(this).attr('href');
        });

        jQuery(".opt-modal").append("<div class='opt-close'></div> ");

        jQuery(".opt-modal").click(function (event) {
            event.stopPropagation();
        });

        jQuery('body').on('click', 'div.link > a.link[title="Edit"], .opt-sidebar-header .opt-title a.link, #opt-DeliveryMethod-Edit', function (e) {
            e.preventDefault();
            jQuery(".opt-overlay").fadeIn("slow");
            jQuery("body").css({
                "height": "100%",
                "overflow": "hidden"
            });
        });

        jQuery(".opt-close, .opt-overlay, .opt-stay-here").click(function () {
            jQuery(".opt-overlay").fadeOut("slow");
            jQuery("body").css({
                "height": "auto",
                "overflow": "inherit"
            });
        });
    }, [
        '#co-transparent-form-braintree #payment_form_braintree'
    ]);
}

function createLoginswitch() {
    elementDefer(function () {
        jQuery('#opt-account').append('<div class="opt-form-toggle"><p>Already have an account? <span class="opt-underline opt-link-text">Sign in here</span></p></div>');
        jQuery('.opt-form-toggle').click(function () {
            if (jQuery('body').hasClass('opt-showloginstate')) {
                jQuery('body').removeClass('opt-showloginstate');
                jQuery('.opt-form-toggle').html('<p>Already have an account? <span class="opt-underline opt-link-text">Sign in here</span></p>');
            } else {
                jQuery('body').addClass('opt-showloginstate');
                jQuery('.opt-form-toggle').html('<p><span class="opt-underline opt-link-text">Return to Guest Checkout</span></p>');
            }
        });
    }, [
        "#opt-account"
    ]);
}

function addTitle(titlenum, titlename) {
    return '<div class="opt-form-title"><div class="opt-step-number"><span>' + titlenum + '</span></div><strong class="opt-title"><span>' + titlename + '</span></strong></div>';
}

function addSignin() {
    elementDefer(function () {
        jQuery('#customer-email-fieldset').before('<div class="opt-signin-cont"><p>Continue as a guest below or <span><a href="opt-signin">Sign In</a></span></div>');
    }, '#customer-email-fieldset');
}

function deliveryingToCompany() {
    elementDefer(function () {
        jQuery('.field[name="shippingAddress.company"]').toggleClass('opt-hide');
        var deliverying = '<div class="opt-delivering"><span>Delivering to a company? </span><a id="opt-companyDelivery" href="#" class="opt-underline">Click here</a></div>';
        jQuery('.field[name="shippingAddress.company"]').before(deliverying);
        jQuery('#opt-companyDelivery').click(function (e) {
            e.preventDefault();
            jQuery('.field[name="shippingAddress.company"]').toggleClass('opt-hide');
        });
    }, [
        '.field[name="shippingAddress.company"]'
    ]);
}

/** Update */
function updateShipping() {
    jQuery(".opt-quantity-container").children().remove();
    jQuery(".opt-quantity-container").append(jQuery(".table-totals").clone());
}


/** Getters */
function getBag() {
    var data = [];
    jQuery(".details").each(function () {
        var product = {
            "name": jQuery(this).find(".name").text(),
            "colour": jQuery(this).find(".values").first().text(),
            "size": jQuery(this).find(".values").last().text(),
            "qty": jQuery(this).find(".value").first().text(),
            "delivery": jQuery(this).find(".value").last().text(),
            "price": jQuery(this).find(".price").text(),
            "img": jQuery(this).prev().attr("src")
        }
        data.push(product);
    });
    return data;
}

function getLoginErrors() {
    if (jQuery("#customer-email.mage-error").length > 0) {
        return true;
    } else {
        return false;
    }
}

function getShippingErrors() {
    if (jQuery("#shipping .field._error, .message-error").length > 0) {
        return true;
    } else {
        return false;
    }
}

/** Adders */
//add after login
function addPayment() {
    jQuery(".opt-login-continue").remove();
    jQuery(".opt-edit").remove();

    jQuery("#co-payment-form .secure-pay-wrapper").remove();

    jQuery("#co-payment-form > fieldset").append(jQuery(".discount-code"));
    jQuery("#co-payment-form > fieldset").append(jQuery("#giftcardaccount-placer"));


    jQuery("#shipping").before('<div class="opt-edit"><span>Edit Details</span></div>');

    jQuery(".opt-edit").click(function () {
        window.location.href = "#shipping";
        runShipping();
    });

    jQuery("#co-payment-form").append(jQuery(".secure-pay-wrapper").clone());

    jQuery("#payment .secure-pay-wrapper .trust-block img").wrapAll('<div></div>');
    jQuery("#payment .secure-pay-wrapper .trust-block p").wrapAll('<div></div>');
    jQuery("#payment .secure-pay-wrapper .trust-block").prepend(jQuery("#payment .secure-pay-wrapper .trust-block strong"));
    jQuery("img[src='https://www.paypalobjects.com/webstatic/en_US/i/buttons/pp-acceptance-medium.png']").attr("src", "//cdn.optimizely.com/img/6092490016/b2a2c5dba9a4484d84369fb7d97f334e.png");

    jQuery("body").click(function () {
        jQuery('.opt-1 #checkout #checkoutSteps').css("max-height", (jQuery("#payment").height() + 20) + "px");
    });

    jQuery("#co-payment-form > fieldset").append(jQuery(".opt-giftcard-promo-dropdown").children());
    jQuery(".opt-giftcard-promo-container").remove();
    jQuery("#co-payment-form .secure-pay-wrapper").before('<div class="opt-giftcard-promo-container"></div>');
    jQuery(".opt-giftcard-promo-container").append('<div class="opt-dropdown-title"><span>APPLY GIFT CARDS AND PROMO CODES</span></div>');
    jQuery("#co-payment-form > fieldset .discount-code:eq(0), #co-payment-form > fieldset .giftcardaccount:eq(0)").wrapAll('<div class="opt-giftcard-promo-dropdown"></div>');
    jQuery(".opt-giftcard-promo-container").append(jQuery(".opt-giftcard-promo-dropdown"));

    jQuery(".opt-dropdown-title").click(function () {
        jQuery(this).parent().toggleClass("opt-active");
        var interval = setInterval(function () {
            jQuery('.opt-1 #checkout #checkoutSteps').css("max-height", (jQuery("#payment").height() + 20) + "px");
        }, 100);
        setTimeout(function () {
            clearInterval(interval);
        }, 800);
    });

    jQuery('.opt-1 #checkout #checkoutSteps').css("max-height", (jQuery("#payment").height() + 20) + "px");
    jQuery('.secure-pay-wrapper:eq(0)').appendTo(jQuery('.opt-bag-container'));
}

function addShoppingLogin() {
    jQuery("body").addClass("opt1-login");
}

function addPaymentLogin() {
    jQuery("body").addClass("opt1-login");
}

//add after login
function addShipping() {
    //phone help
    jQuery(".opt-phone-message").remove();
    jQuery(".opt-edit").remove();

    jQuery(".opt-1 #co-shipping-form").append('<small class="opt-phone-message">Pssst, drop the "0" from the beginning of your mobile number. For example, if your phone number is 0401 123 456, enter 410 123 456</small>');
    jQuery("body").click(function () {
        jQuery('.opt-1 #checkout #checkoutSteps').css("max-height", (jQuery("#payment").height() + 20) + "px");
    });
    jQuery('.opt-1 #checkout #checkoutSteps').css("max-height", (jQuery("#payment").height() + 20) + "px");

    jQuery("label[for='checkout-newsletter-signup']").html('<span>Sign up to receive awesome updates and promotions</span>');

    //add button
    jQuery("button.login").click(function () {
        localStorage.setItem("checkoutlogin", true);
    });

}

//add after login
function addBag() {
    jQuery(".opt-bag-container").remove();
    jQuery(".opt-payment-continue").remove();
    jQuery("#checkout .opc-wrapper").append('<div class="opt-bag-container"></div>');
    //add baggage details
    var bag = getBag();
    for (var i = 0; i < bag.length; i++) {
        jQuery(".opt-bag-container").append('<div class="opt-bag-item" data-value="' + i + '"></div>');
        jQuery(".opt-bag-item[data-value='" + i + "']").append('<div class="opt-img-container"><img src="' + bag[i].img + '" alt="product image"></div>');
        jQuery(".opt-bag-item[data-value='" + i + "']").append('<div class="opt-details-container"></div>');
        jQuery(".opt-bag-item[data-value='" + i + "'] .opt-details-container").append('<div class="opt-top"><span><strong>' + bag[i].name + '</strong></span></div>');
        jQuery(".opt-bag-item[data-value='" + i + "'] .opt-details-container").append('<div class="opt-color"><span><strong>Colour:</strong></span><span>' + bag[i].colour + '</span></div>');
        jQuery(".opt-bag-item[data-value='" + i + "'] .opt-details-container").append('<div class="opt-size"><span><strong>Size:</strong></span><span>' + bag[i].size + '</span></div>');
        jQuery(".opt-bag-item[data-value='" + i + "'] .opt-details-container").append('<div class="opt-qty"><span><strong>QTY:</strong></span><span>' + bag[i].qty + '</span></div>');
        jQuery(".opt-bag-item[data-value='" + i + "'] .opt-details-container").append('<div class="opt-price"><span><strong>Price:</strong></span><span>' + bag[i].price + '</span></div>');
        jQuery(".opt-bag-item[data-value='" + i + "'] .opt-details-container").append('<div class="opt-delivery"><span><strong>Delivery Method:</strong></span><span>' + bag[i].delivery + '</span></div>');
    }

    //add quantity
    jQuery(".opt-bag-container").append('<div class="opt-quantity-container"></div>');
    jQuery(".opt-quantity-container").append(jQuery(".table-totals").clone());

    jQuery(".opt-bag-container").append('<div class="opt-payment-continue"><div><span>Place Order</span></div></div>');
    jQuery(".opt-payment-continue").click(function () {
        jQuery('.place-order-container button').click()
    });
}

//add on start
function addLogin() {
    defer(function () {
        jQuery(".opt-login-continue").remove();
        jQuery('.or-separator').remove();
        jQuery("#co-shipping-form").before('<div class="opt-login-continue"><span>Continue</span></div>');
        jQuery("#customer-email").after('<div for="customer-email" generated="true" class="mage-error" id="customer-email-error" style="display: none;">Please enter a valid email address (Ex: johnsmith@domain.com).</div>');
        jQuery(".opt-login-continue").click(function () {
            if (!getLoginErrors() && jQuery("#customer-email").val() != "") {
                jQuery(".form-login").addClass("opt-guest");
                runShipping();
            } else {
                jQuery("#customer-email").addClass("mage-error");
                jQuery("#customer-email-error").show();
            }
        });
    }, "#co-shipping-form");
}

//add on start
function addBreadCrumb() {
    jQuery(".opt-breadcrumb").remove();
    jQuery("#checkout").prepend('<div class="opt-breadcrumb"></div>');
    jQuery(".opt-breadcrumb").append('<div class="opt-item"><div class="opt-icon"><img src="//cdn.optimizely.com/img/6092490016/f33c853356c24f5b8113ffa3eea9b4ad.png" alt=""/></div><span>Shipping<span class="opt-tabletshow"> & Payment</span></span></div>');
    jQuery(".opt-breadcrumb").append('<div class="opt-item opt-tablethide"><div class="opt-icon"><img src="//cdn.optimizely.com/img/6092490016/50beb4f44971422fba21f03179c2a398.png" alt=""/></div><span>Payment</span></div>');
    jQuery(".opt-breadcrumb").append('<div class="opt-item"><div class="opt-icon"><img src="//cdn.optimizely.com/img/6092490016/36b8b024ac224fb9979887135aa3b256.png" alt=""/></div><span>Your Bag</span></div>');
}

/** State Changers */
//run after hitting shipping continue button
function runPayment() {

    jQuery("#checkout").attr("data-state", 2);
    addBag();
    addPayment();
    if (jQuery("body").hasClass("logged-user")) {
        addPaymentLogin();
    }
    addBreadCrumb();
}

//run after login
function runShipping() {
    jQuery("#checkout").attr("data-state", 1);
    addBag();
    addPayment();
    addShipping();
    if (jQuery("body").hasClass("logged-user")) {
        addShoppingLogin();
    }
    addBreadCrumb();
}

//run on start
function runLogin() {
    jQuery("#checkout").attr("data-state", 0);
    addLogin();
    addBreadCrumb();
}

// Functions from one pages

function addEdit(selector) {
    jQuery(selector).after('<a href="/checkout/cart/" class="link _right">Edit Cart</a>');
}


function updateNewsletter() {
    elementDefer(function () {
        var news = '<span>  Keep me updated with the latest Platypus news. </span>';
        jQuery('.checkout-container .newsletter-signup .label[for="checkout-newsletter-signup"]').html(news);
    }, [
        '.checkout-container .newsletter-signup .label[for="checkout-newsletter-signup"]'
    ]);
}

function updateLoginOrder() {
    jQuery('#opt-account > .form.form-login').insertBefore(jQuery('#opt-account .or-separator'));
    jQuery('#opt-account > .facebook.button').insertAfter(jQuery('#opt-account .or-separator'));

}

function deliveryMethodHide() {
    var optdelivery = {};
    jQuery('.opt-delivery-line').addClass('opt-hide');
    for (var x = 0; x < jQuery('.checkout-summary:eq(0) .product-item .delivery-info').length; x++) {
        var item = jQuery('.checkout-summary .product-item .delivery-info:eq(' + x + ')').find('.method > span:eq(1)'),
            qty = jQuery('.checkout-summary .product-item:eq(' + x + ') .options-list > .value').text();
            console.log(item.text().indexOf('Standard'));
        if (item.text().indexOf('Standard') >= 0) {
            jQuery('#opt-method-standard').removeClass('opt-hide');
        }
        if (item.text().indexOf('Next') >= 0) {
            jQuery('#opt-method-Next').removeClass('opt-hide');
        }
        if (item.text().indexOf('Click') >= 0) {
            jQuery('#opt-method-click').removeClass('opt-hide');
        }
    }
}

function initialstateshowemail(email) {
    if (jQuery('.opt-initialemail').length <= 0) {
        jQuery('.opt-left').prepend('<div class="opt-initialemail"><div class="opt-email-cont"><p>Welcome back</p><strong><span>' + jQuery('#customer-email').val() + '</span></strong></div><div class="opt-email-signout"></div></div>');
    }

    if (email == true) {
        console.log(email);
        jQuery('.opt-initialemail span').text(jQuery('#customer-email').val());
    } else if (email == undefined) {
        jQuery('#opt-account .opt-form-title').addClass('opt-Tick');
    } else if (email && email.length > 0) {
        jQuery('.opt-initialemail span').text(email);
    } else {
        jQuery('.opt-initialemail span').text(jQuery('#customer-email').val());
    }
}

function addCarefuldontlose (selector, clickselector, showid, type) {
    var html = '<div class="opt-carefulcontainer" id="'+showid+'" style="display: none;"><div class="opt-topcontainer"><img src="//placehold.it/25x25" /><p><strong>Careful! Don\'t lose your pair.</strong></p></div><div class="opt-bottom"><p>To update your delivery method you will be redirected to the cart. <a href="/checkout/cart/">Change Delivery Method</a></p></div></div>';
    switch (type) {
        case "append":
            jQuery(selector).append(html);
            break;
        case "before":
            jQuery(selector).before(html);
            break;
    }
    
    jQuery('body').on('click', clickselector, function (e) {
        e.preventDefault();
        jQuery('#'+showid).toggle();
    });
}


function createDeliveryMethod() {
    jQuery('#opc-delivery_notes').prepend('<div class="opt-DeliveryMethod-Cont"><div class="opt-delivery-container"></div></div>');
    jQuery('.opt-DeliveryMethod-Cont').prepend('<div class="opt-heading opt-title"><span class="opt-title">Delivery Method</span><span id="opt-DeliveryMethod-Edit">Edit</span></div>');
    elementDefer(function () {
        jQuery('.opt-DeliveryMethod-Cont .opt-delivery-container').append('<div class="opt-delivery-line" id="opt-method-standard"><img src="//placehold.it/41x41" ><p><strong>Free</strong> Standard Shipping</p></div><div class="opt-delivery-line" id="opt-method-Next"><img src="//placehold.it/41x41"><p><strong>Next Day</strong> Shipping</p></div><div class="opt-delivery-line" id="opt-method-click"><img src="//placehold.it/41x41"><p><strong>Free</strong> Click and Collect</p></div>');
        deliveryMethodHide();
        addCarefuldontlose('.opt-DeliveryMethod-Cont', '.opt-DeliveryMethod-Cont #opt-DeliveryMethod-Edit', 'opt-deliverymethod-show', "append");
    }, [
        '.checkout-summary .product-item .delivery-info'
    ]);
}

function setupYourDetailsCandC(email) {
    elementDefer(function () {
        console.log('>>>> Running setupdetails()');
        jQuery('#checkout-step-shipping > .newsletter-signup').wrapAll('<div class="opt-step-container" id="opt-account"></div>');
        jQuery('#opt-account').prepend(addTitle('1', 'YOUR DETAILS')); // Add step 1 Your Details area
        addEditItems('#opt-account'); // Add Edit Items Link
        addSignin(); // Add signin area
        updateNewsletter();
        createLoginswitch();
    }, [
        '#checkout-step-shipping > .newsletter-signup'
    ]);
}

function updatePromoPlaceholds(selector, name) {
    jQuery(selector).attr('placeholder', name);
}

function addCCVTooltip() {
    jQuery('.payment-method.payment-method-braintree .payment-method-content .checkout-agreements-block').after('<div class="opt-ccvtooltip"><span>What is my CCV?</span></div>');
    jQuery('.opt-ccvtooltip').click(function () {
        jQuery('.cvv .field-tooltip .field-tooltip-content').toggleClass('_active').toggle();
    });
}

function setupPromo() {
    jQuery('.opt-giftcard-promo-container').prependTo(jQuery('#co-payment-form'));
    jQuery('#co-payment-form').prepend(addTitle('1', 'Billing Address'));
    jQuery('#co-payment-form > .fieldset').prepend(addTitle('1', 'Select Payment Method'));
    jQuery('.opt-1 .opt-dropdown-title span').text(jQuery('.opt-1 .opt-dropdown-title span').text().toLowerCase());
    jQuery('.opt-1 .checkout-payment-method .discount-code').insertAfter(jQuery('.checkout-payment-method .giftcardaccount'));
    jQuery('.opt-1 .checkout-payment-method .giftcardaccount .actions-toolbar .action.action-check').text('View Balance');
    if (jQuery('#billing-address-same-as-shipping-braintree').prop('checked') != true) {
        jQuery('#billing-address-same-as-shipping-braintree').click()
    }
        updatePromoPlaceholds('#giftcard-code', 'Gift Card Number');
        updatePromoPlaceholds('#giftcard-pin', 'Pin');
        updatePromoPlaceholds('#discount-code', 'Promo Code');

        jQuery('.opt-1 .checkout-payment-method .discount-code .payment-option-inner .detail-label span').text('Apply Promo Code');
        addCCVTooltip();
        jQuery('#giftcard-form > div.actions-toolbar.large-6.small-12._right > div.primary._right').appendTo(jQuery('#giftcard-form > div.payment-option-inner.large-6.small-12._left'));
}

function setupOrderSummary() {
    jQuery('.opt-bag-container').prepend(addTitle('1', 'Order Summary'));
    jQuery('.opt-bag-container .opt-form-title').after('<span id="opt-DeliveryMethod-Edit">Edit</span>');
    addCarefuldontlose('.opt-bag-container .opt-bag-item:eq(0)', '.opt-bag-container #opt-DeliveryMethod-Edit', 'opt-bag-show', "before");
}

function detectCandC() {
    var data = checkoutConfig.quoteItemData,
        CandCOnly = true;
    for (var i = 0; i < data.length; i++) {
        if (data[i].shipping_method != 'collect_collect') {
            CandCOnly = false;
            return CandCOnly;
        }
    }
    return CandCOnly;
}

function detectStep() {
    var step = 'none';
    jQuery('#checkoutSteps').find(' > li').each(function () {
        if (jQuery(this).css('display') != 'none') {
            step = jQuery(this).attr('id');
        }
    });
    return step;
}

function setupYourDetails() {
    elementDefer(function () {
        console.log('>>>> Running setupdetails()');
        jQuery('#checkout-step-shipping > button, #checkout-step-shipping .form-login, .or-separator').wrapAll('<div class="opt-step-container" id="opt-account"></div>');
        jQuery('#opt-account').before(addTitle('1', 'YOUR DETAILS')); // Add step 1 Your Details area
        elementDefer(function () {
            jQuery('#opc-delivery_notes').prepend(addTitle('1', 'Authority to Leave')); // Add step 1 Your Details area
            jQuery('#shipping-new-address-form').prepend(addTitle('1', 'Deliver To')); // Add step 1 Your Detail
        }, [
            "#opc-delivery_notes",
            "#shipping-new-address-form"
        ]);
        updateNewsletter();
        createLoginswitch();
        updateLoginOrder();
        deliveryingToCompany();
    }, [
        '#checkout-step-shipping > button, #checkout-step-shipping .form-login'
    ]);
}

function runNotLoggedIn() {
    watcher("estimate-shipping-methods", null, function () {
        console.log("updating shipping");
        updateShipping();
    });

    if (location.href.indexOf("#") >= 0) {
        if (location.href.indexOf("#payment") >= 0) {
            runShipping();
        } else if (location.href.indexOf("#shipping") >= 0) {
            runShipping();
        }
    } else {
        runShipping();
    }
    setupYourDetails();
    createDeliveryMethod();
    jQuery('#shipping, #opc-delivery_notes').wrapAll('<div class="opt-left"></div>');

}

/**Logged in Functions */


function setupLoggedInDetails() {
    elementDefer(function () {
        console.log('>>>> Running setupLoggedInDetails()');
        jQuery('#checkout-step-shipping > button, #checkout-step-shipping .form-login, .or-separator').wrapAll('<div class="opt-step-container" id="opt-account"></div>');
        jQuery('.field.addresses').after('<div class="opt-add-new-address"><span>Enter a new address</span></div>');
        jQuery('.opt-add-new-address').click(function () {
            jQuery('#opt-account > button').triggerHandler('click');
            jQuery('body').addClass('opt-modal-open');
            if (!jQuery('body').hasClass('opt-modal-moved')) {
                jQuery('body').addClass('opt-modal-moved');
                setTimeout(function () {
                    jQuery('#opc-new-shipping-address').closest('.modal-popup').insertAfter(jQuery('.opt-add-new-address'));
                }, 100);
            }
        });
        elementDefer(function () {
            jQuery('#opc-delivery_notes').prepend(addTitle('1', 'Authority to Leave')); // Add step 1 Your Details area
            jQuery('#shipping-new-address-form').prepend(addTitle('1', 'Deliver To')); // Add step 1 Your Detail
        }, [
            "#opc-delivery_notes",
            "#shipping-new-address-form"
        ]);
        updateNewsletter();
        createLoginswitch();
        updateLoginOrder();
        deliveryingToCompany();
    }, [
        '#checkout-step-shipping > button, #checkout-step-shipping .form-login'
    ]);


}

function runLoggedIn() {
    watcher("estimate-shipping-methods", null, function () {
        console.log("updating shipping");
        updateShipping();
    });

    runShipping();

    setupLoggedInDetails();
    createDeliveryMethod();

    jQuery('#shipping, #opc-delivery_notes').wrapAll('<div class="opt-left"></div>');
    var email = checkoutConfig.customerData.email;
    initialstateshowemail(email);

    jQuery('.opt-email-signout').append('<div><button class="btn btn-primary">Sign Out</button></div>');
    jQuery('.opt-email-signout button').click(function (e) {
        e.preventDefault();
        window.location = "https://www.platypusshoes.com.au/customer/account/logout/";
    })
    jQuery('.opt-left').prepend(addTitle('1', 'YOUR DETAILS')); // Add step 1 Your Details area

    jQuery('#checkout-step-shipping .field.addresses').before(addTitle('1', 'Deliver To')); 
    deliveryFunctionality();

}

/** MOBILE FUNCTIONS */
function addMobileTitles (selector, title, titletohide) {
    if (titletohide) {
        jQuery(titletohide).addClass('opt-hide');
    }
    jQuery(selector).before('<div class="opt-form-title-mobile" id="opt-mobile-title-' + title + '"><strong class="opt-title"><span>' + title + '</span></strong></div>');
}

function mobileDetailsLoggedOut () {
        addMobileTitles('#shipping  #checkout-step-shipping > .opt-form-title', 'Your Details', '#shipping #checkout-step-shipping > .opt-form-title');

}

function updateName (selector, newName) {
    jQuery(selector).text(newName);
}


function checkForMobile (count) {
    var count = count || 0;
    if (jQuery('html').hasClass('mobile')) {
        makeMobile();
    } else if (count < 100) {
        setTimeout(function () {
            checkForMobile(count);
        },50);
    }
}

function updateDetails () {
    
}

function makeMobile () {
    // Update titles 
    // Update Main Titles
}

/** START FUNCTIONS */
function start() {
    var loggedIn = checkoutConfig.isCustomerLoggedIn,
        step = detectStep(),
        CandC = detectCandC();
    switch (loggedIn) {
        case true:
            console.log("Running Logged In");
            if (CandC == false) {
                console.log("Not Click and Collect");
                runLoggedIn();
                setupPromo();
                checkForMobile();
            } else {
                console.log("Click and Collect")
                runLoggedIn();
                setupPromo();
                checkForMobile();
            }
            jQuery('body').addClass('opt-showpaymentaddress');
            break;
        case false:
            console.log("Running Not Logged In");
            jQuery('body').addClass('opt-not-loggedin');
            if (CandC == false) {
                console.log("Click and Collect");
                runNotLoggedIn();
                setupPromo();
                checkForMobile();
            } else {
                console.log("Not Click and Collect")
                runNotLoggedIn();
                setupPromo();
                checkForMobile();
            }
            break;
    }
}

// defer(function(){
//REMOVE BELOW COOKIE BEFORE LIVE
document.cookie = "opttest=3";
jQuery("body").addClass("opt-loading");
jQuery("body").addClass("opt-1");
updateHeader();
defer(function () {
    jQuery(".opt-1 .sidebar-main").addClass("opt-hidden");
}, ".sidebar-main");
defer(function () {
    jQuery(" .opt-1 .page-title").addClass("opt-hidden");
}, ".page-title");
defer(function () {
    jQuery(" .opt-1 .opc-progress-bar").addClass("opt-hidden");
}, ".opt-1 .opc-progress-bar");

jQuery("#checkout").attr("data-state", 0);
addBreadCrumb();
afterDefer(function () {
    jQuery("body").removeClass("opt-loading");
    start();
    setupOrderSummary();
}, "#checkout-loader");
// }, "#checkout-loader");