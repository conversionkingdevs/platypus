function desktopcss () {
    var style = '<style>.opt-1 #opc-delivery_notes .opt-carefulcontainer{border-top: 0;}.opt-1 .page-header{max-width: initial;}.opt-1 .opt-tabletshow{display: none;}.opt-1 .opt-back-link a{color: white;}.opt-hide{display: none;}.header-item.menu._left{display: none;}.opt-left{width: 50%;}.fieldset .field input[type="checkbox"] + label:before,.delivery-block .field input[type="checkbox"] + label:before,.shipping-availability .field input[type="checkbox"] + label:before{border-radius: 2px;}.fieldset .field.required > .label:after,.fieldset > .fields > .field.required > .label:after,.fieldset .field._required > .label:after,.fieldset > .fields > .field._required > .label:after,.delivery-block .field.required > .label:after,.delivery-block .field._required > .label:after,.shipping-availability .field.required > .label:after,.shipping-availability .field._required > .label:after{display: none;}.checkout-container .table-totals .amount{padding-top: 10px;}/** HEADER **/.opt-1 .nav-sections{display: none;}.opt-1 .header .header-item.minicart{display: none;}.opt-1 .page-header .header.content{display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; position: relative; max-width: 1440px;}.opt-1 .header > .logo{margin: 0 auto;}.opt-1 .opt-back-link{position: absolute; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; color: white; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif; font-size: 16px; line-height: 19px; padding-left: 22px; margin-left: 30px; font-weight: bold;}.opt-1 .opt-back-link img{display: none;}.opt-1 .opt-back-link a:after{content: ""; background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA5LjYwMyA1LjkyMiI+PHBhdGggZmlsbD0iIzU5QjdCMyIgZD0iTTkuNjAzIDEuMTJMOC40OCAwIDQuOCAzLjY4IDEuMTIzIDAgMCAxLjEybDQuOCA0LjgwMiIvPjwvc3ZnPg==); color: white; -webkit-filter: brightness(0)invert(1); filter: brightness(0)invert(1); -webkit-background-size: 15px 15px; background-size: 15px 15px; background-position: center center; width: 20px; height: 20px; position: absolute; left: -12px; top: 0; margin-top: -2px; background-repeat: no-repeat; -webkit-transform: rotate(90deg); -ms-transform: rotate(90deg); transform: rotate(90deg)}.opt-1 .messages{margin-bottom: 10px;}.opt-1 .opt-hidden{visibility: hidden; height: 0px !important; line-height: 0px !important; max-height: 0px !important; max-width: 0px !important; overflow: hidden; padding: 0px !important; margin: 0px !important; pointer-events: none; border: none !important;}.opt-1 .opt-freeze div{cursor: progress !important; position: relative;}.opt-1 .opt-freeze div:after{content: " "; height: 100%; position: absolute; top: 0px; left: 0px; width: 100%; background-color: rgba(255, 255, 255, 0.6)}.opt-1 .opt-freeze div:before{content: " "; height: 100%; width: 100%; background-image: url("https://www.platypusshoes.com.au/static/version1534159831/frontend/Ewave/platypus/en_AU/images/loader-2.gif"); -webkit-background-size: 50% auto; background-size: 50% auto; background-repeat: no-repeat; background-position: center; position: absolute; left: 0px; z-index: 1; top: 0px; display: inline-block;}.opt-1 .page-title,.opt-1 .opc-progress-bar{display: none;}.opt-1 #checkoutSteps > li{display: block !important;}.opt-1 .checkout-container,.opt-1.checkout-index-index .column.main{max-width: 1640px; padding: 0 25px;}.opt-1 #checkout[data-state="0"] .form-shipping-address,.opt-1 #checkout[data-state="0"] #opc-delivery_notes,.opt-1 #checkout[data-state="0"] #payment,.opt-1 #checkout[data-state="0"] #shipping .step-title{display: none !important;}.opt-1 #checkout[data-state="0"] .opt-breadcrumb .opt-item span{color: rgba(0, 0, 0, 0.1);}.opt-1 #checkout[data-state="0"] .opt-breadcrumb .opt-item .opt-icon{position: relative;}/** Footer **/.footer.content .footer-block.columns{display: none;}/** breadcrumb */.opt-breadcrumb{display: -webkit-box; display: -ms-flexbox; display: -webkit-flex; display: flex; -webkit-box-pack: justify; -ms-flex-pack: justify; -webkit-justify-content: space-between; justify-content: space-between; padding: 20px 0; border-right: none; border-left: none; margin: 30px 0; position: relative;}.opt-breadcrumb:after{content: ""; position: absolute; -webkit-transform: translatey(-50%); -ms-transform: translatey(-50%); transform: translatey(-50%); width: 100%; height: 1px; background: black; right: 0; top: 50%; z-index: -1}.opt-breadcrumb .opt-item{background: white; text-align: center; display: -webkit-box; display: -ms-flexbox; display: -webkit-flex; display: flex; -webkit-box-pack: center; -ms-flex-pack: center; -webkit-justify-content: center; justify-content: center; padding: 0 10px;}.opt-breadcrumb .opt-item:first-of-type{padding-left: 0;}.opt-breadcrumb .opt-item:last-of-type{padding-right: 0;}.opt-breadcrumb .opt-item .opt-icon{margin-right: 20px;}.opt-breadcrumb .opt-item span{font-size: 30px; line-height: 34px; text-align: center; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif; text-transform: uppercase; font-weight: 700;}/** login */.opt-1 .secure-pay-wrapper .info-block{display: none;}.opt-1 .checkout-payment-method .payment-method-content{margin-bottom: 20px;}.opt-1.checkout-index-index .secure-pay-wrapper .trust-block img{min-width: 93px;}.opt-1.checkout-index-index .secure-pay-wrapper .trust-block{display: -webkit-box; display: -ms-flexbox; display: -webkit-flex; display: flex; -ms-flex-wrap: wrap; -webkit-flex-wrap: wrap; flex-wrap: wrap; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; margin-top: 20px; margin-bottom: 10px;}.opt-1.checkout-index-index .secure-pay-wrapper .trust-block > div{display: inline;}.opt-1.checkout-index-index .secure-pay-wrapper .trust-block > div:last-child{width: 100%; -webkit-box-ordinal-group: 4; -webkit-order: 3; -ms-flex-order: 3; order: 3;}.opt-1.checkout-index-index .secure-pay-wrapper .trust-block p{margin-bottom: 0px; color: #4B4B4C; font-size: 14px; line-height: 22px; margin-top: 10px;}.opt-1.checkout-index-index .secure-pay-wrapper .trust-block p br{display: none;}.opt-1.checkout-index-index .secure-pay-wrapper .trust-block > .trust-label{display: block; padding-left: 0px; text-align: center; margin-bottom: 8px; -webkit-box-ordinal-group: 3; -webkit-order: 2; -ms-flex-order: 2; order: 2; margin-left: 10px;}.opt-1 .checkout-payment-method .payment-method-content #co-transparent-form-braintree .fieldset > .field{margin-bottom: 0px;}.opt-1 #checkout-payment-method-load #co-transparent-form-braintree .fieldset .field:not(.choice) > .label{margin-bottom: 0px; -webkit-transform: none; -ms-transform: none; transform: none;}.opt-1 .form-braintree .hosted-control{background-color: white;}.opt-1.logged-user #checkout[data-state="1"] #shipping > .step-title{display: none !important;}.opt-1.logged-user .newsletter-signup{display: none !important;}.opt-1 .opc-wrapper .shipping-address-items{display: -webkit-box; display: -ms-flexbox; display: -webkit-flex; display: flex; -ms-flex-wrap: wrap; -webkit-flex-wrap: wrap; flex-wrap: wrap;}.opt-1 .opc-wrapper .shipping-address-item{width: 100% !important; padding: 13px; display: -webkit-box; display: -ms-flexbox; display: -webkit-flex; display: flex; -webkit-box-pack: end; -ms-flex-pack: end; -webkit-justify-content: flex-end; justify-content: flex-end; -webkit-box-orient: vertical; -webkit-box-direction: reverse; -ms-flex-direction: column-reverse; -webkit-flex-direction: column-reverse; flex-direction: column-reverse; font-size: 12px; line-height: 17px; min-height: 130px; border: 1px solid #cfcfcf;}.opt-1 .opc-wrapper .shipping-address-items > label{display: none;}.opt-1 .action-select-shipping-item{font-size: 12px; font-weight: bold; line-height: 17px; margin-top: 0px; text-align: left; margin-bottom: 8px;}.opt-1 .opc-wrapper .shipping-address-item.selected-item .action-select-shipping-item{visibility: visible; text-decoration: none;}.opt-1 .opc-wrapper .shipping-address-item.selected-item .action-select-shipping-item:after{content: "Selected Address"; pointer-events: none; color: #59B7B3 !important; text-decoration: none; border-bottom: none;}.opc-wrapper .shipping-address-item.selected-item .action-select-shipping-item span{display: none;}.opt-1 #checkout button.facebook{font-size: 15px; line-height: 21px; text-align: center; margin-bottom: 15px; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; padding: 20px 17px;}.opt-1 #checkout button.facebook svg{margin-right: 10px; width: 15px;}.opt-1 #checkout .or-separator{text-transform: uppercase;}.opt-1 #checkout .or-separator:before,.opt-1 #checkout .or-separator:after{border: 1px solid #979797;}.opt-1 .field-tooltip.toggle{right: 16px;}.opt-1 .opc-wrapper .form-login .fieldset .note{font-size: 12px; line-height: 17px; color: #000000;}.opt-1 #checkout[data-state="0"] #shipping{max-width: 360px; margin: 0 auto;}.opt-1 #checkout-newsletter-signup{display: none;}.opt-1 label[for="checkout-newsletter-signup"]{position: relative; padding-left: 50px; min-height: 35px; cursor: pointer; display: -webkit-box; display: -ms-flexbox; display: -webkit-flex; display: flex; -webkit-box-align: center; -ms-flex-align: center; -webkit-align-items: center; align-items: center;}.opt-1 .newsletter-signup span{display: block; line-height: 18px;}.opt-1 .field input[type="checkbox"]:checked + label:after{background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI3Ljg1IiBoZWlnaHQ9IjYuMzUiIHZpZXdCb3g9IjAgMCA3Ljg1IDYuMzUiPjxwYXRoIGZpbGw9IiM1OUI3QjMiIGQ9Ik02LjQxIDBMMi45NCAzLjUgMS40MSAyIDAgMy40MWwyLjkxIDIuOTQgNC45NC00Ljk0TDYuNDEgMCIvPjwvc3ZnPg==); -webkit-filter: brightness(0)invert(1); filter: brightness(0)invert(1); -webkit-background-size: 21px; background-size: 21px; background-position: 50% 50%; background-repeat: no-repeat;}.opt-1 .field input[type="checkbox"]:checked + label:before{background: #59B7B3; border: #59B7B3;}.opt-1 label[for="checkout-newsletter-signup"]:before{border: 1px solid #c7c7c7;}.opt-1 label[for="checkout-newsletter-signup"]:before,label[for="checkout-newsletter-signup"]:after{width: 25px; height: 25px; margin-top: -12.5px; top: 17px; position: absolute; left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; content: "";}.opt-1 .opt-login-continue{background-color: #59B7B3; height: 55px; text-align: center; cursor: pointer;}.opt-1 .opt-login-continue:hover{background-color: #a0d6d4;}.opt-1 .opt-login-continue span{font-size: 19px; line-height: 55px; color: white; text-transform: uppercase; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif; font-weight: bold;}/** shipping **/.opt-1 .note-logged{display: none !important;}.opt-1 .opc-wrapper .form-login .actions-toolbar > .primary{float: right; padding: 0px;}.opt-1 .opc-wrapper .form-login .actions-toolbar > .secondary{float: left; padding: 0px; text-align: left;}.opt-1.logged-user #checkout[data-state="1"] #checkout-step-shipping > *,.opt-1.logged-user #checkout[data-state="1"] #customer-email-fieldset > div.field.required.custom.ready.filled > div > span{display: block;}.opt-1 #checkout[data-state="1"] #checkout-step-shipping .form-login{display: block;}.opt-1 #checkout[data-state="1"] #co-shipping-form{display: block !important;}.opt-1 #checkout[data-state="1"] #payment{-webkit-box-ordinal-group: 3; -ms-flex-order: 2; -webkit-order: 2; order: 2;}.opt-1 #checkout[data-state="1"] #checkoutSteps > li{width: 50%; margin: 0px; padding-left: 30px; padding-right: 30px;}.opt-1 #checkout[data-state="1"] #checkoutSteps > li.checkout-payment-method{padding-right: 0;}.opt-1 #checkout[data-state="1"] .opc-wrapper{display: -webkit-box; display: -ms-flexbox; display: -webkit-flex; display: flex; -webkit-box-pack: end; -ms-flex-pack: end; -webkit-justify-content: flex-end; justify-content: flex-end;}.opt-1 #checkout[data-state="1"] #checkoutSteps{display: -webkit-box; display: -ms-flexbox; display: -webkit-flex; display: flex; max-height: initial !important; /*-ms-flex-wrap: wrap; flex-wrap: wrap; -webkit-box-orient: vertical; -webkit-box-direction: normal; -ms-flex-direction: column; flex-direction: column; -webkit-box-pack: start; -ms-flex-pack: start; justify-content: flex-start; -webkit-box-align: end; -ms-flex-align: end; align-items: flex-end; max-height: 1028px; min-height: 1160px;*/ width: 66%;}.opt-1.opt1-login #checkout[data-state="1"] #checkoutSteps,.opt-1.opt1-login #checkout[data-state="2"] #checkoutSteps{min-height: 760px !important;}.opt-1.opt1-login .opc-wrapper .edit-address-link{right: 5px;}.opt-1 #checkout[data-state="1"] #shipping > .step-title{display: none;}.opt-1 #checkout[data-state="1"] #shipping-new-address-form > div:nth-child(12) > div > small{display: none;}.opt-1 #checkout[data-state="1"] .fieldset{margin-bottom: 0px;}.opt-1.checkout-index-index #checkout[data-state="1"] .column.main{padding-left: 25px; padding-right: 25px;}.opt-1 #checkout[data-state="1"] #payment{position: relative;}/** payment **/.payment-method-billing-address .actions-toolbar .action-cancel{cursor: pointer; color: #59B7B3; font-family: "Open Sans"; font-size: 14px; text-transform: initial; font-weight: initial;}.checkout-payment-method .payment-methods .actions-toolbar .primary{float: left;}.checkout-payment-method .payment-method-title label[for="paypal_express"] li{max-width: 80px; margin-right: 10px;}.checkout-payment-method .payment-method-title label[for="paypal_express"] .payment-icon{min-width: 80px;}.opt-1 #afterpaypayovertime-method > div.payment-method-title.field.choice,.opt-1 #checkout-payment-method-load > div > div > div.payment-method.item > div.payment-method-title.title.field.choice._clearfix{padding-top: 20px; padding-bottom: 15px;}.opt-1 #afterpaypayovertime-method > div.payment-method-title.field.choice > div{display: none;}.opt-1 #afterpaypayovertime-method > div.payment-method-title.field.choice > label > span{display: none;}.opt-1 #checkout-payment-method-load > div > div > div.payment-method.item > small{display: none;}.opt-giftcard-promo-container{margin-bottom: 10px;}.checkout-payment-method .discount-code,.checkout-payment-method .giftcardaccount,.paypal-review.view .discount-code,.paypal-review.view .giftcardaccount{padding-top: 10px;}#giftcard-form > div.actions-toolbar.large-6.small-12._right > div > button{line-height: initial;}#giftcard-form > div.payment-option-inner.large-6.small-12._left > div.primary._right{margin-top: 25px; margin-left: 10px;}#giftcard-form > div.payment-option-inner.large-6.small-12._left > div.primary._right button{border-width: 1px; width: 120px;}.opt-dropdown-title{position: relative; background-color: #F5F5F5; padding: 15px; cursor: pointer;}.opt-1 #giftcard-form > div.payment-option-inner.large-6.small-12._left > div.field.medium-3.small-12._left .detail-label{visibility: hidden}.checkout-payment-method .discount-code .detail-label:first-child,.checkout-payment-method .giftcardaccount .detail-label:first-child,.paypal-review.view .discount-code .detail-label:first-child,.paypal-review.view .giftcardaccount .detail-label:first-child{text-transform: uppercase;}.opt-dropdown-title span{font-size: 15px; line-height: 17px; font-weight: 700; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif;}.opt-giftcard-promo-dropdown{max-height: 0px;}.opt-active .opt-giftcard-promo-dropdown{max-height: 650px;}.opt-dropdown-title:after{content: " "; position: absolute; right: 15px; top: 50%; -webkit-transform: translate(0%, -50%); -ms-transform: translate(0%, -50%); transform: translate(0%, -50%); height: 15px; width: 15px; background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA5LjYwMyA1LjkyMiI+PHBhdGggZmlsbD0iIzU5QjdCMyIgZD0iTTkuNjAzIDEuMTJMOC40OCAwIDQuOCAzLjY4IDEuMTIzIDAgMCAxLjEybDQuOCA0LjgwMiIvPjwvc3ZnPg==); -webkit-background-size: 15px 15px; background-size: 15px 15px; background-position: center center; background-repeat: no-repeat;}.opt-active .opt-dropdown-title:after{-webkit-transform: rotate(180deg) translate(0%, 50%); -ms-transform: rotate(180deg) translate(0%, 50%); transform: rotate(180deg) translate(0%, 50%);}.discount-code .payment-option-title{padding-top: 0px !important;}.checkout-payment-method .discount-code._collapsible .payment-option-title,.checkout-payment-method .giftcardaccount._collapsible .payment-option-title,.paypal-review.view .discount-code._collapsible .payment-option-title,.paypal-review.view .giftcardaccount._collapsible .payment-option-title{cursor: initial;}.opt-1 .checkout-payment-method .discount-code .payment-option-inner{width: 100%;}.opt-1 .checkout-payment-method .discount-code{margin-bottom: 1px;}.opt-1 .checkout-payment-method .discount-code .gift-hint,.checkout-payment-method .giftcardaccount .gift-hint{border-bottom: none; padding-bottom: 0px;}.opt-1 .payment-option-content{display: block !important;}.opt-1 .checkout-payment-method .discount-code .payment-option-inner,.opt-1 .checkout-payment-method .giftcardaccount .payment-option-inner{margin-bottom: 0px;}.opt-1 .checkout-payment-method .discount-code._active .payment-option-title .action-toggle:after,.opt-1 .checkout-payment-method .giftcardaccount._active .payment-option-title .action-toggle:after,.paypal-review.view .discount-code._active .payment-option-title .action-toggle:after,.paypal-review.view .giftcardaccount._active .payment-option-title .action-toggle:after,.opt-1 .abs-payment-additional-collapsible > .payment-option-title .action-toggle:after,.checkout-payment-method .discount-code > .payment-option-title .action-toggle:after,.checkout-payment-method .giftcardaccount > .payment-option-title .action-toggle:after,.paypal-review.view .discount-code > .payment-option-title .action-toggle:after,.paypal-review.view .giftcardaccount > .payment-option-title .action-toggle:after{display: none;}.checkout-payment-method .discount-code > .payment-option-title .action-toggle,.checkout-payment-method .giftcardaccount > .payment-option-title .action-toggle{color: #59b7b3;}.opt-1 #checkout[data-state="2"] #shipping > .step-title{display: none;}.opt-1 #discount-form > div.actions-toolbar._right{margin-top: 0px; float: right !important;}.opt-1 #checkout[data-state="2"] #shipping-new-address-form > div:nth-child(12) > div > small{display: none;}.opt-1 #checkout[data-state="2"] #checkout-step-shipping > *,.opt-1 #checkout[data-state="2"] #customer-email-fieldset > div.field.required.custom.ready.filled > div > span{display: none;}.opt-1.logged-user #checkout[data-state="2"] #checkout-step-shipping > *{display: block;}.opt-1 #checkout[data-state="2"] #checkout-step-shipping .form-login{display: block;}.opt-1 #checkout[data-state="2"] #co-shipping-form{display: block !important;}.opt-1 #checkout[data-state="2"] #payment{-webkit-box-ordinal-group: 3; -ms-flex-order: 2; -webkit-order: 2; order: 2;}.opt-1 #checkout[data-state="2"] #checkoutSteps > li{width: 50%; margin: 0px; padding-left: 25px; padding-right: 25px;}.opt-1 #checkout[data-state="2"] .opc-wrapper{display: -webkit-box; display: -ms-flexbox; display: -webkit-flex; display: flex; -webkit-box-pack: end; -ms-flex-pack: end; -webkit-justify-content: flex-end; justify-content: flex-end;}.opt-1 #checkout[data-state="2"] #checkoutSteps{display: -webkit-box; display: -ms-flexbox; display: -webkit-flex; display: flex; -ms-flex-wrap: wrap; -webkit-flex-wrap: wrap; flex-wrap: wrap; -webkit-box-orient: vertical; -webkit-box-direction: normal; -ms-flex-direction: column; -webkit-flex-direction: column; flex-direction: column; -webkit-box-pack: start; -ms-flex-pack: start; -webkit-justify-content: flex-start; justify-content: flex-start; -webkit-box-align: end; -ms-flex-align: end; -webkit-align-items: flex-end; align-items: flex-end; max-height: 1028px; min-height: 1100px; width: 66%;}.opt-1 #checkout[data-state="2"] #shipping > .step-title{display: none;}.opt-1 #checkout[data-state="2"] #shipping-new-address-form > div:nth-child(12) > div > small{display: none;}.opt-1 #checkout[data-state="2"] .fieldset{margin-bottom: 0px;}.opt-1.checkout-index-index #checkout[data-state="2"] .column.main{padding-left: 25px; padding-right: 25px;}.opt-1 #checkout[data-state="2"] #shipping,.opt-1 #checkout[data-state="2"] #opc-delivery_notes{position: relative;}.opt-1 #checkout[data-state="2"] #shipping:before,.opt-1 #checkout[data-state="2"] #opc-delivery_notes:before{content: " "; position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; background-color: rgba(255, 255, 255, 0.7); z-index: 2;}.opt-1 #payment .step-title{display: none;}.opt-ccvtooltip{display: inline-block; text-align: right; float: right; font-size: 12px; color: #434343; cursor: pointer; z-index: 99; position: relative;}.opt-5 #braintree_cc_type_cvv_div .hosted-error{margin-top: 25px; z-index: -1;}/** Modal**/.opt-overlay{width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.6); position: fixed; z-index: 9001; top: 0px;}.opt-modal{position: absolute; background-color: white; width: 80%; max-width: 600px; top: 50%; left: 50%; -webkit-transform: translate(-50%, -50%); display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; -webkit-box-orient: vertical; -webkit-box-direction: normal; -webkit-flex-direction: column; -ms-flex-direction: column; flex-direction: column; padding: 40px;}.opt-modal .opt-modal-header{margin-bottom: 24px; font-size: 20px; font-weight: bold; color: #434343;}.opt-modal .opt-modal-buttons{width: 100%; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex;}.opt-modal .opt-modal-buttons button{width: 100%; max-width: 250px; font-size: 16px; margin: auto; background-color: #34BAB4; color: white; border: 2px solid #34BAB4;}.opt-modal .opt-close{position: absolute; top: -30px; right: 0px; width: 20px; height: 20px; font-size: 20px; color: white; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif; text-align: center; cursor: pointer; padding: 10px;}.opt-modal .opt-close:after,.opt-modal .opt-close:before{content: ""; width: 20px; height: 2px; background: white; position: absolute; -webkit-transform: rotate(45deg); -ms-transform: rotate(45deg); transform: rotate(45deg); right: 0; top: 10px;}.opt-modal .opt-close:before{-webkit-transform: rotate(135deg); -ms-transform: rotate(135deg); transform: rotate(135deg);}.opt-modal .opt-modal-buttons button.opt-modal-back-to-cart{background-color: white; color: #34BAB4;}/** Inputs **/.opt-1 .opt-form-title{margin-bottom: 10px;}.opt-1 .opt-bag-container .opt-form-title{display: inline-block;}.opt-1 .opt-form-title .opt-step-number{display: none !important; height: 33px; width: 33px; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; color: #34BAB4; border: 1px solid #34BAB4; border-radius: 50%; font-weight: bold; font-size: 20px;}.opt-1 .opt-form-title .opt-title{font-size: 16px; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif; text-transform: uppercase; font-weight: 700;}.opt-1 .opc-wrapper .fieldset .field:not(.choice) > .label,.opt-1 .opc-wrapper .delivery-block .field:not(.choice) > .label,.opt-1 .opc-wrapper .shipping-availability .field:not(.choice) > .label{color: #434343; font-size: 14px; line-height: 17px; font-weight: normal; font-family: "Open Sans"; text-transform: initial;}.opt-1 div.mage-error[generated]{font-family: "Open Sans"; font-size: 12px; text-transform: initial; text-align: right;}/* .opt-1 div.mage-error[generated]:before{display: none;}*/.opt-1 div[name="shippingAddress.telephone"] small,.opt-1 div[name="billingAddressbraintree.telephone"],.opt-1 div[name="shippingAddress.country_id"]{display: none;}.opt-1 .fieldset .field.custom input,.opt-1 .delivery-block .field.custom input,.opt-1 .shipping-availability .field.custom input,.opt-1 .fieldset .field select,.opt-1 .delivery-block .field select,.opt-1 .shipping-availability .field select{border: 1px solid #E6E6E6;}.opt-1 .field._error .control input,.opt-1 .field._error .control select,.opt-1 .field._error .control textarea{border: 1px solid #E02E2B;}.opt-1 .fieldset .field.filled > .label,.opt-1 .delivery-block .field.filled > .label,.opt-1 .shipping-availability .field.filled > .label,.opt-1 .fieldset .field.filled > .label,.opt-1 .delivery-block .field.filled > .label,.opt-1 .shipping-availability .field.filled > .label{}.fieldset .field.filled > .label,.delivery-block .field.filled > .label,.shipping-availability .field.filled > .label{-webkit-transform: translate(0, -20px) scale(.75, .75); -ms-transform: translate(0, -20px) scale(.75, .75); transform: translate(0, -20px) scale(.75, .75); color: #9A9A9A !important; font-weight: 100 !important; font-family: "ProximaNova", "Helvetica Neue", Helvetica, Arial, sans-serif !important; letter-spacing: 1px;}.checkout-index-index .fieldset.address .field.phone-region{display: none;}.checkout-index-index .fieldset.address .field.phone-region + .field[name$=telephone]{width: 100%; padding-left: 0;}.checkout-index-index .fieldset.address .field.phone-region + .field[name$=telephone] label{left: 12px !important;}.opt-phone-message{display: none;}.opt-1 .opt-DeliveryMethod-Cont .opt-heading{display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: justify; -webkit-justify-content: space-between; -ms-flex-pack: justify; justify-content: space-between;}.opt-1 .opt-DeliveryMethod-Cont .opt-heading .opt-title{font-size: 16px; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif; text-transform: uppercase; font-weight: 700;}.opt-1 #opt-DeliveryMethod-Edit,.opt-bag-container .opt-form-title a{cursor: pointer; color: #59B7B3; font-family: "Open Sans"; font-size: 14px;}.opt-1 #opt-DeliveryMethod-Edit{float: right;}.opt-1 .opt-DeliveryMethod-Cont{margin-top: -10px;}.opt-1 .opt-DeliveryMethod-Cont .opt-delivery-container{border: 1px solid #cfcfcf; padding: 20px; margin-top: 10px; margin-bottom: 20px;}.opt-1 .opt-DeliveryMethod-Cont .opt-delivery-line:not(.opt-hide){line-height: initial; height: initial; min-height: initial; max-height: initial; font-size: initial; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center;}.opt-1 .opt-DeliveryMethod-Cont .opt-delivery-container p{margin: 5px 0; line-height: initial; height: initial; margin-left: 20px;}.opt-1 .authoritytoleave-checkbox label span{display: none;}.checkout-index-index .authoritytoleave-note{margin-top: -15px; font-size: 14px;}.opt-1 .filled:not(._error) .input-text[aria-invalid="false"]:not(:focus),.opt-1 .filled:not(._error)[name="shippingAddress.street.0"] .input-text:not(:focus){background-image: url(//cdn.optimizely.com/img/6092490016/a49c2b7d20a24f809ecfd75074e1e22f.png) !important; background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI3Ljg1IiBoZWlnaHQ9IjYuMzUiIHZpZXdCb3g9IjAgMCA3Ljg1IDYuMzUiPjxwYXRoIGZpbGw9IiM1OUI3QjMiIGQ9Ik02LjQxIDBMMi45NCAzLjUgMS40MSAyIDAgMy40MWwyLjkxIDIuOTQgNC45NC00Ljk0TDYuNDEgMCIvPjwvc3ZnPg==) !important; -webkit-background-size: 15px !important; background-size: 15px !important; background-repeat: no-repeat !important; background-position: right 20px center !important;}.opt-1 ._error .input-text[aria-invalid="true"],.opt-1 ._error .input-text{background-image: url(//cdn.optimizely.com/img/6092490016/ac173ad1b6cc40de937f25b3f51d7989.png) !important; -webkit-background-size: 12px 12px !important; background-size: 12px 12px !important; background-repeat: no-repeat !important; background-position: right 20px center !important;}.opt-1 .payment-method{padding: 0 20px;}.opt-1 .payment-method.item{margin-bottom: 30px; padding-bottom: 0px;}.opt-1 .checkout-container .payment-methods .form{padding-bottom: 0px;}.opt-1 .checkout-container .payment-methods small a,.opt-1 .afterpay-payment-method .payment-method-note .link{font-weight: 700 !important; text-decoration: none;}.opt-1 #afterpaypayovertime-method > div.payment-method-title.field.choice > div > div > span,.opt-1 #checkout-payment-method-load > div > div > div.payment-method.item > small > span{font-size: 12px; line-height: 17px;}.opt-1 .checkout-payment-method .payment-method-billing-address{margin-bottom: 10px !important;}.opt-1 #braintree_cc_type_cvv_div .field-tooltip .field-tooltip-content{right: 100px; left: initial; top: 55px;}.opt-1 #braintree_cc_type_cvv_div .field-tooltip .field-tooltip-content:before,.opt-1 #braintree_cc_type_cvv_div .field-tooltip .field-tooltip-content:after{left: 70%; -webkit-transform: rotate(90deg); -ms-transform: rotate(90deg); transform: rotate(90deg); top: -20px;}.field-tooltip .field-tooltip-action{display: none;}.opt-1 #co-payment-form > fieldset > legend + br{display: none;}.opt-1 .afterpay-payment-method .afterpay-checkout-note ul{display: -webkit-box; display: -ms-flexbox; display: -webkit-flex; display: flex; padding: 0 10px;}.opt-1 #afterpaypayovertime-method{padding-bottom: 0px;}.opt-1 #giftcard-form .large-6{width: 100%;}.opt-1 #giftcard-form div.large-6._left{display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: justify; -webkit-justify-content: space-between; -ms-flex-pack: justify; justify-content: space-between;}.opt-1 .opt-edit{background-color: #000; color: white; padding: 5px; text-align: right; margin-right: 25px; cursor: pointer; margin-bottom: 10px;}.opt-1 .opt-shipping-continue{height: 55px; text-align: center; cursor: pointer; max-width: 50%; padding: 0 25px; width: 100%; margin-top: 25px;}.opt-1 .opt-shipping-continue div{width: 100%; background-color: #59B7B3;}.opt-1 .opt-shipping-continue span{font-size: 19px; line-height: 55px; width: 100%; color: white; font-weight: bold; text-transform: uppercase; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif;}.opt-1.checkout-index-index .secure-pay-wrapper .info-block{margin-bottom: 10px;}.checkout-payment-method .payment-option-content .payment-option-inner + .actions-toolbar{float: left !important; margin-top: 0px; margin-bottom: 10px;}.opt-1.checkout-index-index .secure-pay-wrapper{padding-bottom: 0px; padding-left: 20px; padding-right: 20px;}.opt-1 .opt-payment-continue{height: 55px; text-align: center; cursor: initial; width: 100%; margin-top: 30px; margin-bottom: 0px;}.opt-1 .opt-payment-continue div{width: 100%; background-color: #59B7B3;}.opt-1 .opt-payment-continue span{font-size: 19px; line-height: 55px; width: 100%; color: white; font-weight: bold; text-transform: uppercase; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif;}/** BAG **/.opt-bag-container{width: 34%; padding: 0 0 0 30px;}.opt-bag-item{display: -webkit-box; display: -ms-flexbox; display: -webkit-flex; display: flex; -webkit-box-pack: start; -ms-flex-pack: start; -webkit-justify-content: flex-start; justify-content: flex-start; padding: 15px; margin-bottom: -1px;}.opt-bag-container > .opt-bag-item:last-child{margin-bottom: 0px;}.opt-details-container{width: 100%;}.opt-details-container .opt-top{display: -webkit-box; display: -ms-flexbox; display: -webkit-flex; display: flex; -webkit-box-pack: justify; -ms-flex-pack: justify; -webkit-justify-content: space-between; justify-content: space-between; width: 100%;}.opt-details-container > div{margin-bottom: 0; padding-right: 15px;}.opt-details-container > div > span:first-child{padding-right: 5px;}.opt-details-container > div > span{font-size: 14px; line-height: 16px;}.opt-quantity-container{padding: 15px;}.opt-quantity-container,.opt-bag-item{border: 1px solid #cfcfcf}.opt-bag-item:nth-of-type(1){border: 1px solid #cfcfcf; border-bottom: 0;}.cart-totals .mark,.cart-totals .amount,.opc-block-summary .table-totals .mark,.opc-block-summary .table-totals .amount,.checkout-container .table-totals .mark,.checkout-container .table-totals .amount,.review-container .table-totals .mark,.review-container .table-totals .amount,.checkout-container .table-totals .grand .mark,.checkout-container .table-totals .shipping .mark span{font-size: 14px; line-height: 16px; padding: 5px 0; font-weight: initial;}.cart-totals .mark,.cart-totals .amount,.opc-block-summary .table-totals .mark,.checkout-container .table-totals .mark,.review-container .table-totals .mark{text-align: left;}.checkout-container .table-totals .grand .mark,.checkout-container .table-totals .grand .mark span,.checkout-container .table-totals .grand .amount strong{margin-top: 10px; font-size: 22px !important; font-weight: bold !important;}.checkout-container .table-totals .grand .amount{padding-bottom: 0 !important; padding-top: 12px !important;}.checkout-container .table-totals .grand .amount strong{font-weight: 500;}.checkout-container .table-totals .amount{width: 64px;}.opt-1 .opt-img-container{min-width: 120px !important; max-width: 120px !important;}/** optaccount **/.opt-1 #opt-account{padding: 20px 20px 10px 20px; background: #F5F5F7; margin-bottom: 10px; margin-top: 10px;}.opt-1.checkout-index-index .fieldset.address fieldset.field .field{margin-bottom: 0;}.opt-1.checkout-index-index .fieldset.address fieldset.field .field:last-child{display: none;}.opt-1 .opc-wrapper .form-login .actions-toolbar > .secondary .action{color: #59B7B3; font-size: 14px; text-decoration: none;}.opt-1:not(.opt-showloginstate) .hidden-fields{display: none !important;}.opt-1 #opt-account #customer-email-fieldset .note{display: none;}.opt-1:not(.opt-showloginstate) #opt-account .facebook,.opt-1:not(.opt-showloginstate) #opt-account .or-separator{display: none;}.opt-1 #opt-account .opt-form-toggle,.opt-1 .opt-delivering{font-size: 14px;}.opt-1 #opt-account .opt-form-toggle span,.opt-1 .opt-delivering a{color: #59B7B3; cursor: pointer;}.opt-delivering{margin-bottom: 10px;}/** input spacing **/.opt-1 .opc-wrapper .fieldset > .field.required,.opt-1 .opc-wrapper .fieldset > .field._required,.opt-1.checkout-index-index .fieldset.address .field,.opc-wrapper .field{margin-bottom: 10px;}.opt-1 .newsletter-signup a{display: none;}/** PAYMENT AREA**/.opt-1 .checkout-payment-method .discount-code._collapsible .payment-option-title{display: none;}.opt-1 .opt-giftcard-promo-dropdown{display: none;}.opt-1 .opt-active .opt-giftcard-promo-dropdown{display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: vertical; -webkit-box-direction: normal; -webkit-flex-direction: column; -ms-flex-direction: column; flex-direction: column;}.opt-1 .checkout-payment-method .discount-code .gift-hint,.checkout-payment-method .giftcardaccount .gift-hint,.checkout-payment-method .giftcardaccount._collapsible .payment-option-title{display: none;}.opt-1 .checkout-payment-method .discount-code .payment-option-inner{width: 100%; margin-right: 10px;}.opt-1 .checkout-payment-method .discount-code > .payment-option-content > .form{display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: end; -webkit-align-items: flex-end; -ms-flex-align: end; align-items: flex-end; -webkit-box-pack: justify; -webkit-justify-content: space-between; -ms-flex-pack: justify; justify-content: space-between;}.opt-1 .payment-option-content ._clearfix:after,.opt-1 .payment-option-content .helper_clearfix:after{display: none;}.opt-1 .opt-dropdown-title{background: white; border: 1px solid #D5D5D5; padding: 30px 15px;}.opt-1 .opt-dropdown-title span{font-family: "Open Sans"; font-weight: normal; text-transform: capitalize;}.opt-1 .checkout-payment-method .giftcardaccount{margin-bottom: 0; border-bottom: 1px solid white;}.opt-1 .checkout-payment-method .giftcardaccount .actions-toolbar .action.action-check{text-decoration: none; font-weight: normal; color: #59B7B3; margin-left: 0; text-align: left;}.opt-1 .checkout-payment-method .discount-code .actions-toolbar .action,.checkout-payment-method .giftcardaccount .actions-toolbar .action{width: 120px; min-width: initial;}.opt-1 #giftcard-form .payment-option-inner .field._left:nth-of-type(1){width: 75%; margin-right: 10px;}.opt-1 #giftcard-form > div.payment-option-inner.large-6.small-12._left > div.field.medium-3.small-12._left{min-width: 120px;}.opt-1 .checkout-payment-method .payment-method-billing-address .billing-address-details{display: none;}.opt-1 .checkout-payment-method .payment-method{padding: 0; border: 1px solid #D5D5D5; margin-bottom: -1px;}.opt-1 .checkout-payment-method .payment-method > div{padding: 20px 20px;}.opt-1 .checkout-payment-method .payment-method._active:not(.item) > div{position: relative; border-bottom: 1px solid #D5D5D5; margin-bottom: 0px;}.opt-1 .checkout-payment-method .payment-method._active:not(.item) > div:last-of-type{border-bottom: 0px solid #D5d5d5;}html:not(.mobile) .opt-1 .checkout-payment-method .payment-method.item > div:not(.payment-method-title){display: none;}.opt-1 .checkout-payment-method .payment-method._active .payment-method-content{background: #f0f0f0;}.opt-1 .checkout-payment-method .payment-method.payment-method-braintree .payment-method-title{padding-bottom: 15px;}#checkout-payment-method-load > div > div > div.payment-method.payment-method-braintree._active > div.payment-method-title.field.choice > label{margin-bottom: 0;}#payment_form_braintree > div.field.number.required:nth-of-type(3),#payment_form_braintree > div.cvv{width: 47.5%;}#payment_form_braintree > div.cvv{float: right;}#payment_form_braintree > div.field.number.required:nth-of-type(3){float: left;}.checkout-agreements-block{margin-bottom: 0;}.opt-1 .payment-method-content .messages{margin: 0;}.form-braintree .hosted-control{border: 1px solid #cfcfcf;}#co-transparent-form-braintree .opt-form-title,#afterpaypayovertime-method .opt-form-title{display: none;}#afterpaypayovertime-method .fieldset .field.field-select-billing{margin: 0;}#co-transparent-form-braintree br{display: none;}.payment-method.item strong{display: none;}/**Logged In User**/.logged-user #opt-account{display: none !important;}.opt-1.logged-user .opt-initialemail{background: #f3f3f3; padding: 20px 20px; margin-bottom: 10px; border: 1px solid #D5D5D5; ; border-top: 3px solid #59B7B3; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: justify; -webkit-justify-content: space-between; -ms-flex-pack: justify; justify-content: space-between; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center;}.opt-1.logged-user .opt-initialemail p{margin: 0;}.opt-1.logged-user .opt-initialemail button{border-color: #59B7B3; color: #59B7B3; padding: 20px}.opt-1.logged-user .opt-initialemail button:hover{border-color: #59B7B3; background-color: #59B7B3; color: white;}.opt-1.logged-user .opt-add-new-address{border: 2px solid #59B7B3; color: #59B7B3; padding: 20px; cursor: pointer; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif; font-weight: 700; margin: 0; line-height: 1.7rem; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; vertical-align: middle; text-align: center; text-transform: uppercase; margin-top: 20px; margin-bottom: 20px;}.opt-1.logged-user #opc-delivery_notes{padding-top: 10px;}.opt-1.logged-user .opt-add-new-address:hover{border-color: #59B7B3; background-color: #59B7B3; color: white;}.opt-1 .opc-wrapper .shipping-address-item{width: 100%; background: white; border: 1px solid #CFCFCF; min-height: initial; font-size: 12px; position: relative; padding: 14px; padding-left: 65px; cursor: pointer;}.opt-1 .opc-wrapper .shipping-address-items label{display: none;}.opt-1 .opc-wrapper .shipping-address-item .action-select-shipping-item{display: none;}.opt-1 .opc-wrapper .shipping-address-item br:not(:nth-last-child(3)){display: none;}.opt-1 .opc-wrapper .shipping-address-item br:nth-of-type(1){display: block;}.opt-1 .opc-wrapper .shipping-address-item.selected-item:after,.opt-1 .opc-wrapper .shipping-address-item:before{-webkit-transform: translateY(-50%); -ms-transform: translateY(-50%); transform: translateY(-50%); position: absolute; top: 50%; content: ""; display: block;}.opt-1 .opc-wrapper .shipping-address-item:before{left: 20px; top: 50%; border-radius: 50%; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; content: ""; background: white; border: 1px solid #c7c7c7 !important; width: 20px !important; height: 20px !important;}.opt-1 .opc-wrapper .shipping-address-item.selected-item:before{border-color: #59B7B3 !important;}.opt-1 .opc-wrapper .shipping-address-item.selected-item:after{margin-top: 0px; position: absolute; border-radius: 50%; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; content: ""; left: 25px; width: 10px !important; height: 10px !important; background: none; background-color: #59B7B3;}/** Modal CSS **/.opt-1.opt-modal-open._has-modal{overflow: initial;}.opt-1.opt-modal-open.checkout-index-index .modal-slide,.opt-1.opt-modal-open.checkout-index-index .modal-popup{position: static;}.opt-1.checkout-index-index .opt-initial .modal-popup,.opt-1.checkout-index-index:not(.opt-modal-open) .modal-popup,.opt-1.checkout-index-index:not(._has-modal) .modal-popup{display: none !important;}.opt-1.opt-modal-open.checkout-index-index .modal-popup .modal-inner-wrap{width: initial; margin: 0; left: 0; position: static;}.opt-1.opt-modal-open.checkout-index-index .modal-popup .modal-inner-wrap svg{display: none;}.opt-1.opt-modal-open.checkout-index-index .modals-overlay{display: none;}.opt-1.opt-modal-open.checkout-index-index .modal-slide .modal-inner-wrap,.opt-1.opt-modal-open.checkout-index-index .modal-popup .modal-inner-wrap{-webkit-box-shadow: none; box-shadow: none; -webkit-transition: none; transition: none;}.opt-1.opt-modal-open.checkout-index-index .modal-popup .modal-header{padding-left: 0; padding-right: 0; padding-top: 0; position: relative;}.opt-1.opt-modal-open.checkout-index-index .modal-popup .modal-header .action-close{background: none; width: initial; top: -10px; right: 0px; color: #59B7B3; text-decoration: none;}.opt-1.opt-modal-open.checkout-index-index .modal-popup .opt-form-title{display: none;}.opt-1.opt-modal-open.checkout-index-index .modal-popup .modal-header .action-close:hover{color: #59B7B3;}.opt-1.opt-modal-open.checkout-index-index .modal-popup .modal-header .action-close:after{content: "Close"; text-transform: initial; font-size: 14px; text-decoration: none; font-family: "Open Sans";}.opt-1.opt-modal-open.checkout-index-index .modal-popup .modal-title{text-align: left; padding-bottom: 0; font-size: 16px; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif; text-transform: uppercase; font-weight: 700;}.opt-1.opt-modal-open.checkout-index-index .modal-popup .modal-content{padding: 0;}.opt-1.opt-modal-open.checkout-index-index .modal-popup .modal-footer{padding: 0; text-align: left;}.opt-1.opt-modal-open.checkout-index-index .modal-popup .modal-footer button{float: none; margin-left: 0;}.opt-1.opt-modal-open.checkout-index-index .modal-popup .modal-footer button:nth-of-type(2){margin-left: 10px; font-weight: initial; text-transform: initial; font-size: 14px; text-decoration: none; font-family: "Open Sans"; color: #59B7B3;}.opt-1.opt-modal-open.checkout-index-index .modal-popup .form-shipping-address .label[for="shipping-save-in-address-book"]{font-size: 14px; font-weight: initial; padding-bottom: 0;}.opt-1.opt-showpaymentaddress #checkout[data-state="1"] .opc-payment .fieldset{display: block !important; padding-bottom: 10px;}/** Careful Containers **/.opt-carefulcontainer{display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: vertical; -webkit-box-direction: normal; -webkit-flex-direction: column; -ms-flex-direction: column; flex-direction: column; padding: 20px; background: #f2f1f2; border: 1px solid #d5d5d5;}.opt-carefulcontainer p{margin: 0;}.opt-carefulcontainer img{margin-right: 15px;}.opt-carefulcontainer > div{display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center;}.opt-carefulcontainer a{color: #59B7B3; text-decoration: underline;}.opt-carefulcontainer .opt-topcontainer{margin-bottom: 10px;}.opt-carefulcontainer#opt-deliverymethod-show{margin-top: -20px; margin-bottom: 20px;}.opt-carefulcontainer#opt-bag-show{border-bottom: none;}.opt-1 .tooltip.wrapper .toggle.icon{display: none;}@media (max-width: 1510px){.opt-1 #giftcard-form .payment-option-inner .field._left:nth-of-type(1){width: 100%; margin-right: initial;}.opt-1 #giftcard-form div.large-6._left{-webkit-flex-wrap: wrap; -ms-flex-wrap: wrap; flex-wrap: wrap;}.opt-1 #giftcard-form > div.payment-option-inner.large-6.small-12._left > div.field.medium-3.small-12._left .detail-label{display: none;}#giftcard-form > div.payment-option-inner.large-6.small-12._left > div.primary._right{margin-top: initial;}}@media (max-width: 1470px) and (min-width: 1150px){.opt-1.checkout-index-index .authoritytoleave-note{margin-top: -25px;}}@media (max-width: 1400px){.opt-details-container > div > span{font-size: 11px; line-height: 11px;}.opt-details-container > div{line-height: initial; font-size: 12px;}}@media (max-width: 1365px){.opt-1 #checkout[data-state="1"] #checkoutSteps > li{padding-left: 10px;}.opt-bag-container{padding-left: 10px;}.opt-1 .opt-payment-continue{margin-top: 10px;}.checkout-container .table-totals .grand .mark{margin-top: -10px;}.cart-totals .mark, .cart-totals .amount, .opc-block-summary .table-totals .mark, .opc-block-summary .table-totals .amount, .checkout-container .table-totals .mark, .checkout-container .table-totals .amount, .review-container .table-totals .mark, .review-container .table-totals .amount, .checkout-container .table-totals .grand .mark, .checkout-container .table-totals .shipping .mark span, .checkout-container .table-totals .grand .mark, .checkout-container .table-totals .grand .mark span, .checkout-container .table-totals .grand .amount strong{font-size: 12px !important; line-height: initial; padding: 0 !important;}}@media (max-width: 1200px){.opt-1 .opt-bag-container{padding: 0 10px;}.opt-1 #checkout[data-state="1"] #checkoutSteps > li{padding: 0 10px;}.opt-1 .checkout-container, .opt-1.checkout-index-index .column.main{padding: 0 10px;}}/** Tablet **/@media (max-width: 1150px){.page-header.js-page-header:not(.-offset) + #maincontent .opt-bag-container{top: 140px;}.opt-1 #maincontent .opt-bag-container{margin-left: 20px; max-width: 400px; width: 100%;}.opt-1 .opt-bag-container{position: -webkit-sticky; position: sticky; top: 20px; height: 0;}.opt-breadcrumb .opt-item span{font-size: 20px;}.opt-1 #opt-account{padding: 10px 10px 5px 10px;}.opt-1 #opt-account .opt-form-toggle, .opt-1 .opt-delivering{font-size: 12px;}.opt-1 .opt-img-container{display: none;}.opt-1 .fieldset .field.custom input, .opt-1 .delivery-block .field.custom input, .opt-1 .shipping-availability .field.custom input, .opt-1 .fieldset .field select, .opt-1 .delivery-block .field select, .opt-1 .shipping-availability .field select{font-size: 11px;}.opt-1 .opc-wrapper .fieldset .field:not(.choice) > .label, .opt-1 .opc-wrapper .delivery-block .field:not(.choice) > .label, .opt-1 .opc-wrapper .shipping-availability .field:not(.choice) > .label{font-size: 11px !important;}.field-select-billing{margin: 0 !important;}.checkout-payment-method .payment-method .payment-method-billing-address .actions-toolbar .primary{padding: 10px;}.checkout-payment-method .payment-method .payment-method-billing-address .actions-toolbar > .primary{padding: 0;}.opt-left{width: 100%;}.opt-1 #checkout[data-state="1"] .opc-wrapper{-webkit-box-pack: justify; -webkit-justify-content: space-between; -ms-flex-pack: justify; justify-content: space-between;}.opt-1 #checkout[data-state="1"] #checkoutSteps{-webkit-box-orient: vertical; -webkit-box-direction: normal; -webkit-flex-direction: column; -ms-flex-direction: column; flex-direction: column; width: 60%;}.opt-1 #checkout[data-state="1"] #checkoutSteps > li{width: 100%; padding: 0; margin-top: 10px;}.opt-1 #checkout[data-state="1"] #checkout-step-shipping .form-login{margin-right: 0 !important; max-width: initial;}.opt-1 #checkout[data-state="1"] #co-shipping-form{margin-right: 0; max-width: initial;}.opt-1 .opt-dropdown-title{padding: 20px 15px;}.opt-1 .opt-DeliveryMethod-Cont .opt-delivery-line:not(.opt-hide){font-size: 12px;}.opt-1 .opt-DeliveryMethod-Cont .opt-delivery-line img{width: 30px;}.opt-1 .opt-DeliveryMethod-Cont .opt-delivery-container p{margin-left: 10px;}.opt-dropdown-title span{font-size: 12px;}.checkout-index-index .authoritytoleave-note{font-size: 12px;}.opt-1 .newsletter-signup span{font-size: 12px;}.opt-1 .payment-method-content{font-size: 12px;}.afterpay-payment-method .afterpay-note-tilte{margin: 0; font-size: 14px;}.opt-1 .afterpay-payment-method .afterpay-checkout-redirect{max-width: 350px;}.opt-1 .opt-tablethide{display: none;}.opt-breadcrumb{margin: 15px 0;}.opt-1 .opt-tabletshow{display: contents;}}@media (max-width: 900px){.opt-1 #maincontent .opt-bag-container{max-width: 320px;}}@media (max-width: 795px){.opt-1.checkout-index-index .authoritytoleave-note{margin-top: -25px;}}@media (max-width: 765px){.opt-1 #maincontent .opt-bag-container{max-width: 280px;}}@media (max-width: 660px){.opt-1 #checkout[data-state="1"] #checkout-step-shipping .form-login{border-bottom: 0; margin: 10px 0 0; padding: 0;}}/** MOBILE **/.mobile .opt-breadcrumb{display: none;}.mobile .header-item.minicart-wrapper,.mobile .header-item.menu-toggle{display: none;}.mobile .header>.logo img{width: 128px;}.mobile .opt-1 .opt-back-link{padding-left: 10px;}.mobile .opt-1 .page-header .header.trust{display: none;}.mobile .opt-1 #checkout[data-state="1"] #checkoutSteps{width: 100%;}.mobile .opt-1 #checkout[data-state="1"] .opc-wrapper{-webkit-box-orient: vertical; -webkit-box-direction: normal; -webkit-flex-direction: column; -ms-flex-direction: column; flex-direction: column; padding: 0;}.mobile .opt-1 .page-main{margin-left: 0; margin-right: 0;}.mobile .opt-1 #maincontent .opt-bag-container{max-width: 280px; height: auto; margin: 0; padding: 0;}.mobile .opt-1 .opt-giftcard-promo-container{display: none;}.mobile .opt-1 #checkout-step-payment .opt-form-title{display: none;}.mobile .opt-1 .opt-form-title{border-bottom: 1px solid black;}.mobile .opt-1 .opt-img-container{display: block; min-width: 100px !important; max-width: 100px !important;}.mobile #opc-sidebar{}.mobile .opt-1 .opt-step-container{border: 1px solid #e9e9e9; border-top: 3px solid #63bfc9;}.mobile .opt-1 label[for="checkout-newsletter-signup"]{padding-left: 40px;}.mobile .opt-1 .newsletter-signup span{font-size: 10px;}.mobile .opt-1 .checkout-payment-method .payment-method._active .payment-method-content{display: none;}.mobile .opt-1 .types-list>.item{max-width: 100px;}.mobile .opt-1 .checkout-container .payment-methods .item>.title{padding-bottom: 5px;}.mobile .opt-1.checkout-index-index .authoritytoleave-note{margin-top: -15px;}.mobile .opt-1 .payment-method.payment-method-braintree .payment-method-title .label > strong{display: none;}.mobile .opt-1 #checkout .or-separator{margin-bottom: 10px; font-size: 10px;}.mobile .opt-1 .opt-carefulcontainer p{font-size: 12px;}.mobile .opt-1 .opt-carefulcontainer{border-top: 0;}</style>'
    jQuery('head').append(style);
}

function desktopjs () {
    function defer(method, selector) {
        if (window.jQuery && window.checkoutConfig) {
            if (jQuery(selector).length > 0) {
                method();
            } else {
                setTimeout(function () {
                    defer(method, selector);
                }, 50);
            }
        } else {
            setTimeout(function () {
                defer(method, selector);
            }, 50);
        }
    }
    function elementDefer(method, arrayOfSelectors) {
        var valid = 0;
        for (var i = 0; i < arrayOfSelectors.length; i++) {
            if (jQuery(arrayOfSelectors[i]).length > 0) {
                valid++;
            }
        }
        if (valid == arrayOfSelectors.length) {
            method();
        } else {
            setTimeout(function () {
                elementDefer(method, arrayOfSelectors);
            }, 50);
        }
    }
    
    
    function afterDefer(method, selector) {
        if (window.jQuery) {
            if (jQuery(selector).length == 0) {
                method();
            } else {
                setTimeout(function () {
                    afterDefer(method, selector);
                }, 50);
            }
        } else {
            setTimeout(function () {
                afterDefer(method, selector);
            }, 50);
        }
    }
    
    function watcher(loadingIdentifier, receivingcallback) {
        (function (open) {
            XMLHttpRequest.prototype.open = function (m, u, a, us, p) {
                this.addEventListener('readystatechange', function () {
                    try {
                        if (this.responseURL.indexOf(loadingIdentifier) >= 0 && this.readyState == 4) {
                            receivingcallback();
                        }
                    } catch (e) {}
                }, false);
                open.call(this, m, u, a, us, p);
            };
        })(XMLHttpRequest.prototype.open);
    }
    
    function updateHeader() {
        if (jQuery('.opt-back-link').length <= 0) {  
            jQuery('.header.content').prepend('<div class="opt-back-link"><a href="/checkout/cart/"><span class="opt-tablethide">CONTINUE SHOPPING</span><span class="opt-tabletshow">Back</span></a></div>');
        }
    }
    
    function addEditItems(container) {
        jQuery(container + ' .opt-form-title').append('<div class="opt-Edit-Button"><p class="opt-a-Edit">Edit</p></div>');
        jQuery(container + ' .opt-a-Edit').click(function () {
            if (jQuery('body').hasClass('opt-loggedin') && !jQuery('body').hasClass('opt-loggedin-delivery')) {
                document.location = document.location.origin + '/customer/account/';
            } else {
                jQuery('.opt-initial').removeClass('opt-initial');
                showHideAreas(jQuery(this).closest('.opt-step-container').attr('id'));
            }
        });
    }
    function deliveryFunctionality() {
        jQuery('.shipping-address-items').on('click', '.shipping-address-item:not(.opt-added-click)', function (e) {
            e.stopPropagation();
            jQuery(this).find('button.action-select-shipping-item').triggerHandler('click');
        }).addClass('opt-added-click');
    }
    
    function modal() {
        elementDefer(function () {
            jQuery("body").append("<div class='opt-overlay' style='display: none;'><div class='opt-modal'><div class='opt-modal-header'><div class='opt-modal-head'></div><span>This will take you back to your cart.</span></div><div class='opt-modal-buttons'><button class='btn btn-primary opt-stay-here'>STAY HERE</button><button href='/checkout/cart/' class='btn btn-primary opt-modal-back-to-cart'>BACK TO CART</button></div></div></div></div>");
    
            jQuery('.opt-modal-back-to-cart').click(function () {
                document.location = document.location.origin + jQuery(this).attr('href');
            });
    
            jQuery(".opt-modal").append("<div class='opt-close'></div> ");
    
            jQuery(".opt-modal").click(function (event) {
                event.stopPropagation();
            });
    
            jQuery('body').on('click', 'div.link > a.link[title="Edit"], .opt-sidebar-header .opt-title a.link, #opt-DeliveryMethod-Edit', function (e) {
                e.preventDefault();
                jQuery(".opt-overlay").fadeIn("slow");
                jQuery("body").css({
                    "height": "100%",
                    "overflow": "hidden"
                });
            });
    
            jQuery(".opt-close, .opt-overlay, .opt-stay-here").click(function () {
                jQuery(".opt-overlay").fadeOut("slow");
                jQuery("body").css({
                    "height": "auto",
                    "overflow": "inherit"
                });
            });
        }, [
            '#co-transparent-form-braintree #payment_form_braintree'
        ]);
    }
    
    function createLoginswitch() {
        elementDefer(function () {
            jQuery('#opt-account').append('<div class="opt-form-toggle"><p>Already have an account? <span class="opt-underline opt-link-text">Sign in here</span></p></div>');
            jQuery('.opt-form-toggle').click(function () {
                if (jQuery('body').hasClass('opt-showloginstate')) {
                    jQuery('body').removeClass('opt-showloginstate');
                    jQuery('.opt-form-toggle').html('<p>Already have an account? <span class="opt-underline opt-link-text">Sign in here</span></p>');
                } else {
                    jQuery('body').addClass('opt-showloginstate');
                    jQuery('.opt-form-toggle').html('<p><span class="opt-underline opt-link-text">Return to Guest Checkout</span></p>');
                }
            });
        }, [
            "#opt-account"
        ]);
    }
    
    function addTitle(titlenum, titlename) {
        return '<div class="opt-form-title"><div class="opt-step-number"><span>' + titlenum + '</span></div><strong class="opt-title"><span>' + titlename + '</span></strong></div>';
    }
    
    function addSignin() {
        elementDefer(function () {
            jQuery('#customer-email-fieldset').before('<div class="opt-signin-cont"><p>Continue as a guest below or <span><a href="opt-signin">Sign In</a></span></div>');
        }, '#customer-email-fieldset');
    }
    
    function deliveryingToCompany() {
        elementDefer(function () {
            jQuery('.field[name="shippingAddress.company"]').toggleClass('opt-hide');
            var deliverying = '<div class="opt-delivering"><span>Delivering to a company? </span><a id="opt-companyDelivery" href="#" class="opt-underline">Click here</a></div>';
            jQuery('.field[name="shippingAddress.company"]').before(deliverying);
            jQuery('#opt-companyDelivery').click(function (e) {
                e.preventDefault();
                jQuery('.field[name="shippingAddress.company"]').toggleClass('opt-hide');
            });
        }, [
            '.field[name="shippingAddress.company"]'
        ]);
    }
    
    /** Update */
    function updateShipping() {
        jQuery(".opt-quantity-container").children().remove();
        jQuery(".opt-quantity-container").append(jQuery(".table-totals").clone());
    }
    
    
    /** Getters */
    function getBag() {
        var data = [];
        jQuery(".details").each(function () {
            var product = {
                "name": jQuery(this).find(".name").text(),
                "colour": jQuery(this).find(".values").first().text(),
                "size": jQuery(this).find(".values").last().text(),
                "qty": jQuery(this).find(".value").first().text(),
                "delivery": jQuery(this).find(".value").last().text(),
                "price": jQuery(this).find(".price").text(),
                "img": jQuery(this).prev().attr("src")
            }
            data.push(product);
        });
        return data;
    }
    
    function getLoginErrors() {
        if (jQuery("#customer-email.mage-error").length > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    function getShippingErrors() {
        if (jQuery("#shipping .field._error, .message-error").length > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    /** Adders */
    //add after login
    function addPayment() {
        jQuery(".opt-login-continue").remove();
        jQuery(".opt-edit").remove();
    
        jQuery("#co-payment-form .secure-pay-wrapper").remove();
    
        jQuery("#co-payment-form > fieldset").append(jQuery(".discount-code"));
        jQuery("#co-payment-form > fieldset").append(jQuery("#giftcardaccount-placer"));
    
    
        jQuery("#shipping").before('<div class="opt-edit"><span>Edit Details</span></div>');
    
        jQuery(".opt-edit").click(function () {
            window.location.href = "#shipping";
            runShipping();
        });
    
        jQuery("#co-payment-form").append(jQuery(".secure-pay-wrapper").clone());
    
        jQuery("#payment .secure-pay-wrapper .trust-block img").wrapAll('<div></div>');
        jQuery("#payment .secure-pay-wrapper .trust-block p").wrapAll('<div></div>');
        jQuery("#payment .secure-pay-wrapper .trust-block").prepend(jQuery("#payment .secure-pay-wrapper .trust-block strong"));
        jQuery("img[src='https://www.paypalobjects.com/webstatic/en_US/i/buttons/pp-acceptance-medium.png']").attr("src", "//cdn.optimizely.com/img/6092490016/b2a2c5dba9a4484d84369fb7d97f334e.png");
    
        jQuery("body").click(function () {
            jQuery('.opt-1 #checkout #checkoutSteps').css("max-height", (jQuery("#payment").height() + 20) + "px");
        });
    
        jQuery("#co-payment-form > fieldset").append(jQuery(".opt-giftcard-promo-dropdown").children());
        jQuery(".opt-giftcard-promo-container").remove();
        jQuery("#co-payment-form .secure-pay-wrapper").before('<div class="opt-giftcard-promo-container"></div>');
        jQuery(".opt-giftcard-promo-container").append('<div class="opt-dropdown-title"><span>APPLY GIFT CARDS AND PROMO CODES</span></div>');
        jQuery("#co-payment-form > fieldset .discount-code:eq(0), #co-payment-form > fieldset .giftcardaccount:eq(0)").wrapAll('<div class="opt-giftcard-promo-dropdown"></div>');
        jQuery(".opt-giftcard-promo-container").append(jQuery(".opt-giftcard-promo-dropdown"));
    
        jQuery(".opt-dropdown-title").click(function () {
            jQuery(this).parent().toggleClass("opt-active");
            var interval = setInterval(function () {
                jQuery('.opt-1 #checkout #checkoutSteps').css("max-height", (jQuery("#payment").height() + 20) + "px");
            }, 100);
            setTimeout(function () {
                clearInterval(interval);
            }, 800);
        });
    
        jQuery('.opt-1 #checkout #checkoutSteps').css("max-height", (jQuery("#payment").height() + 20) + "px");
        jQuery('.secure-pay-wrapper:eq(0)').appendTo(jQuery('.opt-bag-container'));
    }
    
    function addShoppingLogin() {
        jQuery("body").addClass("opt1-login");
    }
    
    function addPaymentLogin() {
        jQuery("body").addClass("opt1-login");
    }
    
    //add after login
    function addShipping() {
        //phone help
        jQuery(".opt-phone-message").remove();
        jQuery(".opt-edit").remove();
    
        jQuery(".opt-1 #co-shipping-form").append('<small class="opt-phone-message">Pssst, drop the "0" from the beginning of your mobile number. For example, if your phone number is 0401 123 456, enter 410 123 456</small>');
        jQuery("body").click(function () {
            jQuery('.opt-1 #checkout #checkoutSteps').css("max-height", (jQuery("#payment").height() + 20) + "px");
        });
        jQuery('.opt-1 #checkout #checkoutSteps').css("max-height", (jQuery("#payment").height() + 20) + "px");
    
        jQuery("label[for='checkout-newsletter-signup']").html('<span>Sign up to receive awesome updates and promotions</span>');
    
        //add button
        jQuery("button.login").click(function () {
            localStorage.setItem("checkoutlogin", true);
        });
    
    }
    
    //add after login
    function addBag() {
        jQuery(".opt-bag-container").remove();
        jQuery(".opt-payment-continue").remove();
        jQuery("#checkout .opc-wrapper").append('<div class="opt-bag-container"></div>');
        //add baggage details
        var bag = getBag();
        for (var i = 0; i < bag.length; i++) {
            jQuery(".opt-bag-container").append('<div class="opt-bag-item" data-value="' + i + '"></div>');
            jQuery(".opt-bag-item[data-value='" + i + "']").append('<div class="opt-img-container"><img src="' + bag[i].img + '" alt="product image"></div>');
            jQuery(".opt-bag-item[data-value='" + i + "']").append('<div class="opt-details-container"></div>');
            jQuery(".opt-bag-item[data-value='" + i + "'] .opt-details-container").append('<div class="opt-top"><span><strong>' + bag[i].name + '</strong></span></div>');
            jQuery(".opt-bag-item[data-value='" + i + "'] .opt-details-container").append('<div class="opt-color"><span><strong>Colour:</strong></span><span>' + bag[i].colour + '</span></div>');
            jQuery(".opt-bag-item[data-value='" + i + "'] .opt-details-container").append('<div class="opt-size"><span><strong>Size:</strong></span><span>' + bag[i].size + '</span></div>');
            jQuery(".opt-bag-item[data-value='" + i + "'] .opt-details-container").append('<div class="opt-qty"><span><strong>QTY:</strong></span><span>' + bag[i].qty + '</span></div>');
            jQuery(".opt-bag-item[data-value='" + i + "'] .opt-details-container").append('<div class="opt-price"><span><strong>Price:</strong></span><span>' + bag[i].price + '</span></div>');
            jQuery(".opt-bag-item[data-value='" + i + "'] .opt-details-container").append('<div class="opt-delivery"><span><strong>Delivery Method:</strong></span><span>' + bag[i].delivery + '</span></div>');
        }
    
        //add quantity
        jQuery(".opt-bag-container").append('<div class="opt-quantity-container"></div>');
        jQuery(".opt-quantity-container").append(jQuery(".table-totals").clone());
    
        jQuery(".opt-bag-container").append('<div class="opt-payment-continue"><div><span>Place Order</span></div></div>');
        jQuery(".opt-payment-continue").click(function () {
            jQuery('.place-order-container button').click()
        });
    }
    
    //add on start
    function addLogin() {
        defer(function () {
            jQuery(".opt-login-continue").remove();
            jQuery('.or-separator').remove();
            jQuery("#co-shipping-form").before('<div class="opt-login-continue"><span>Continue</span></div>');
            jQuery("#customer-email").after('<div for="customer-email" generated="true" class="mage-error" id="customer-email-error" style="display: none;">Please enter a valid email address (Ex: johnsmith@domain.com).</div>');
            jQuery(".opt-login-continue").click(function () {
                if (!getLoginErrors() && jQuery("#customer-email").val() != "") {
                    jQuery(".form-login").addClass("opt-guest");
                    runShipping();
                } else {
                    jQuery("#customer-email").addClass("mage-error");
                    jQuery("#customer-email-error").show();
                }
            });
        }, "#co-shipping-form");
    }
    
    //add on start
    function addBreadCrumb() {
        jQuery(".opt-breadcrumb").remove();
        jQuery("#checkout").prepend('<div class="opt-breadcrumb"></div>');
        jQuery(".opt-breadcrumb").append('<div class="opt-item"><div class="opt-icon"><img src="//cdn.optimizely.com/img/6092490016/f33c853356c24f5b8113ffa3eea9b4ad.png" alt=""/></div><span>Shipping<span class="opt-tabletshow"> & Payment</span></span></div>');
        jQuery(".opt-breadcrumb").append('<div class="opt-item opt-tablethide"><div class="opt-icon"><img src="//cdn.optimizely.com/img/6092490016/50beb4f44971422fba21f03179c2a398.png" alt=""/></div><span>Payment</span></div>');
        jQuery(".opt-breadcrumb").append('<div class="opt-item"><div class="opt-icon"><img src="//cdn.optimizely.com/img/6092490016/36b8b024ac224fb9979887135aa3b256.png" alt=""/></div><span>Your Bag</span></div>');
    }
    
    /** State Changers */
    //run after hitting shipping continue button
    function runPayment() {
    
        jQuery("#checkout").attr("data-state", 2);
        addBag();
        addPayment();
        if (jQuery("body").hasClass("logged-user")) {
            addPaymentLogin();
        }
        addBreadCrumb();
    }
    
    //run after login
    function runShipping() {
        jQuery("#checkout").attr("data-state", 1);
        addBag();
        addPayment();
        addShipping();
        if (jQuery("body").hasClass("logged-user")) {
            addShoppingLogin();
        }
        addBreadCrumb();
    }
    
    //run on start
    function runLogin() {
        jQuery("#checkout").attr("data-state", 0);
        addLogin();
        addBreadCrumb();
    }
    
    // Functions from one pages
    
    function addEdit(selector) {
        jQuery(selector).after('<a href="/checkout/cart/" class="link _right">Edit Cart</a>');
    }
    
    
    function updateNewsletter() {
        elementDefer(function () {
            var news = '<span>  Keep me updated with the latest Platypus news. </span>';
            jQuery('.checkout-container .newsletter-signup .label[for="checkout-newsletter-signup"]').html(news);
        }, [
            '.checkout-container .newsletter-signup .label[for="checkout-newsletter-signup"]'
        ]);
    }
    
    function updateLoginOrder() {
        jQuery('#opt-account > .form.form-login').insertBefore(jQuery('#opt-account .or-separator'));
        jQuery('#opt-account > .facebook.button').insertAfter(jQuery('#opt-account .or-separator'));
    
    }
    
    function deliveryMethodHide() {
        var optdelivery = {};
        jQuery('.opt-delivery-line').addClass('opt-hide');
        for (var x = 0; x < jQuery('.checkout-summary:eq(0) .product-item .delivery-info').length; x++) {
            var item = jQuery('.checkout-summary .product-item .delivery-info:eq(' + x + ')').find('.method > span:eq(1)'),
                qty = jQuery('.checkout-summary .product-item:eq(' + x + ') .options-list > .value').text();
                console.log(item.text().indexOf('Standard'));
            if (item.text().indexOf('Standard') >= 0) {
                jQuery('#opt-method-standard').removeClass('opt-hide');
            }
            if (item.text().indexOf('Next') >= 0) {
                jQuery('#opt-method-Next').removeClass('opt-hide');
            }
            if (item.text().indexOf('Click') >= 0) {
                jQuery('#opt-method-click').removeClass('opt-hide');
            }
        }
    }
    
    function initialstateshowemail(email) {
        if (jQuery('.opt-initialemail').length <= 0) {
            jQuery('.opt-left').prepend('<div class="opt-initialemail"><div class="opt-email-cont"><p>Welcome back</p><strong><span>' + jQuery('#customer-email').val() + '</span></strong></div><div class="opt-email-signout"></div></div>');
        }
    
        if (email == true) {
            console.log(email);
            jQuery('.opt-initialemail span').text(jQuery('#customer-email').val());
        } else if (email == undefined) {
            jQuery('#opt-account .opt-form-title').addClass('opt-Tick');
        } else if (email && email.length > 0) {
            jQuery('.opt-initialemail span').text(email);
        } else {
            jQuery('.opt-initialemail span').text(jQuery('#customer-email').val());
        }
    }
    
    function addCarefuldontlose (selector, clickselector, showid, type) {
        var html = '<div class="opt-carefulcontainer" id="'+showid+'" style="display: none;"><div class="opt-topcontainer"><img src="//placehold.it/25x25" /><p><strong>Careful! Don\'t lose your pair.</strong></p></div><div class="opt-bottom"><p>To update your delivery method you will be redirected to the cart. <a href="/checkout/cart/">Change Delivery Method</a></p></div></div>';
        switch (type) {
            case "append":
                jQuery(selector).append(html);
                break;
            case "before":
                jQuery(selector).before(html);
                break;
        }
        
        jQuery('body').on('click', clickselector, function (e) {
            e.preventDefault();
            jQuery('#'+showid).toggle();
        });
    }
    
    
    function createDeliveryMethod() {
        jQuery('#opc-delivery_notes').prepend('<div class="opt-DeliveryMethod-Cont"><div class="opt-delivery-container"></div></div>');
        jQuery('.opt-DeliveryMethod-Cont').prepend('<div class="opt-heading opt-title"><span class="opt-title">Delivery Method</span><span id="opt-DeliveryMethod-Edit">Edit</span></div>');
        elementDefer(function () {
            jQuery('.opt-DeliveryMethod-Cont .opt-delivery-container').append('<div class="opt-delivery-line" id="opt-method-standard"><img src="//placehold.it/41x41" ><p><strong>Free</strong> Standard Shipping</p></div><div class="opt-delivery-line" id="opt-method-Next"><img src="//placehold.it/41x41"><p><strong>Next Day</strong> Shipping</p></div><div class="opt-delivery-line" id="opt-method-click"><img src="//placehold.it/41x41"><p><strong>Free</strong> Click and Collect</p></div>');
            deliveryMethodHide();
            addCarefuldontlose('.opt-DeliveryMethod-Cont', '.opt-DeliveryMethod-Cont #opt-DeliveryMethod-Edit', 'opt-deliverymethod-show', "append");
        }, [
            '.checkout-summary .product-item .delivery-info'
        ]);
    }
    
    function setupYourDetailsCandC(email) {
        elementDefer(function () {
            console.log('>>>> Running setupdetails()');
            jQuery('#checkout-step-shipping > .newsletter-signup').wrapAll('<div class="opt-step-container" id="opt-account"></div>');
            jQuery('#opt-account').prepend(addTitle('1', 'YOUR DETAILS')); // Add step 1 Your Details area
            addEditItems('#opt-account'); // Add Edit Items Link
            addSignin(); // Add signin area
            updateNewsletter();
            createLoginswitch();
        }, [
            '#checkout-step-shipping > .newsletter-signup'
        ]);
    }
    
    function updatePromoPlaceholds(selector, name) {
        jQuery(selector).attr('placeholder', name);
    }
    
    function addCCVTooltip() {
        jQuery('.payment-method.payment-method-braintree .payment-method-content .checkout-agreements-block').after('<div class="opt-ccvtooltip"><span>What is my CCV?</span></div>');
        jQuery('.opt-ccvtooltip').click(function () {
            jQuery('.cvv .field-tooltip .field-tooltip-content').toggleClass('_active').toggle();
        });
    }
    
    function setupPromo() {
        jQuery('.opt-giftcard-promo-container').prependTo(jQuery('#co-payment-form'));
        jQuery('#co-payment-form').prepend(addTitle('1', 'Billing Address'));
        jQuery('#co-payment-form > .fieldset').prepend(addTitle('1', 'Select Payment Method'));
        jQuery('.opt-1 .opt-dropdown-title span').text(jQuery('.opt-1 .opt-dropdown-title span').text().toLowerCase());
        jQuery('.opt-1 .checkout-payment-method .discount-code').insertAfter(jQuery('.checkout-payment-method .giftcardaccount'));
        jQuery('.opt-1 .checkout-payment-method .giftcardaccount .actions-toolbar .action.action-check').text('View Balance');
        if (jQuery('#billing-address-same-as-shipping-braintree').prop('checked') != true) {
            jQuery('#billing-address-same-as-shipping-braintree').click()
        }
            updatePromoPlaceholds('#giftcard-code', 'Gift Card Number');
            updatePromoPlaceholds('#giftcard-pin', 'Pin');
            updatePromoPlaceholds('#discount-code', 'Promo Code');
    
            jQuery('.opt-1 .checkout-payment-method .discount-code .payment-option-inner .detail-label span').text('Apply Promo Code');
            addCCVTooltip();
            jQuery('#giftcard-form > div.actions-toolbar.large-6.small-12._right > div.primary._right').appendTo(jQuery('#giftcard-form > div.payment-option-inner.large-6.small-12._left'));
    }
    
    function setupOrderSummary() {
        jQuery('.opt-bag-container').prepend(addTitle('1', 'Order Summary'));
        jQuery('.opt-bag-container .opt-form-title').after('<span id="opt-DeliveryMethod-Edit">Edit</span>');
        addCarefuldontlose('.opt-bag-container .opt-bag-item:eq(0)', '.opt-bag-container #opt-DeliveryMethod-Edit', 'opt-bag-show', "before");
    }
    
    function detectCandC() {
        var data = checkoutConfig.quoteItemData,
            CandCOnly = true;
        for (var i = 0; i < data.length; i++) {
            if (data[i].shipping_method != 'collect_collect') {
                CandCOnly = false;
                return CandCOnly;
            }
        }
        return CandCOnly;
    }
    
    function detectStep() {
        var step = 'none';
        jQuery('#checkoutSteps').find(' > li').each(function () {
            if (jQuery(this).css('display') != 'none') {
                step = jQuery(this).attr('id');
            }
        });
        return step;
    }
    
    function setupYourDetails() {
        elementDefer(function () {
            console.log('>>>> Running setupdetails()');
            jQuery('#checkout-step-shipping > button, #checkout-step-shipping .form-login, .or-separator').wrapAll('<div class="opt-step-container" id="opt-account"></div>');
            jQuery('#opt-account').before(addTitle('1', 'YOUR DETAILS')); // Add step 1 Your Details area
            elementDefer(function () {
                jQuery('#opc-delivery_notes').prepend(addTitle('1', 'Authority to Leave')); // Add step 1 Your Details area
                jQuery('#shipping-new-address-form').prepend(addTitle('1', 'Deliver To')); // Add step 1 Your Detail
            }, [
                "#opc-delivery_notes",
                "#shipping-new-address-form"
            ]);
            updateNewsletter();
            createLoginswitch();
            updateLoginOrder();
            deliveryingToCompany();
        }, [
            '#checkout-step-shipping > button, #checkout-step-shipping .form-login'
        ]);
    }
    
    function runNotLoggedIn() {
        watcher("estimate-shipping-methods", null, function () {
            console.log("updating shipping");
            updateShipping();
        });
    
        if (location.href.indexOf("#") >= 0) {
            if (location.href.indexOf("#payment") >= 0) {
                runShipping();
            } else if (location.href.indexOf("#shipping") >= 0) {
                runShipping();
            }
        } else {
            runShipping();
        }
        setupYourDetails();
        createDeliveryMethod();
        jQuery('#shipping, #opc-delivery_notes').wrapAll('<div class="opt-left"></div>');
    
    }
    
    /**Logged in Functions */
    
    
    function setupLoggedInDetails() {
        elementDefer(function () {
            console.log('>>>> Running setupLoggedInDetails()');
            jQuery('#checkout-step-shipping > button, #checkout-step-shipping .form-login, .or-separator').wrapAll('<div class="opt-step-container" id="opt-account"></div>');
            jQuery('.field.addresses').after('<div class="opt-add-new-address"><span>Enter a new address</span></div>');
            jQuery('.opt-add-new-address').click(function () {
                jQuery('#opt-account > button').triggerHandler('click');
                jQuery('body').addClass('opt-modal-open');
                if (!jQuery('body').hasClass('opt-modal-moved')) {
                    jQuery('body').addClass('opt-modal-moved');
                    setTimeout(function () {
                        jQuery('#opc-new-shipping-address').closest('.modal-popup').insertAfter(jQuery('.opt-add-new-address'));
                    }, 100);
                }
            });
            elementDefer(function () {
                jQuery('#opc-delivery_notes').prepend(addTitle('1', 'Authority to Leave')); // Add step 1 Your Details area
                jQuery('#shipping-new-address-form').prepend(addTitle('1', 'Deliver To')); // Add step 1 Your Detail
            }, [
                "#opc-delivery_notes",
                "#shipping-new-address-form"
            ]);
            updateNewsletter();
            createLoginswitch();
            updateLoginOrder();
            deliveryingToCompany();
        }, [
            '#checkout-step-shipping > button, #checkout-step-shipping .form-login'
        ]);
    
    
    }
    
    function runLoggedIn() {
        watcher("estimate-shipping-methods", null, function () {
            console.log("updating shipping");
            updateShipping();
        });
    
        runShipping();
    
        setupLoggedInDetails();
        createDeliveryMethod();
    
        jQuery('#shipping, #opc-delivery_notes').wrapAll('<div class="opt-left"></div>');
        var email = checkoutConfig.customerData.email;
        initialstateshowemail(email);
    
        jQuery('.opt-email-signout').append('<div><button class="btn btn-primary">Sign Out</button></div>');
        jQuery('.opt-email-signout button').click(function (e) {
            e.preventDefault();
            window.location = "https://www.platypusshoes.com.au/customer/account/logout/";
        })
        jQuery('.opt-left').prepend(addTitle('1', 'YOUR DETAILS')); // Add step 1 Your Details area
    
        jQuery('#checkout-step-shipping .field.addresses').before(addTitle('1', 'Deliver To')); 
        deliveryFunctionality();
    
    }
    
    /** MOBILE FUNCTIONS */
    function addMobileTitles (selector, title, titletohide) {
        if (titletohide) {
            jQuery(titletohide).addClass('opt-hide');
        }
        jQuery(selector).before('<div class="opt-form-title-mobile" id="opt-mobile-title-' + title + '"><strong class="opt-title"><span>' + title + '</span></strong></div>');
    }
    
    function mobileDetailsLoggedOut () {
            addMobileTitles('#shipping  #checkout-step-shipping > .opt-form-title', 'Your Details', '#shipping #checkout-step-shipping > .opt-form-title');
    
    }
    
    function updateName (selector, newName) {
        jQuery(selector).text(newName);
    }
    
    
    function checkForMobile (count) {
        var count = count || 0;
        if (jQuery('html').hasClass('mobile')) {
            makeMobile();
        } else if (count < 100) {
            setTimeout(function () {
                checkForMobile(count);
            },50);
        }
    }
    
    function updateDetails () {
        
    }
    
    function makeMobile () {
        // Update titles 
        // Update Main Titles
    }
    
    /** START FUNCTIONS */
    function start() {
        var loggedIn = checkoutConfig.isCustomerLoggedIn,
            step = detectStep(),
            CandC = detectCandC();
        switch (loggedIn) {
            case true:
                console.log("Running Logged In");
                if (CandC == false) {
                    console.log("Not Click and Collect");
                    runLoggedIn();
                    setupPromo();
                    checkForMobile();
                } else {
                    console.log("Click and Collect")
                    runLoggedIn();
                    setupPromo();
                    checkForMobile();
                }
                jQuery('body').addClass('opt-showpaymentaddress');
                break;
            case false:
                console.log("Running Not Logged In");
                jQuery('body').addClass('opt-not-loggedin');
                if (CandC == false) {
                    console.log("Click and Collect");
                    runNotLoggedIn();
                    setupPromo();
                    checkForMobile();
                } else {
                    console.log("Not Click and Collect")
                    runNotLoggedIn();
                    setupPromo();
                    checkForMobile();
                }
                break;
        }
    }
    
    // defer(function(){
    //REMOVE BELOW COOKIE BEFORE LIVE
    document.cookie = "opttest=3";
    jQuery("body").addClass("opt-loading");
    jQuery("body").addClass("opt-1");
    updateHeader();
    defer(function () {
        jQuery(".opt-1 .sidebar-main").addClass("opt-hidden");
    }, ".sidebar-main");
    defer(function () {
        jQuery(" .opt-1 .page-title").addClass("opt-hidden");
    }, ".page-title");
    defer(function () {
        jQuery(" .opt-1 .opc-progress-bar").addClass("opt-hidden");
    }, ".opt-1 .opc-progress-bar");
    
    jQuery("#checkout").attr("data-state", 0);
    addBreadCrumb();
    afterDefer(function () {
        jQuery("body").removeClass("opt-loading");
        start();
        setupOrderSummary();
    }, "#checkout-loader");
    // }, "#checkout-loader");
}

function defermob(method, selector, method2, selector2) {
    if (window.jQuery && window.checkoutConfig) {
        if (jQuery(selector).length > 0) {
            method();
        } else if (jQuery(selector2).length > 0) {
            method2();
        } else {
            setTimeout(function () {
                defermob(method, selector, method2, selector2);
            }, 50);
        }
    } else {
        setTimeout(function () {
            defermob(method, selector, method2, selector2);
        }, 50);
    }
}

function mobilejs() {
    document.cookie = 'optTesting=optTesting;expires=0';

/** HELPERS */

function defer(method, selector) {
    if (window.jQuery && window.checkoutConfig) {
        if (jQuery(selector).length > 0) {
            method();
        } else {
            setTimeout(function () {
                defer(method, selector);
            }, 50);
        }
    } else {
        setTimeout(function () {
            defer(method, selector);
        }, 50);
    }
}

function elementDefer(method, arrayOfSelectors) {
    var valid = 0;
    for (var i = 0; i < arrayOfSelectors.length; i++) {
        if (jQuery(arrayOfSelectors[i]).length > 0) {
            valid++;
        }
    }
    if (valid == arrayOfSelectors.length) {
        method();
    } else {
        setTimeout(function () {
            elementDefer(method, arrayOfSelectors);
        }, 50);
    }
}

function isCustomerLoggedIn() { // checks if customer is logged in
    return checkoutConfig.isCustomerLoggedIn;
}

function addButton(selector, name, id) { // Adds a checkout button
    var html = '<div class="opt-button"><button class="opt-blue-Button button large primary inverted expanded" id="' + id + '">' + name + '</button></div>';
    jQuery(selector).after(html);
    jQuery('#' + id).click(function (e) {
        e.preventDefault();
        jQuery('.place-order-container button').click();
    });
}

function updateonchange(target, dest, opt) { // Takes the value of the input and applies it to a target
    var opt = opt || '';
    jQuery(target).change(function () {
        jQuery(dest).text(jQuery(this).val() + opt);
        jQuery('.opt-nodata-hidden').removeClass('opt-nodata-hidden');
    });
}

function getDataFromLocal (storage) {
    return JSON.parse(localStorage.getItem(storage));
}

function updateonsubmit(data, dest, opt) { // Takes the value of the input and applies it to a target
    var opt = opt || '';
        jQuery(dest).text(data + opt);
        jQuery('.opt-nodata-hidden').removeClass('opt-nodata-hidden');
}

function updateInitialDetails() {
    var data = getDataFromLocal('mage-cache-storage'),
        custData = data["checkout-data"]["shippingAddressFromData"];
    
        updateonsubmit(custData.firstname, '#opt-detailsfirstname');
        updateonsubmit(custData.lastname, '#opt-detailslastname');
        updateonsubmit(custData.company, '#opt-detailscomapny');
        updateonsubmit(custData.street[0], '#opt-detailsstreet', ',');
        updateonsubmit(custData.street[1], '#opt-detailsstreet1');
        updateonsubmit(custData.telephone, '#opt-detailsphone');
        updateonsubmit(custData.postcode, '#opt-detailspost');
        updateonsubmit(custData.city, '#opt-detailssuburb', ',');
        updateonsubmit(data["directory-data"]["AU"]["regions"][custData.region_id].code, '#opt-detailsstate', ',');
}

function updateonchangestate(target, dest, opt) { // Takes the value of the input and applies it to a target
    var opt = opt || '';
    jQuery(dest).text(jQuery(target).attr('data-title') + opt);
    jQuery('.opt-nodata-hidden').removeClass('opt-nodata-hidden');
}

function scrollTo (element) {
    //console.log('Scrolling to: ', element);
    if (jQuery('body').hasClass('opt-scrolltoready')) {
        jQuery([document.documentElement, document.body]).animate({
            scrollTop: jQuery('.opt-scrolltoready ' +element).offset().top
        }, 1000);
    }
}

function showHideAreas(toShow) {
    jQuery('.opt-initial').removeClass('opt-initial');
    //console.log('>>>> ' + toShow);
    switch (toShow) {
        case "opt-account":
            jQuery('#opt-delivery, #opt-deliveryNote, #opt-payment').addClass('opt-initial');
            scrollTo('#opt-account');
            break;
        case "opt-delivery":
            jQuery('#opt-account, #opt-payment').addClass('opt-initial');
            scrollTo('#opt-delivery');
            break;
        case "opt-deliveryNote":
            jQuery('#opt-account, #opt-payment').addClass('opt-initial');
            scrollTo('#opt-delivery');
            break;
        case "opt-payment":
            jQuery('#opt-delivery, #opt-deliveryNote, #opt-account').addClass('opt-initial');
            scrollTo('#opt-payment');
            setTimeout(function () {
                console.log('here');
                if (jQuery('.loading-mask').css('display') != 'none') {
                    showHideAreas('opt-payment');
                }
            },1000);
            break;
    }
}

function addEditItems(container) {
    jQuery(container + ' .opt-form-title').append('<div class="opt-Edit-Button"><p class="opt-a-Edit">Edit</p></div>');
    jQuery(container + ' .opt-a-Edit').click(function () {
        if (jQuery('body').hasClass('opt-loggedin') && !jQuery('body').hasClass('opt-loggedin-delivery')) {
            document.location = document.location.origin + '/customer/account/';
        } else {
            jQuery('.opt-initial').removeClass('opt-initial');
            showHideAreas(jQuery(this).closest('.opt-step-container').attr('id'));
        }
    });
}

/** Header Changes */

function updateHeader() {
    if (jQuery('.opt-back-link').length <= 0) {
        jQuery('.header.content').prepend('<div class="opt-back-link"><a href="/checkout/cart/">BACK</a></div>');
    }
}

/** Header Banner */

function addBenefitBar() {
    if (jQuery('.opt-benefit').length <= 0 && !jQuery('html').hasClass('mobile')) {
        var benefit = "<div class='opt-benefit'><p>Free delivery on orders over $99 + easy 30 day returns</p></div>";
        jQuery('.page-header').append(benefit);
    } else if (jQuery('.opt-benefit').length <= 0 && jQuery('html').hasClass('mobile')) {
        var benefit = "<div class='opt-benefit'><p>Free delivery over $99 + easy returns</p></div>";
        jQuery('.page-header').append(benefit);
    }
}

/** Form Your Details Functions */

function addTitle(titlenum, titlename) {
    return '<div class="opt-form-title"><div class="opt-step-number"><span>' + titlenum + '</span></div><strong class="opt-title"><span>' + titlename + '</span></strong></div>';
}

function addSignin() {
    elementDefer(function () {
        jQuery('#customer-email-fieldset').before('<div class="opt-signin-cont"><p>Continue as a guest below or <span><a href="opt-signin">Sign In</a></span></div>');
    }, '#customer-email-fieldset');
}

function updateNote() {
    elementDefer(function () {
        var note = 'We’ll send your order confirmation here.';
        jQuery('#opt-account #customer-email-fieldset > div.field.required.custom.ready > div > span.note').before('<div class="opt-note"><span>' + note + '</span></div>');
    }, [
        '#opt-account #customer-email-fieldset > div.field.required.custom.ready > div > span.note'
    ]);
}

function updateNewsletter() {
    elementDefer(function () {
        var news = '<span>Keep my updated with the latest Platypus news.</span>';
        jQuery('.checkout-container .newsletter-signup .label[for="checkout-newsletter-signup"]').html(news);
    }, [
        '.checkout-container .newsletter-signup .label[for="checkout-newsletter-signup"]'
    ]);
}

function addDetailsButton(selector, name, id) {
    var html = '<div class="opt-button"><button class="opt-blue-Button button large primary inverted expanded" id="' + id + '">' + name + '</button></div>';
    jQuery(selector).after(html);
    if (jQuery('#customer-email').val() != undefined && jQuery('#customer-email').val().length > 0) {
        jQuery('#customer-email').addClass('valid');
    }
    jQuery('#' + id).click(function (e) {
        e.preventDefault();
        var local = getDataFromLocal('mage-cache-storage');
        if (jQuery('body').hasClass('opt-showloginstate') && local != undefined &&  local["checkout-data"] != undefined && local["checkout-data"]["validatedEmailValue"] != local["checkout-data"]["checkedEmailValue"] && jQuery('.loading-mask').css('display') == 'none') {
            if (jQuery('#opt-account .opt-email-error').length <= 0) {
                jQuery('#opt-account .opt-button').before('<div class="opt-email-error" style="display: none; color: red;"><span>It looks like your email is incorrect please try a different one or continue as a guest.</span></div>');
                jQuery('.opt-email-error').fadeIn('medium');
                setTimeout(function () {
                    jQuery('.opt-email-error').fadeOut(function () {
                        jQuery('.opt-email-error').remove();
                    });
                }, 4000);
            }
        } else {
            if (jQuery('body').hasClass('opt-showloginstate')) {
                jQuery('.opt-5.opt-showloginstate #opt-account #customer-email-fieldset .actions-toolbar .primary button').click();
            } else {
                if (jQuery('#customer-email').hasClass('valid') && jQuery('#customer-email').val().length > 0) {
                    if (jQuery('.opt-step-container#opt-delivery').length <= 0) {
                        showHideAreas('opt-payment');
                    } else {
                        showHideAreas('opt-delivery');
                    }
                    initialstateshowemail(true);
                }
            }
        }
    });
}

function initialstateshowemail(email) {
    if (jQuery('.opt-initialemail').length <= 0) {
        elementDefer(function () {
            jQuery('#opt-account > .newsletter-signup').after('<div class="opt-initialemail"><span>' + jQuery('#customer-email').val() + '</span></div>');
        }, [
            '#opt-account > .newsletter-signup',
        ]);
    }

    if (email == true) {
        jQuery('.opt-initialemail span').text(jQuery('#customer-email').val());
    } else if (email == undefined) {
        jQuery('#opt-account .opt-form-title').addClass('opt-Tick');
    } else if (email && email.length > 0) {
        jQuery('.opt-initialemail span').text(email);
    } else {
        jQuery('.opt-initialemail span').text(jQuery('#customer-email').val());
    }
}

function createLoginswitch() {
    elementDefer(function () {
        jQuery('.opc-wrapper .form-login').wrapAll('<div class="opt-logincontainer"><div>');
        jQuery('#opt-account .opt-logincontainer').append('<div class="opt-form-toggle"><p>Already have an account? <span class="opt-underline opt-link-text">Sign in here</span></p></div>');
        jQuery('.opt-form-toggle').click(function () {
            if (jQuery('body').hasClass('opt-showloginstate')) {
                jQuery('body').removeClass('opt-showloginstate');
                jQuery('.opt-form-toggle').html('<p>Already have an account? <span class="opt-underline opt-link-text">Sign in here</span></p>');
            } else {
                jQuery('body').addClass('opt-showloginstate');
                jQuery('.opt-form-toggle').html('<p><span class="opt-underline opt-link-text">Return to Guest Checkout</span></p>');
            }
        });
    }, [
        "#opt-account > .opt-form-title"
    ]);
}

/** Init Your Details Sections */
function setupYourDetails() {
    elementDefer(function () {
        //console.log('>>>> Running setupdetails()');
        jQuery('#checkout-step-shipping > button, #checkout-step-shipping .form-login, #checkout-step-shipping > .newsletter-signup').wrapAll('<div class="opt-step-container" id="opt-account"></div>');
        jQuery('.or-separator').remove();
        jQuery('#opt-account').prepend(addTitle('1', 'YOUR DETAILS')); // Add step 1 Your Details area
        addEditItems('#opt-account'); // Add Edit Items Link
        addSignin(); // Add signin area
        // updateNote();
        updateNewsletter();
        addDetailsButton('#opt-account > .newsletter-signup', 'Continue', 'opt-detailcontinue');
        initialstateshowemail();
        createLoginswitch();
        jQuery('#customer-email').on('keydown', function (e) {
            if(e.keyCode === 13 && !jQuery('body').hasClass('opt-showloginstate')) {
                e.preventDefault();
                return false;
            }
        });
    }, [
        "#checkout-step-shipping > button",
        "#checkout-step-shipping .form-login",
        "#checkout-step-shipping > .newsletter-signup"
    ]);
}

function setupYourDetailsCandC(email) {
    elementDefer(function () {
        //console.log('>>>> Running setupdetails()');
        jQuery('#checkout-step-shipping > .newsletter-signup').wrapAll('<div class="opt-step-container" id="opt-account"></div>');
        jQuery('.or-separator').remove();
        jQuery('#opt-account').prepend(addTitle('1', 'YOUR DETAILS')); // Add step 1 Your Details area
        addEditItems('#opt-account'); // Add Edit Items Link
        addSignin(); // Add signin area
        // updateNote();
        updateNewsletter();
        addDetailsButton('#opt-account > .newsletter-signup', 'Continue', 'opt-detailcontinue');
        initialstateshowemail(email);
        createLoginswitch();
        jQuery('#customer-email').on('keydown', function (e) {
            if(e.keyCode === 13 && !jQuery('body').hasClass('opt-showloginstate')) {
                e.preventDefault();
                return false;
            }
        });
        jQuery('#customer-email').click().focus();
    }, [
        '#checkout-step-shipping > .newsletter-signup'
    ]);
}

function setupLoggedInDetails() {
    elementDefer(function () {
        //console.log('>>>> Running Setuploggedindetails()');
        jQuery('#checkout-step-shipping > button, #checkout-step-shipping .form-login, #checkout-step-shipping > .newsletter-signup').wrapAll('<div class="opt-step-container" id="opt-account"></div>');
        jQuery('#opt-account').prepend(addTitle('1', 'YOUR DETAILS')); // Add step 1 Your Details area
        addEditItems('#opt-account'); // Add Edit Items Link
        addSignin(); // Add signin area
        // updateNote();
        //updateNewsletter();
        var email = checkoutConfig.customerData.email || 'tick';
        initialstateshowemail(email);
        // Start Step 2
        showHideAreas('opt-delivery');
        jQuery('#customer-email').click().focus();
    }, [
        '#checkout-step-shipping > button, #checkout-step-shipping .form-login',
        '#opt-delivery',
        '.opt-loggedin-delivery'
    ]);
}

function setupLoggedInNoAddressDetails() {
    elementDefer(function () {
        //console.log('>>>> Running setupLoggedInNoAddressDetails()');
        jQuery('#checkout-step-shipping > button, #checkout-step-shipping .form-login, #checkout-step-shipping > .newsletter-signup').wrapAll('<div class="opt-step-container" id="opt-account"></div>');
        jQuery('#opt-account').prepend(addTitle('1', 'YOUR DETAILS')); // Add step 1 Your Details area
        addEditItems('#opt-account'); // Add Edit Items Link
        addSignin(); // Add signin area
        // updateNote();
        //updateNewsletter();
        var email = checkoutConfig.customerData.email || 'tick';
        initialstateshowemail(email);
        // Start Step 2
        showHideAreas('opt-delivery');
        jQuery('#customer-email').click().focus();
    }, [
        '#checkout-step-shipping > .newsletter-signup',
    ]);
}

function setupClickAndCollect() {
    jQuery('#checkout-step-shipping > button, #checkout-step-shipping .form-login, #checkout-step-shipping > .newsletter-signup').wrapAll('<div class="opt-step-container" id="opt-account"></div>');
}

/** DELIVERY FUNCTION */
function addPhoneNumberSubText() {
    var text = '<small class="opt-textright">For delivery information and updates.</small>';
    jQuery('.field[name="shippingAddress.telephone"] ._with-tooltip > small').before(text);
}

function deliveryingToCompany() {
    elementDefer(function () {
        jQuery('.field[name="shippingAddress.company"]').toggleClass('opt-hide');
        var deliverying = '<div class="opt-delivering"><span>Delivering to a company? </span><a tabindex="-1" id="opt-companyDelivery" href="#" class="opt-underline">Click here</a></div>';
        jQuery('.field[name="shippingAddress.company"]').before(deliverying);
        jQuery('#opt-companyDelivery').click(function () {
            jQuery('.opt-delivering').remove();
            jQuery('.field[name="shippingAddress.company"]').toggleClass('opt-hide');
        });
    }, [
        '.field[name="shippingAddress.company"]'
    ]);
}

function deliveryMethodHTML() {
    var optdelivery = {};
    for (var x = 0; x < jQuery('.checkout-summary:eq(0) .product-item .delivery-info').length; x++) {
        var item = jQuery('.checkout-summary .product-item .delivery-info:eq(' + x + ')').find('.method > span:eq(1)'),
            qty = jQuery('.checkout-summary .product-item:eq(' + x + ') .options-list > .value').text();
            //console.log(qty);
        if (optdelivery[item.text()] && optdelivery[item.text()] > 0) {
            optdelivery[item.text()] = optdelivery[item.text()] + parseInt(qty);
        } else {
            optdelivery[item.text()] = parseInt(qty);
        }

    }

    var html = '';

    for (var x in optdelivery) {
        var items = '';
        if (optdelivery[x] > 1) {
            items = "s";
        }
        html += '<p>' + optdelivery[x] + ' item' + items + ' ' + x + '</p>';
    }
    //console.log(html);
    return html;
}

function checkDeliveryMethod () {
    var count = 0;
    var checker = setInterval(function () {
        count++;
        jQuery('.opt-DeliveryMethod-Cont .opt-delivery-container *').remove();
        jQuery('.opt-DeliveryMethod-Cont .opt-delivery-container').append(deliveryMethodHTML());
        if (count > 10) {
            clearInterval(checker);
        }
    },1000);
}

function createDeliveryMethod() {
    jQuery('#opt-delivery').append('<div class="opt-DeliveryMethod-Cont"><div class="opt-show"><span><strong>Delivery Method: </strong></span></div><div class="opt-delivery-container"></div></div>');
    jQuery('.opt-DeliveryMethod-Cont').prepend('<div class="opt-heading"><span>Delivery Method</span><span id="opt-DeliveryMethod-Edit">Edit</span></div>');
    elementDefer(function () {
        jQuery('.opt-DeliveryMethod-Cont .opt-delivery-container').append(deliveryMethodHTML());
    }, [
        '.checkout-summary .product-item .delivery-info'
    ]);
}

function createDeliveryNote() {
    if (jQuery('.opt-DeliveryNote-Cont').length <= 0) {
        jQuery('.opt-DeliveryMethod-Cont').after('<div class="opt-DeliveryNote-Cont"><div class="opt-show"><span><strong>Delivery Note: </strong></span></div><div class="opt-deliverynote-container"></div></div>');
    }
    
    elementDefer(function () {
        if (jQuery('.opt-deliverynote-container > p').length <= 0) {
            jQuery('.opt-deliverynote-container').append('<p id="opt-deliveryNote-sel"></p>');
            jQuery('#opc-delivery_notes select').change(function () {
                var selectedNote = jQuery(this).val();
                if (selectedNote != '') {
                    jQuery('.opt-DeliveryNote-Cont #opt-deliveryNote-sel').text(selectedNote);
                    jQuery('.opt-DeliveryNote-Cont').addClass('opt-showDeliveryNote');
                } else {
                    jQuery('.opt-DeliveryNote-Cont').removeClass('opt-showDeliveryNote');
                }
            });
        }
    }, [
        '.opt-DeliveryMethod-Cont'
    ]);
}

function initialstateshowdelivery() {
    elementDefer(function () {
        jQuery('#opt-delivery > form').after('<div class="opt-initialdetails opt-nodata-hidden"><p class="opt-detailsname"><span id="opt-detailsfirstname"></span><span> </span><span id="opt-detailslastname"></span></p><p class="opt-detailscomapny"><span id="opt-detailscomapny"></span></p><p class="opt-detailsaddress"><span id="opt-detailsstreet"></span> <span id="opt-detailsstreet1"></span> <span id="opt-detailssuburb"></span> <span id="opt-detailsstate"></span> <span id="opt-detailspost"></span><br><span id="opt-detailsphone"></span></p></div>');
        jQuery('.field[name="shippingAddress.street.0"] > label > span').text('Address');
        jQuery('.field[name="shippingAddress.city"] > label > span').text('Suburb');
        jQuery('.field[name="shippingAddress.region_id"] > label > span').text('State');
        jQuery('.input-text[name="telephone"]').click(function () {
            jQuery(this).get(0).setSelectionRange(4,4)
        });
    }, [
        'div[name="shippingAddress.telephone"] input'
    ]);
}

function removeErrors (count) {
    var count = count || 0;
    setTimeout(function () {
        count++;
        if (count <= 20) {
            jQuery('.hosted-control').removeClass('braintree-hosted-fields-invalid');
            removeErrors(count);
        }
    },50);
}

function AddDeliveryButton(selector, name, id) { // Adds a checkout button
    var html = '<div class="opt-button"><button class="opt-blue-Button button large primary inverted expanded" id="' + id + '">' + name + '</button></div>';
    jQuery(selector).after(html);
    jQuery('#' + id).click(function (e) {
        e.preventDefault();
        removeErrors();
        jQuery('body').addClass('opt-removeerror');
        setTimeout(function () {
            jQuery('body').removeClass('opt-removeerror');
        },1000);
        setTimeout(function () {
            jQuery('#braintree, #afterpaypayovertime, #paypal_express').prop('checked',false);
            // jQuery('#sameship1, #sameship0').prop('checked', false).focus().blur().click();
            jQuery('.billing-address-same-as-shipping-block input').each(function () {
                if (jQuery(this).prop('checked') == false) {
                    jQuery(this).focus().blur().click();
                }
            });
            jQuery('.payment-method-title').on('click', function () {
                jQuery('.billing-address-same-as-shipping-block input').each(function () {
                    if (jQuery(this).prop('checked') == false) {
                        jQuery(this).focus().blur().click();
                    }
                });
            })
            jQuery('.billing-address-same-as-shipping-block input').on('click', function () {
                if (jQuery(this).prop('checked') == false) {
                    jQuery('body').addClass('opt-showinformation');
                } else {
                    jQuery('body').removeClass('opt-showinformation');
                }
            });
            jQuery('.payment-method').removeClass('_active');
            jQuery('#braintree').prop('checked', true).click();
            jQuery('.payment-method.payment-method-braintree').addClass('_active');
            if (jQuery('body').hasClass('opt-loggedin')) {
                if (jQuery('#shipping-new-address-form .ready._error').length <= 0 && jQuery('.shipping-address-item.selected-item').length > 0) {
                    jQuery('.place-order-container button').click();
                }
            } else {
                jQuery('.place-order-container button').click();
            }
        },200);
    });
}

function checkDisplays () {
    var check = true;
    jQuery('.validation-container.postcode-change-container').each(function () {
        if (jQuery(this).css('display') != 'none') {
            check = false;
        }
    })
    return check;
}

function setupDeliveryArea() {
    elementDefer(function () {
        //console.log('>>>> Running setupDeliveryArea()');
        jQuery('#co-shipping-form').wrapAll('<div class="opt-step-container" id="opt-delivery"></div>');
        if (jQuery('#opt-deliveryNote').length <= 0){
            jQuery('#opc-delivery_notes').wrapAll('<div class="opt-step-container" id="opt-deliveryNote"></div>');
        }
        jQuery('#opt-delivery').prepend(addTitle('2', 'DELIVERY'));
        addEditItems('#opt-delivery'); // Add Edit Items Link
        elementDefer(function () {
            jQuery('div[name="shippingAddress.postcode"]').insertBefore(jQuery('div[name="shippingAddress.region_id"]'));
        }, [
            'div[name="shippingAddress.region_id"]',
            'div[name="shippingAddress.postcode"]'
        ]);
        deliveryingToCompany();
        addPhoneNumberSubText();
        AddDeliveryButton('#opc-delivery_notes', 'Continue', 'opt-Delivery-Continue');
        // jQuery('body').on('click', '#braintree, #afterpaypayovertime, #paypal_express', function(e) {
        //     if (jQuery(this).attr('id') == 'afterpaypayovertime' || jQuery(this).attr('id') == 'braintree') {
        //         jQuery('#billing-address-same-as-shipping-braintree').click();
        //         jQuery('#billing-address-same-as-shipping-braintree').click();
        //     }
        //     // jQuery(this).parent('.payment-method').addClass('_active');
        //     // jQuery(this).closest('.payment-method').find('.opt-billing-toggle-cont input').prop('checked', false);
        //     // jQuery(this).closest('.payment-method').find('.opt-billing-toggle-cont input:eq(0)').prop('checked', true);
        //     // jQuery(this).closest('.payment-method').find('.billing-address-same-as-shipping-block input').prop('checked', false).focus().blur().click();
        // });
        jQuery('#opt-Delivery-Continue').click(function () {
            if (jQuery('#opt-delivery div.mage-error[generated]').length <= 0 && jQuery('#opt-delivery .field[name="shippingAddress.postcode"] input').val().length > 0 && checkDisplays() && jQuery('.field[name="shippingAddress.telephone"] input').val().match(/\d/g).length==11 && jQuery('.field[name="shippingAddress.street.0"] input').val().length > 0 && jQuery('.field[name="shippingAddress.postcode"] input').val().length > 0 && jQuery('.field[name="shippingAddress.city"] input').val().length > 0) {
                updateInitialDetails();
                showHideAreas('opt-payment');
                jQuery('body').removeClass('opt-initial-for-edit');
                jQuery('body').removeClass('opt-initial-for-deliverymethod');
            } else {
                if (jQuery('.opt-noneselected').length < 0) {
                    jQuery('#opt-Delivery-Continue').parent().before('<div class="opt-noneselected"><p>Please select an address to continue.</p></div>');
                }
                setTimeout(function () {
                    jQuery('.opt-noneselected').remove();
                },4000);
            }
        });
        createDeliveryMethod();
        createDeliveryNote();

        initialstateshowdelivery();
    }, [
        '#co-shipping-form'
    ]);
}

function addDeliverToText() {
    jQuery('#opt-delivery .opt-form-title').after('<div class="opt-deliver-to-text"><span>Deliver to</span></div>');
}

function AddnewAddress() {
    jQuery('#opt-delivery .field.addresses').after('<div class="opt-add-new-address"><span>Add new address</span></div>');
    jQuery('.opt-add-new-address').click(function () {
        jQuery('#opt-account > button').triggerHandler('click');
        jQuery('body').addClass('opt-modal-open');
        if (!jQuery('body').hasClass('opt-modal-moved')) {
            jQuery('body').addClass('opt-modal-moved');
            setTimeout(function () {
                jQuery('#opc-new-shipping-address').closest('.modal-popup').insertAfter(jQuery('.opt-add-new-address'));
                //jQuery(button).closemodaltrigger();
            }, 100);
        }
    });
}

function deliveryFunctionality() {
    jQuery('#opt-delivery').on('click', '.shipping-address-item:not(.opt-added-click)', function (e) {
        e.stopPropagation();
        jQuery(this).find('button.action-select-shipping-item').triggerHandler('click');
    }).addClass('opt-added-click');
}

function setupLoggedInDelivery() {
    elementDefer(function () {
        //console.log('>>>> Running setupLoggedinDelivery()');
        jQuery('#checkout-step-shipping > .field.addresses').wrapAll('<div class="opt-step-container" id="opt-delivery"></div>');
        if (jQuery('#opt-deliveryNote').length <= 0){
            jQuery('#opc-delivery_notes').wrapAll('<div class="opt-step-container" id="opt-deliveryNote"></div>');
        }
        jQuery('#opt-delivery').prepend(addTitle('2', 'DELIVERY'));
        addEditItems('#opt-delivery'); // Add Edit Items Link

        deliveryingToCompany();
        AddDeliveryButton('#opc-delivery_notes', 'Continue', 'opt-Delivery-Continue');
        jQuery('#opt-Delivery-Continue').click(function () {
            if (jQuery('body').hasClass('opt-loggedin')) {
                if (jQuery('#shipping-new-address-form .ready._error').length <= 0 && jQuery('.shipping-address-item.selected-item').length > 0 && checkDisplays() ) {
                    showHideAreas('opt-payment');
                    jQuery('body').removeClass('opt-initial-for-edit');
                    jQuery('body').removeClass('opt-initial-for-deliverymethod');
                } else {
                    jQuery('#opt-Delivery-Continue').parent().before('<div class="opt-noneselected"><p>Please select an address to continue.</p></div>');
                    setTimeout(function () {
                        jQuery('.opt-noneselected').remove();
                    },4000);
                }
            } else {
                if (jQuery('#shipping-new-address-form .read._error').length <= 0 && checkDisplays()) {
                    showHideAreas('opt-payment');
                    jQuery('body').removeClass('opt-initial-for-edit');
                    jQuery('body').removeClass('opt-initial-for-deliverymethod');
                } else {
                    jQuery('#opt-Delivery-Continue').parent().before('<div class="opt-noneselected"><p>Please select an address to continue.</p></div>');
                    setTimeout(function () {
                        jQuery('.opt-noneselected').remove();
                    },4000);
                }
            }
        });

        addDeliverToText();
        AddnewAddress();
        deliveryFunctionality();

        initialstateshowdelivery();
        createDeliveryMethod();
        createDeliveryNote();
    }, [
        '#checkout-step-shipping > .field.addresses',
        '#opc-delivery_notes'
    ]);
}

/** Payment Area */

function updatePromoPlaceholds(selector, name) {
    jQuery(selector).attr('placeholder', name);
}

function updateAfterPay() {
    jQuery('.opt-5 .afterpay-payment-method .afterpay-checkout-redirect .instalment-footer > span').text('You will be redirected to the Afterpay website. ');
    jQuery('.opt-5 .afterpay-payment-method .afterpay-note-tilte').text(jQuery('.opt-5 .afterpay-payment-method .afterpay-note-tilte').text().toLowerCase());
}

function toggleOn($selector) {
    //console.log('on ',$selector);
    $selector.find('.billing-address-same-as-shipping-block input').prop('checked', false).focus().blur().click();
}

function toggleOff($selector) {
    //console.log('off ',$selector);
    $selector.find('.billing-address-same-as-shipping-block input').prop('checked', true).focus().blur().click();
}


// NOT USED CURRENTLY AS CONTROL FUNCTIONALITY IS NOT CONDUCIVE
function billingToggleFunctionality() {
    jQuery('.opt-billing-toggle-cont input').click(function () {
        var val = jQuery(this).attr('id');

        var $el = jQuery(this).closest('.opt-billingaddresstoggle').parent();
        if (val.indexOf('sameship') >= 0) {
            // needs to be toggled on
            toggleOn($el);
        } else {
            toggleOff($el);
        }
    });
}

// NOT USED CURRENTLY AS CONTROL FUNCTIONALITY IS NOT CONDUCIVE
function addBillingToggle() {
    for (var i = 0; i < jQuery('.billing-address-same-as-shipping-block').length; i++) {
        jQuery('.billing-address-same-as-shipping-block:eq(' + i + ')').after('<div class="opt-billingaddresstoggle"><div class="opt-billing-header"><span class="opt-title">Billing Address</span></div><div class="opt-billing-toggle-cont"><div class="field"> <input type="radio" name="optshippingtoggle' + i + '" class="radio" id="sameship' + i + '""> <label class="label" for="sameship' + i + '">Same as shipping address</label> </div><div class="field"> <input type="radio" name="optshippingtoggle' + i + '" class="radio" id="diffship' + i + '""> <label class="label" for="diffship' + i + '">Use a different billing address</label> </div></div></div>');
        jQuery('#sameship' + i).prop('checked', true);
    }
    billingToggleFunctionality();

    setTimeout(function () {
        if (jQuery('.opt-billingaddresstoggle').length <= 0) {
            addBillingToggle();
        }
    },1000);
    // jQuery('.payment-method-title > input').click(function () {
    //     jQuery('#sameship1, #sameship0').prop('checked', false).focus().blur().click();
    // });
}

function addCCVTooltip() {
    jQuery('.payment-method.payment-method-braintree .payment-method-content .checkout-agreements-block').after('<div class="opt-ccvtooltip"><span>What is my CCV?</span></div>');
    jQuery('.opt-ccvtooltip').click(function () {
        jQuery('.cvv .field-tooltip .field-tooltip-content').toggleClass('_active').toggle();
    });
}

function updatePayPal() {
    jQuery('.payment-method.item .payment-method-title').after('<div class="opt-Paypal-message"><span>You will be redirected to the PayPal website.</span></div>');
    jQuery('.payment-method.item .payment-method-title .payment-icon').attr('src', '//cdn.optimizely.com/img/6092490016/2ecf5aa213c1429e9717b9e0019371a8.png');
}


function setupPaymentArea(step) {
    elementDefer(function () {
        var placeordermessage = '<div class="opt-tnc-message">By clicking Place Order, you agree to the <span></span><a target="_blank" href="https://www.skechers.com.au/terms-and-conditions">Terms & Conditions</a></div>';
        //console.log('>>>> Running SetupPaymentArea()');
        jQuery('#payment').wrapAll('<div class="opt-step-container" id="opt-payment"></div>');
        jQuery('#opt-payment').prepend(addTitle(step, 'PAYMENT'));
        jQuery('.payment-option.discount-code, .payment-option.giftcardaccount ').wrapAll('<div class="opt-giftCardSection"></div>');
        jQuery('.opt-giftCardSection').prepend('<div class="opt-giftTitle"><span>Apply Gift Card or Promo Code</span></div>');
        jQuery('.opt-giftTitle').click(function () {
            jQuery('.opt-giftCardSection').toggleClass('opt-open');
        });
        jQuery('.payment-option.discount-code').appendTo('.opt-giftCardSection');
        jQuery('.payment-option.discount-code').before('<div class="opt-bluesep"></div>');

        updatePromoPlaceholds('#giftcard-code', 'Gift Card Number');
        updatePromoPlaceholds('#giftcard-pin', 'Pin');
        updatePromoPlaceholds('#discount-code', 'Promo Code');

        elementDefer(function () {
            addButton('.payment-method.item .payment-method-title', 'Pay Now With PayPal', 'opt-paypalbutton');
            //jQuery('#opt-paypalbutton').after(placeordermessage);
        }, [
            '.payment-method.item .payment-method-title'
        ]);
        elementDefer(function () {
            addButton('.checkout-payment-method .afterpay-payment-method .payment-method-content .payment-method-content', 'Pay Now With Afterpay', 'opt-paynowwithafterpay');
            //jQuery('#opt-paynowwithafterpay').after(placeordermessage);
        }, [
            '.checkout-payment-method .afterpay-payment-method .payment-method-content .payment-method-content'
        ]);
        elementDefer(function () {
            addButton('.payment-method.payment-method-braintree .payment-method-content .checkout-agreements-block', 'Place Order', 'opt-placeorderbutton');
            jQuery('#opt-placeorderbutton').after(placeordermessage);
        }, [
            '.payment-method.payment-method-braintree .payment-method-content .checkout-agreements-block'
        ]);

        addCCVTooltip();

        updateAfterPay();

        updatePayPal();
        
        elementDefer(function () {
            jQuery('div[name="billingAddressbraintree.postcode"]').insertBefore(jQuery('div[name="billingAddressbraintree.region_id"]'));
        }, [
            'div[name="billingAddressbraintree.region_id"]',
            'div[name="billingAddressbraintree.postcode"]'
        ]);
        elementDefer(function () {
            jQuery('div[name="billingAddressafterpaypayovertime.postcode"]').insertBefore(jQuery('div[name="billingAddressafterpaypayovertime.region_id"]'));
        }, [
            'div[name="billingAddressafterpaypayovertime.region_id"]',
            'div[name="billingAddressafterpaypayovertime.postcode"]'
        ]);

    }, [
        '#payment',
        '#co-transparent-form-braintree #payment_form_braintree'
    ]);
}

function mobilePaymentSectionUpdates() {
    elementDefer(function () {
        jQuery('.block.discount').appendTo('.mobile .opt-5.checkout-index-index .promo-gift>.content');
        jQuery('.block.discount').before('<div class="opt-bluesep"></div>');
        jQuery('.mobile .opt-5 .form.giftcard .field:not(.custom) .control span').text('Check Balance');
        var giftCardText = 'Enter your gift card number & pin and click apply to update your order.',
            promoCodeText = 'Enter your promotional code and click apply to update your order.';

        jQuery('.mobile .opt-5.checkout-index-index .promo-gift > .content > p').text(giftCardText);
        jQuery('.mobile .opt-5 .promo-gift .content > .block.discount').before('<p class="text"><span>' + promoCodeText + '</span></p>');

        jQuery('.mobile .opt-5.checkout-index-index .promo-gift.active > .title span').text('Apply Gift Card or Promo Code');
    }, [
        ".block.discount",
        ".mobile .opt-5 .form.giftcard .field:not(.custom) .control span"
    ]);
}

function mobileFooter() {
    jQuery('.checkout-index-index .secure-pay-wrapper').before('<div class="secure-pay-wrapper opt-securePay"><div class="info-block"><strong class="block-title">Secure ways to pay</strong> <img src="//cdn.optimizely.com/img/6092490016/4ef88c891f864f38967caffd7de895bd.png" "="" alt="Secure pay"></div><div class="trust-block"><img src="//cdn.optimizely.com/img/6092490016/39606060f8244231aa2b13d529603735.png" alt=""> <strong class="trust-label">SHOP IN CONFIDENCE</strong><p>We take credit card security very seriously. <br>All transactions are transmitted via a secure certificate and we never store your credit card details.</p></div></div>');
}


/** Form Setup */



/** Sidebar */

function addSidebarTitle() {

    elementDefer(function () {
        if (jQuery('.opt-sidebar-header').length <= 0) {
            jQuery('.checkout-container .checkout-summary>.items>.title').before('<div class="opt-sidebar-header"><strong class="opt-title"><span>Summary</span><a href="/checkout/cart/" class="link _right">Edit Cart</span></strong></div>');
        }
        jQuery('.opt-5.checkout-index-index .secure-pay-wrapper .info-block img').attr('src', '//cdn.optimizely.com/img/6092490016/4ef88c891f864f38967caffd7de895bd.png');
        jQuery('.opt-5.checkout-index-index .secure-pay-wrapper .trust-block > img').attr('src', '//cdn.optimizely.com/img/6092490016/39606060f8244231aa2b13d529603735.png');

    }, [
        ".checkout-container .checkout-summary>.items>.title span"
    ]);
}

function modal() {
    elementDefer(function () {
        jQuery("body").append("<div class='opt-overlay' style='display: none;'><div class='opt-modal'><div class='opt-modal-header'><div class='opt-modal-head'></div><span>This will take you back to your cart<span class='opt-hide'> to change your delivery method</span>.</span></div><div class='opt-modal-buttons'><button class='btn btn-primary opt-stay-here'>STAY HERE</button><button href='/checkout/cart/' class='btn btn-primary opt-modal-back-to-cart'>BACK TO CART</button></div></div></div></div>");

        jQuery('.opt-modal-back-to-cart').click(function () {
            document.location = document.location.origin + jQuery(this).attr('href');
        });

        jQuery(".opt-modal").append("<div class='opt-close'></div> ");

        jQuery(".opt-modal").click(function (event) {
            event.stopPropagation();
        });

        jQuery('body').on('click', 'div.link > a.link[title="Edit"], .opt-sidebar-header .opt-title a.link, #opt-DeliveryMethod-Edit', function (e) {
            e.preventDefault();
            jQuery('body').removeClass('opt-editcartclick');
            if (e.target.classList.value == 'link _right') {
                jQuery('body').addClass('opt-editcartclick');
            }
            jQuery(".opt-overlay").fadeIn("slow");
            jQuery("body").css({
                "height": "100%",
                "overflow": "hidden"
            });
        });

        jQuery(".opt-close, .opt-overlay, .opt-stay-here").click(function () {
            jQuery(".opt-overlay").fadeOut("slow", function () {
                jQuery('body').removeClass('opt-editcartclick');
            });
            jQuery("body").css({
                "height": "auto",
                "overflow": "inherit"
            });
        });
    }, [
        '#opt-payment',
        '#co-transparent-form-braintree #payment_form_braintree'
    ]);
}

function updateSubTotalArea() {
    elementDefer(function () {
        jQuery('.opt-5 .checkout-container .table-totals .shipping .mark span').text('Shipping');
        jQuery('.opt-5 .checkout-container .table-totals .totals.sub .mark').text(jQuery('.opt-5 .checkout-container .checkout-summary>.items>.title').text().trim().split('in')[0]);
        jQuery('.opt-5 .checkout-container .table-totals .grand .mark strong').text('Total');
    }, [
        '.opt-5 .checkout-container .table-totals .shipping .mark span',
        '.opt-5 .checkout-container .table-totals .totals.sub .mark',
        '.opt-5 .checkout-container .table-totals .grand .mark strong',
        '.opt-5 .checkout-container .checkout-summary>.items>.title'
    ]);
}

function updateSidebar() {
    addSidebarTitle();
    updateSubTotalArea();
    modal();
}


/** Init */

// Need to detect if its click and collect and show all content 

function detectStep() {
    var step = 'none';
    jQuery('#checkoutSteps').find(' > li').each(function () {
        if (jQuery(this).css('display') != 'none') {
            step = jQuery(this).attr('id');
        }
    });
    return step;
}

function detectCandC() {
    var data = checkoutConfig.quoteItemData,
        CandCOnly = true;
    for (var i = 0; i < data.length; i++) {
        if (data[i].shipping_method != 'collect_collect') {
            CandCOnly = false;
            return CandCOnly;
        }
    }
    return CandCOnly;
}

function mainJs() {
    // First detect the type of checkout we are working on.. ie. Click and collect, logged in etc.
    var loggedIn = checkoutConfig.isCustomerLoggedIn,
        step = detectStep(),
        CandC = detectCandC(),
        stepNums = {
            "Your Details": 1,
            "Delivery": 2,
            "Payment": 3
        };
    //console.log('Loading In', loggedIn, step);
    if (step == 'none') {
        setTimeout(function () {
            mainJs();
        }, 50);
        return;
    }
    jQuery('body').addClass('opt-initial-for-deliverymethod');
    switch (loggedIn) {
        case true:
            jQuery('body').addClass('opt-loggedin');
            if (step == 'shipping' || step == 'opc-delivery_notes') {
                //console.log('>> Running Logged in starting at your Delivery');
                if (CandC == false && jQuery('.shipping-address-items').length <= 0) {
                    setupLoggedInNoAddressDetails();
                    //console.log(">>> Not Click and Collect and Shipping details 0. Adding Delivery");
                    jQuery('body').addClass('opt-loggedin-delivery');
                    
                    setupLoggedInDelivery();
                    window.optCheckerCount = 0;
                    var loadingchecker = setInterval(function () {
                        if (jQuery('.loading-mask').css('display') == 'none') {
                            if (jQuery('.shipping-address-items .shipping-address-item').length <= 0) {
                                setupDeliveryArea();
                                clearInterval(loadingchecker);
                            }
                        } else if (optCheckerCount == 100) {
                            clearInterval(loadingchecker);
                        }
                    },100);
                } else if (CandC == false) {
                    setupLoggedInDetails();
                    //console.log(">>> Not Click and Collect Only. Adding Delivery");
                    jQuery('body').addClass('opt-loggedin-delivery');
                    setupLoggedInDelivery();
                } else {
                    setupYourDetailsCandC();
                    //console.log(">>> Click and Collect Only. Not Adding Delivery");
                    stepNums.Payment = 2;
                }
                setupPaymentArea(stepNums.Payment);
                elementDefer(function () {
                    showHideAreas('opt-delivery');
                    //addBillingToggle();
                    jQuery('body').addClass('opt-scrolltoready');
                }, [
                    '#opt-delivery',
                    '#opt-payment'
                ]);
            } else if (step == 'payment') {
                //console.log('>> Running Logged in starting at your Payment');
                setupLoggedInDetails();
                if (CandC == false) {
                    //console.log(">>> Not Click and Collect Only. Adding Delivery");
                    setupLoggedInDelivery();
                } else {
                    setupYourDetailsCandC(JSON.parse(localStorage.getItem('mage-cache-storage')).customer.email);
                    //console.log(">>> Click and Collect Only. Not Adding Delivery");
                    stepNums.Payment = 2;
                }
                setupPaymentArea(stepNums.Payment);
                elementDefer(function () {
                    showHideAreas('opt-account');
                    jQuery('#opt-delivery').addClass('opt-Tick');
                    jQuery('#opt-account').addClass('opt-Tick');
                    //addBillingToggle();
                    jQuery('body').addClass('opt-scrolltoready');
                }, [
                    '#opt-payment',
                ]);
            }
            break;
        case false:
            jQuery('body').addClass('opt-not-loggedin');
            if (step == 'shipping' || step == 'opc-delivery_notes') {
                //console.log('>> Running Not Logged in starting at your Details');
                setupYourDetails();
                if (CandC == false) {
                    //console.log(">>> Not Click and Collect Only. Adding Delivery");
                    setupDeliveryArea();
                } else {
                    //console.log(">>> Click and Collect Only. Not Adding Delivery");
                    stepNums.Payment = 2;
                }
                setupPaymentArea(stepNums.Payment);
                elementDefer(function () {
                    showHideAreas('opt-account');
                    //addBillingToggle();
                    jQuery('body').addClass('opt-scrolltoready');
                }, [
                    '#opt-account',
                    '#co-transparent-form-braintree #payment_form_braintree'
                ]);
            } else if (step == 'payment') {
                //console.log('>> Running Not Logged in starting at your Payment');
                setupYourDetails();
                if (CandC == false) {
                    //console.log(">>> Not Click and Collect Only. Adding Delivery");
                    setupDeliveryArea();
                } else {
                    //console.log(">>> Click and Collect Only. Not Adding Delivery");
                    stepNums.Payment = 2;
                }
                setupPaymentArea(stepNums.Payment);
                elementDefer(function () {
                    showHideAreas('opt-account');
                    //addBillingToggle();
                    jQuery('body').addClass('opt-scrolltoready');
                }, [
                    '#opt-payment',
                    '#opt-account',
                    '#co-transparent-form-braintree #payment_form_braintree'
                ]);
            }
            break;
    }
}

/** MOBILE FUNCTIONS */
function updatePaymentSummary() {
    elementDefer(function () {
        jQuery('#opt-payment .checkout-summary, #opt-payment .place-order-container').wrapAll('<div class="opt-summary-cont" id="opt-mobile-summary"></div>');
        jQuery('#opt-mobile-summary .place-order-container button').text('Place Order');
        jQuery('#opt-payment #opc-sidebar').insertAfter(jQuery('.mobile .opt-5.checkout-index-index .opc-wrapper'));
        jQuery('.paypal-payment-method > .title').wrapAll('<div class="payment-method-title field choice"></div>');
        if (jQuery('.paypal-payment-method > small').length > 0) {
            jQuery('.paypal-payment-method > small').prependTo(jQuery('.paypal-payment-method .payment-method-content'));
        }
    }, [
        ".opt-5 #opt-payment .checkout-summary",
        ".opt-5 #opt-payment .place-order-container"
    ]);
}

function mobileJs() {
    updatePaymentSummary();
}

function init() {
    if (!window.opt_running) {
        window.opt_running = true;
        jQuery('body').addClass('opt-5 opt-5-1');
        // Always on changes
        updateHeader();
        updateSidebar();
        elementDefer(function () {
            jQuery('body').on('click', '.validation-container.postcode-change-container button.button.primary', function () {
                var checking = setInterval(function () {
                    if (jQuery('.loading-mask').css('display') == 'none') {
                        clearInterval(checking);
                        jQuery('.opt-DeliveryMethod-Cont').remove();
                        createDeliveryMethod();
                    }
                },100);
            })
        }, [
            ".validation-container.postcode-change-container"
        ])
        if (jQuery('html').hasClass('mobile')) {
            if (window.location.hash != '#payment') {
                window.location = window.location + '#payment';
            }
            elementDefer(function () {
                mainJs();
                mobileJs();
                mobilePaymentSectionUpdates();
                mobileFooter();
            }, [
                "#checkoutSteps > li"
            ]);
        } else {
            elementDefer(function () {
                mainJs();
            }, [
                "#checkoutSteps > li"
            ]);
        }
    }
}

defer(function () {
    init();
  	// window.location.hash = "#payment";
}, 'body');
}

function mobilecss () {
    jQuery('head').append('<style>.opt-5.checkout-index-index .fieldset.address .field.phone-region + .field[name$="telephone"] .label,.checkout-index-index .fieldset.address ._double + ._double .label{left: 12px;}.opt-showinformation.opt-5 .payment-method-billing-address .billing-address-details{display: block; padding-left: 0;}.opt-5 .checkout-payment-method .payment-method-billing-address .billing-address-details .action-edit-address{color: #fff; border-color: #0063ba; background-color: #0063ba;}.opt-5 .checkout-payment-method .payment-method-billing-address .billing-address-details .action-edit-address:hover{border-color: #0063ba; color: #0063ba; background-color: #fff;}/** GLOBAL CSS **/.opt-removeerror .braintree-hosted-fields-invalid{display: none !important; visibility: hidden !important;}.opt-underline{text-decoration: underline;}.opt-textright{text-align: right; float: right;}.opt-Tick .opt-title{position: relative;}.opt-5 .footer > .footer-block{display: none;}.opt-5 .opt-email-error{color: #fb0103; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif; font-size: 14px; margin-top: 15px; font-weight: bold;}.opt-5 .footer > .footer-info{padding-top: 20px; padding-bottom: 20px;}.opt-5 .header-item.menu._left{display: none;}.opt-5 .opt-noneselected{margin-top: 20px; color: #fb0103; margin-bottom: -10px;}.opt-5 .payment-method button[data-bind="click: cancelAddressEdit"]{display: none;}body:not(.opt-editcartclick) .opt-modal .opt-hide{display: contents;}.opt-hide{display: none;}.opt-bluesep{width: 100%; border-bottom: 2px solid #8FB6DF; padding: 0px 22px !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 10px 0; display: none;}.opt-Edit-Button{font-size: 12px; text-decoration: underline; font-weight: bold; margin-left: auto; cursor: pointer;}.opt-5 ._clearfix._sticky{top: 0 !important;}.opt-nodata-hidden{display: none !important;}.opt-overlay{width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.6); position: fixed; z-index: 9001; top: 0px;}.opt-modal{position: absolute; background-color: white; width: 80%; max-width: 600px; top: 50%; left: 50%; -webkit-transform: translate(-50%, -50%); display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; -webkit-box-orient: vertical; -webkit-box-direction: normal; -webkit-flex-direction: column; -ms-flex-direction: column; flex-direction: column; padding: 40px;}.opt-modal .opt-modal-header{margin-bottom: 24px; font-size: 20px; font-weight: bold; color: #434343;}.opt-modal .opt-modal-buttons{width: 100%; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex;}.opt-modal .opt-modal-buttons button{width: 100%; max-width: 250px; font-size: 16px; margin: auto; background-color: #0263BA; color: white; border: 2px solid #0263BA;}.opt-modal .opt-close{position: absolute; top: -30px; right: 0px; width: 20px; height: 20px; font-size: 20px; color: white; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif; text-align: center; cursor: pointer; padding: 10px;}.opt-modal .opt-close:after,.opt-modal .opt-close:before{content: ""; width: 20px; height: 2px; background: white; position: absolute; -webkit-transform: rotate(45deg); -ms-transform: rotate(45deg); transform: rotate(45deg); right: 0; top: 10px;}.opt-modal .opt-close:before{-webkit-transform: rotate(135deg); -ms-transform: rotate(135deg); transform: rotate(135deg);}.opt-modal .opt-modal-buttons button.opt-modal-back-to-cart{background-color: white; color: #0263BA;}.opt-5.opt-initial-for-deliverymethod .opt-step-container.opt-initial .opt-DeliveryMethod-Cont,.opt-5.opt-initial-for-deliverymethod .opt-step-container.opt-initial:not(#opt-account) .opt-a-Edit{display: none;}.opt-5 .opt-tnc-message{font-size: 12px; margin-top: 5px;}.opt-5 .opt-tnc-message a{text-decoration: underline;}/** General Layout and Styling **/.opt-5 .checkout-shipping-address{display: block !important;}.opt-5 .page-main > .columns{max-width: 1080px; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: justify; -webkit-justify-content: space-between; -ms-flex-pack: justify; justify-content: space-between; -webkit-box-flex: 0; -webkit-flex-grow: 0; -ms-flex-positive: 0; flex-grow: 0; padding-top: 40px;}.opt-5 .page-wrapper{padding-top: 163px !important;}.opt-5 .page-main > .columns:after{content: none;}.opt-5 .main-wrapper{background: white; border-color: white; border-left: 0; border-right: 0; width: 580px; -webkit-box-flex: 0; -webkit-flex-grow: 0; -ms-flex-positive: 0; flex-grow: 0; border-top: 0;}.opt-5 .opc-wrapper .step-content{margin-bottom: 0;}.opt-5.checkout-index-index .column.main{padding-left: 0; padding-right: 0; padding-top: 0;}.opt-5.checkout-index-index .sidebar .checkout-container,.opt-5.checkout-index-index .sidebar .secure-pay-wrapper{max-width: initial; width: 100%;}.opt-5.checkout-index-index .checkout-summary .product-item.-highlited{border: 1px solid red !important; background: white;}.opt-5.checkout-index-index .checkout-summary .product-item.-highlited{margin-bottom: -1px;}.opt-5.checkout-index-index .checkout-summary .product-item.-highlited ~ .product-item:not(.-highlited){margin-top: 10px;}.opt-5 .checkout-container #opc-sidebar{max-width: initial; float: none;}.opt-5 .opc-progress-bar{display: none;}.opt-5 .page-title{display: none;}.opt-5 .column.main > #checkout > .modal-custom.opc-sidebar{display: none;}.opt-5 .checkout-sidebar .place-order-container{display: none;}.opt-5 #opt-payment:not(.opt-initial) > .checkout-payment-method{display: block !important;}.opt-5 .page.messages .message{display: block; margin-left: 0; margin-right: 0; margin-top: 0;}.opt-5 .message.info{background: white; color: #0063ba; border: 1px solid #0063ba; margin-bottom: 20px;}.opt-5 .message.error{margin: 0; margin-bottom: 20px; border: 1px solid #E02E2B; background: white; color: #E02E2B;}html:not(.mobile) .opt-5.checkout-index-index .fieldset.address .field[name="shippingAddress.region_id"],html:not(.mobile) .opt-5.checkout-index-index .fieldset.address .field[name="billingAddressbraintree.region_id"],html:not(.mobile) .opt-5.checkout-index-index .fieldset.address .field[name="billingAddressafterpaypayovertime.region_id"]{padding-right: 0; float: right; clear: initial;}html:not(.mobile) .opt-5.checkout-index-index .fieldset.address .field[name="shippingAddress.postcode"],html:not(.mobile) .opt-5.checkout-index-index .fieldset.address .field[name="billingAddressbraintree.postcode"],html:not(.mobile) .opt-5.checkout-index-index .fieldset.address .field[name="billingAddressafterpaypayovertime.postcode"]{padding-left: 0; float: left; padding-right: 9px;}.opt-5 .filled:not(._error) .input-text[aria-invalid="false"]:not(:focus),.opt-5 .filled:not(._error)[name="shippingAddress.street.0"] .input-text:not(:focus){background-image: url(//cdn.optimizely.com/img/6092490016/a49c2b7d20a24f809ecfd75074e1e22f.png) !important; -webkit-background-size: 12px 12px !important; background-size: 12px 12px !important; background-repeat: no-repeat !important; background-position: right 20px center !important;}.opt-5 ._error .input-text[aria-invalid="true"],.opt-5 ._error .input-text{background-image: url(//cdn.optimizely.com/img/6092490016/ac173ad1b6cc40de937f25b3f51d7989.png) !important; -webkit-background-size: 12px 12px !important; background-size: 12px 12px !important; background-repeat: no-repeat !important; background-position: right 20px center !important;}/* .opt-5 .billing-address-same-as-shipping-block{display: none;}*//** DESKTOP CSS *//** Header **/.opt-5 .nav-sections{display: none;}.opt-5 .header .header-item.minicart{display: none;}.opt-5 .page-header .header.content{display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; position: relative; max-width: 1440px;}.opt-5 .header > .logo{margin: 0 auto;}.opt-5 .opt-back-link{position: absolute; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; color: #0263BA; font-weight: bold; font-size: 16px; line-height: 19px; padding-left: 10px; margin-left: 15px; margin-top: 5px;}.opt-5 .opt-back-link img{display: none;}.opt-5 .opt-back-link a:after{content: ""; background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA5LjYwMyA1LjkyMiI+PHBhdGggZmlsbD0iIzU5QjdCMyIgZD0iTTkuNjAzIDEuMTJMOC40OCAwIDQuOCAzLjY4IDEuMTIzIDAgMCAxLjEybDQuOCA0LjgwMiIvPjwvc3ZnPg==); color: white; -webkit-filter: brightness(0)invert(1); filter: brightness(0)invert(1); -webkit-background-size: 15px 15px; background-size: 15px 15px; background-position: center center; width: 20px; height: 20px; position: absolute; left: -12px; top: 0; margin-top: -2px; background-repeat: no-repeat; -webkit-transform: rotate(90deg); -ms-transform: rotate(90deg); transform: rotate(90deg)}.opt-5 .fieldset .field select{background-size: 15px;}/** Benefit Bar **/.opt-5 .opt-benefit{width: 100%; background: #8FB6DF; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; color: white; color: #FFFFFF; font-size: 15px; font-weight: 600; line-height: 15px; padding: 9px 0; text-transform: uppercase; text-align: center;}.opt-5 .opt-benefit p{margin-bottom: 0;}/** Sidebar **/.opt-5 .sidebar{background: white; width: 440px; -webkit-box-flex: 0; -webkit-flex-grow: 0; -ms-flex-positive: 0; flex-grow: 0; padding-left: 0; padding-right: 0; padding-top: 0;}.opt-5 .checkout-container .opc-sidebar{padding: 45px 40px 30px 40px; border: 1px solid rgba(151, 151, 151, 0.11);}.opt-5 .checkout-container .checkout-summary{margin-top: 0;}.opt-5 .checkout-container .checkout-summary > .items > .title{display: none;}.opt-5 .checkout-container .checkout-summary > .items .opt-sidebar-header{border-bottom: 2px solid black; padding-bottom: 5px;}.opt-5 .checkout-container .checkout-summary > .items .opt-title{font-size: 18px; text-transform: uppercase; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: end; -webkit-align-items: flex-end; -ms-flex-align: end; align-items: flex-end; -webkit-box-pack: justify; -webkit-justify-content: space-between; -ms-flex-pack: justify; justify-content: space-between;}.opt-5 .checkout-container .checkout-summary > .items .opt-title span{line-height: 1;}.opt-5 .checkout-container .checkout-summary > .items .opt-title a{font-size: 12px; text-decoration: none; font-weight: normal; color: #59B7B3;}.opt-5.checkout-index-index .checkout-summary .product-item{border-bottom: 0.5px solid #E5E5E5; margin-bottom: 10px;}.opt-5.checkout-index-index .checkout-summary .product-item:last-child{margin-bottom: 0;}.opt-5 .checkout-container .product-item > .image{width: 110px;}.opt-5 .checkout-container .product-item > .details{margin-left: 110px;}.opt-5 .checkout-container .product-item > .details > .name{font-weight: 600; font-size: 14px;}.opt-5 .checkout-container .product-item > .details .label.qty{margin-bottom: 10px;}.opt-5 .checkout-container .product-item > .details .label.qty span{font-weight: bold;}.opt-5 .checkout-container .options-list .item{line-height: 18px;}.opt-5 .checkout-container .product-item > .details > .options-list{font-size: 12px;}.opt-5 .checkout-container .options-list > .item > .label{font-weight: bold; font-size: 12px;}.opt-5 .price-including-tax .price,.opt-5 .price-excluding-tax .price{font-size: 12px; font-weight: 400;}.opt-5.checkout-index-index .checkout-summary .product-item .details .delivery-info{width: 100%; padding-top: 1px; clear: initial;}.opt-5.checkout-index-index .checkout-summary .product-item .details .delivery-info .link{color: #434343;}.opt-5 .checkout-container .checkout-summary > .items{margin-bottom: 0; padding-bottom: 0;}/** Totals Area **/.opt-5 .checkout-container .table-totals .mark{text-align: left; padding: 5px 0; font-weight: 100; font-size: 14px;}.opt-5 .checkout-container .table-totals .amount{padding: 5px 0; font-size: 14px;}.opt-5 .checkout-container .table-totals .shipping .mark span{font-weight: 100; font-size: 14px;}.opt-5 .checkout-container .table-totals .grand .mark strong{font-size: 14px;}.opt-5.checkout-index-index .secure-pay-wrapper .info-block .block-title{display: none;}.opt-5.checkout-index-index .secure-pay-wrapper .info-block img{width: 100%; max-width: 300px;}.opt-5.checkout-index-index .secure-pay-wrapper .trust-block{display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-flex-wrap: wrap; -ms-flex-wrap: wrap; flex-wrap: wrap; text-align: center;}.opt-5.checkout-index-index .secure-pay-wrapper .trust-block > img{width: 90px;}.opt-5.checkout-index-index .secure-pay-wrapper .trust-block > .trust-label{margin: 0;}.checkout-index-index .secure-pay-wrapper .trust-block p{margin-top: 20px;}/** FORM **/.opt-5 .opc-wrapper .step-title{display: none;}.opt-5 .opt-step-container .opt-form-title{display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center;}.opt-5 .opt-step-container .opt-form-title{border-bottom: 2px solid black;}.opt-5 .opt-step-container:not(#opt-payment) .opt-form-title{margin-bottom: 20px;}.opt-5.checkout-index-index .fieldset.address .field{margin-bottom: 10px;}.fieldset .field.required > .label:after,.fieldset > .fields > .field.required > .label:after,.fieldset .field._required > .label:after,.fieldset > .fields > .field._required > .label:after,.delivery-block .field.required > .label:after,.delivery-block .field._required > .label:after,.shipping-availability .field.required > .label:after,.shipping-availability .field._required > .label:after{display: none;}.opt-5 .opt-step-container .opt-form-title .opt-step-number{height: 33px; width: 33px; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; color: #0263BA; border: 1px solid #0263BA; border-radius: 50%; font-weight: bold; font-size: 20px;}.opt-5 .opt-step-container .opt-form-title .opt-title{font-size: 18px; margin-left: 18px;}.opt-5 .opt-step-container{padding: 40px 40px 35px 40px; background-color: white; margin-bottom: 20px;}.opt-5 .opc-wrapper .fieldset .field:not(.choice) > .label,.opt-5 .opc-wrapper .delivery-block .field:not(.choice) > .label,.opt-5 .opc-wrapper .shipping-availability .field:not(.choice) > .label{color: #434343; font-size: 14px; line-height: 17px; font-weight: normal; font-family: "Open Sans"; text-transform: initial;}.opt-5 div.mage-error[generated]{font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif; font-size: 12px; text-transform: initial; text-align: right;}.opt-5 div.mage-error[generated]:before{display: none;}.opt-5 .fieldset .field.custom input,.opt-5 .delivery-block .field.custom input,.opt-5 .shipping-availability .field.custom input,.opt-5 .fieldset .field select,.opt-5 .delivery-block .field select,.opt-5 .shipping-availability .field select{border: none; border: 2px solid #7cbdbb4d;}.opt-5 .opt-logincontainer,.opt-initialemail{padding: 20px 20px 10px 20px; background: #f3f3f3; margin-bottom: 20px; border: 1px solid #d6d6d6; border-top: 3px solid #7cbdbb; padding-bottom: 20px;}.opt-initialemail{margin-bottom: 0;}.mobile .opt-5 .opt-step-container#opt-account{margin-bottom: 0;}.opt-initialemail span{background: white; width: 100%; display: block; padding: 15px; border: 2px solid #7cbdbb4d;}.opt-5 .opt-initial .opt-logincontainer{display: none;}.opt-5 .opc-wrapper .form-login,.opt-5 .opc-wrapper .form-login .field{margin-bottom: 10px;}.opt-5 .field._error .control input,.opt-5 .field._error .control select,.opt-5 .field._error .control textarea{border: none; border-bottom: 2px solid #E02E2B;}.opt-5 .fieldset .field.filled > .label,.opt-5 .delivery-block .field.filled > .label,.opt-5 .shipping-availability .field.filled > .label,.opt-5 .fieldset .field.filled > .label,.opt-5 .delivery-block .field.filled > .label,.opt-5 .shipping-availability .field.filled > .label{display: none;}/** Initial state Styling **//** General **/.opt-5 .opt-step-container:not(.opt-initial) ~ .opt-step-container{background: white; opacity: 0.3;}.opt-5 .opt-step-container.opt-initial form{display: none;}.opt-5 .opt-step-container.opt-initial .opt-form-title{margin: 0 !important;}.opt-5 .opt-step-container:not(.opt-initial) .opt-Edit-Button,.opt-5.opt-initial-for-edit .opt-step-container .opt-Edit-Button,.opt-5.opt-initial-for-deliverymethod .opt-step-container#opt-delivery.opt-initial .opt-Edit-Button{display: none;}.opt-5 .opt-step-container .opt-Edit-Button,.opt-5:not(.opt-loggedin) #opt-account.opt-step-container.opt-initial .opt-Edit-Button,.opt-5:not(.opt-initial-for-deliverymethod) .opt-step-container#opt-delivery.opt-initial .opt-Edit-Button,.opt-5.opt-initial-for-deliverymethod .opt-step-container#opt-delivery.opt-initial .opt-a-Edit{display: block;}/** Initial state Details **/.opt-5 .opt-step-container.opt-initial .newsletter-signup{display: none;}.opt-5 .opt-step-container.opt-initial .opt-button{display: none;}.opt-5 .opt-step-container:not(.opt-initial) .opt-initialemail{display: none;}.opt-5 .opt-step-container.opt-initial .opt-initialemail{display: block; margin-top: 20px; color: #434343;}/** Initial state Delivery **/.opt-5 .opt-step-container.opt-initial#opt-delivery{border-bottom: 0; padding-bottom: 1px;}.opt-5 .opt-step-container.opt-initial#opt-deliveryNote{border-top: 0; padding-top: 1px;}.opt-5 .opt-step-container:not(.opt-initial)#opt-deliveryNote #opc-delivery_notes{display: block !important;}.opt-5 .opt-step-container.opt-initial#opt-deliveryNote li,.opt-5 .opt-step-container.opt-initial#opt-deliveryNote .opt-button{display: none !important;}.opt-5 .opt-step-container:not(.opt-initial) .opt-initialdetails{display: none;}.opt-5 .opt-step-container .opt-initialdetails p{margin-bottom: 0;}.opt-5 .opt-step-container:not(.opt-initial) .opt-show{display: none;}.opt-5 .opt-step-container.opt-initial .opt-show{display: block;}.opt-5 .opt-step-container.opt-initial .opt-DeliveryMethod-Cont,.opt-5 .opt-step-container.opt-initial .opt-DeliveryNote-Cont{display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: start; -webkit-align-items: flex-start; -ms-flex-align: start; align-items: flex-start;}.opt-5 .opt-step-container.opt-initial .opt-DeliveryNote-Cont:not(.opt-showDeliveryNote){display: none;}.opt-5 .opt-step-container.opt-initial .opt-DeliveryNote-Cont{margin-top: 10px;}.opt-5 .opt-step-container.opt-initial .opt-DeliveryNote-Cont p{margin-bottom: 0;}.opt-5 .opt-step-container.opt-initial .opt-delivery-container,.opt-5 .opt-step-container.opt-initial .opt-deliverynote-container{margin-left: 5px; margin-top: 1px;}.opt-5 .opt-step-container.opt-initial .opt-initialdetails{display: block; margin-top: 20px; color: #434343;}.opt-5 .opt-step-container.opt-initial#opt-delivery .opt-DeliveryMethod-Cont #opt-DeliveryMethod-Edit,.opt-5 .opt-step-container.opt-initial#opt-delivery .opt-DeliveryMethod-Cont .opt-heading{display: none;}/** Account **/.opt-5 #opt-account .facebook{display: none;}.opt-5 .field-tooltip .field-tooltip-action{display: none;}.opt-5 .opt-signin-cont{font-size: 12px; line-height: 14px; color: #434343;}.opt-5 .opt-signin-cont a{font-size: 12px; text-decoration: underline; font-weight: bold;}.opt-5 #opt-account .opt-form-toggle p{font-size: 13px; margin-bottom: 10px; cursor: pointer;}.opt-5 #opt-account .opt-underline.opt-link-text{text-decoration: none; font-weight: normal; color: #59B7B3;}.opt-5.opt-showloginstate #opt-account .opt-note{display: none;}.opt-5.opt-showloginstate #opt-account #customer-email-fieldset{display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: justify; -webkit-justify-content: space-between; -ms-flex-pack: justify; justify-content: space-between;}.opt-5 .opt-initial .opt-form-toggle{display: none;}.opt-5.opt-showloginstate #opt-account #customer-email-fieldset.fieldset:after,.opt-5.opt-showloginstate #opt-account #customer-email-fieldset.fieldset:before{display: none;}.opt-5.opt-showloginstate #opt-account #customer-email-fieldset > .field{width: 47.5%;}.opt-5.opt-showloginstate #opt-account #customer-email-fieldset .fieldset{display: block !important; width: 47.5%;}.opt-5.opt-showloginstate #opt-account #customer-email-fieldset .field{margin-bottom: 0;}.opt-5.opt-showloginstate #opt-account #customer-email-fieldset .fieldset .actions-toolbar .primary{display: none;}.opt-5.opt-showloginstate #opt-account #customer-email-fieldset .fieldset .actions-toolbar .secondary{float: right; font-size: 12px; width: 150%; padding-right: 0; margin-bottom: 0;}.opt-5.opt-showloginstate #opt-account #customer-email-fieldset .fieldset .actions-toolbar .secondary .action{line-height: 40px;}.opt-5.opt-showloginstate #opt-account #customer-email-fieldset .note-logged{display: none;}.opt-5 .opc-wrapper #opt-account .field.newsletter-signup{margin-bottom: 0;}.opt-5 #customer-email-fieldset .fieldset{display: none !important;}.opt-5 #customer-email-fieldset > div.field.required.custom.ready > div > span.note{display: none !important;}.opt-5 .opt-note{color: #434343; font-size: 12px; line-height: 14px; margin-top: 10px;}.opt-5 .checkout-container .newsletter-signup .label{font-size: 11px; position: relative; padding-left: 35px;}.opt-5 .checkout-container .newsletter-signup input{border: 0; clip: rect(0, 0, 0, 0); height: 1px; margin: -1px; overflow: hidden; padding: 0; position: absolute; width: 1px;}.opt-5 .opc-wrapper #opt-account .field.newsletter-signup label:before{width: 25px; height: 25px; margin-top: -18px; top: 12.5px; position: absolute; left: 0; box-sizing: border-box; content: ""; background: white; border: 1px solid #59B7B3;}.opt-5 .opc-wrapper #opt-account .field.newsletter-signup input[type="checkbox"]:checked + label:after{width: 25px; height: 25px; margin-top: -12.5px; top: 7.5px; position: absolute; left: 0; box-sizing: border-box; content: ""; background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI3Ljg1IiBoZWlnaHQ9IjYuMzUiIHZpZXdCb3g9IjAgMCA3Ljg1IDYuMzUiPjxwYXRoIGZpbGw9IiM1OUI3QjMiIGQ9Ik02LjQxIDBMMi45NCAzLjUgMS40MSAyIDAgMy40MWwyLjkxIDIuOTQgNC45NC00Ljk0TDYuNDEgMCIvPjwvc3ZnPg==); -webkit-filter: brightness(0)invert(1); filter: brightness(0)invert(1); background-size: 21px; background-position: 50% 50%; background-repeat: no-repeat;}.opt-5 .field input[type="checkbox"]:checked + label:before{background: #59B7B3 !important; border-color: #59B7B3 !important;}.opt-5 #opt-account .button.action.action-show-popup.greyed.large{display: none;}.opt-5.opt-loggedin .opt-Edit-Button{display: none;}/** Delivery **/.field[name="shippingAddress.country_id"],.phone-region{display: none !important;}.opt-5.checkout-index-index .fieldset.address .field.phone-region + .field[name$="telephone"]{width: 100%; padding-left: 0;}.opt-5 .opt-step-container#opt-delivery{margin-bottom: 0; padding-bottom: 1px;}.opt-5 .opt-step-container#opt-deliveryNote{padding-top: 0;}.opt-5 .opc-wrapper .fieldset > .authoritytoleave-checkbox > .label{padding-left: 0; font-size: 14px; font-weight: bold; margin-bottom: 15px;}.mobile .opt-5 .opc-wrapper .fieldset > .authoritytoleave-checkbox > .label{position: static;}.opt-5 .opc-wrapper .fieldset > .authoritytoleave-checkbox > .label:after,.opt-5 .opc-wrapper .fieldset > .authoritytoleave-checkbox > .label:before{margin-top: initial !important; top: initial !important; bottom: -3px;}.opt-5 .opc-wrapper .fieldset > .authoritytoleave-checkbox > .label:after{-webkit-background-size: 15px; background-size: 15px;}.mobile .opt-5 .opc-wrapper .fieldset > .authoritytoleave-checkbox > .label:after,.mobile .opt-5 .opc-wrapper .fieldset > .authoritytoleave-checkbox > .label:before{bottom: 5px;}.opt-5 .opc-wrapper .fieldset > .authoritytoleave-checkbox > .label:before{background: white;}.opt-5 .field[name="shippingAddress.telephone"] ._with-tooltip > small:not(.opt-textright){display: none;}.opt-5 .field[name="shippingAddress.telephone"] ._with-tooltip > small.opt-textright{margin-top: 10px;}.opt-5 .field[name="shippingAddress.street.1"],.opt-5 .field[name="billingAddressafterpaypayovertime.street.0"],.opt-5 .field[name="billingAddressbraintree.street.1"]{display: none;}.opt-5 .field[name="shippingAddress.street.0"],.opt-5 .field[name="billingAddressbraintree.street.0"]{margin-bottom: 0 !important;}.opt-5 .opt-delivering{font-size: 12px; margin-bottom: 10px;}.opt-5 .opt-delivering a{font-weight: bold;}.opt-5 .opt-DeliveryMethod-Cont{margin-top: 20px;}.opt-5 .opt-DeliveryMethod-Cont .opt-heading{display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; -webkit-box-pack: justify; -webkit-justify-content: space-between; -ms-flex-pack: justify; justify-content: space-between; font-size: 14px; font-weight: bold;}.opt-5 .opt-DeliveryMethod-Cont #opt-DeliveryMethod-Edit{text-decoration: underline; font-size: 12px; cursor: pointer;}.opt-5 .opt-DeliveryMethod-Cont p{margin: 0; font-size: 14px; margin-top: 5px; color: #434343;}.opt-5 .opt-DeliveryMethod-Cont p:first-child{margin-top: 0;}.opt-5 .opt-DeliveryMethod-Cont p:last-child{margin-bottom: 20px;}.opt-5 .opt-initial .opt-DeliveryMethod-Cont p:last-child{margin-bottom: 0;}/** Logged In Delivery **/.opt-5.opt-loggedin-delivery .opc-wrapper .shipping-address-item{width: 100%; background: white; border: 1px solid #CFCFCF; min-height: initial; font-size: 12px; position: relative; padding: 14px; padding-left: 65px; cursor: pointer;}.opt-5.opt-loggedin-delivery .opc-wrapper .shipping-address-item:nth-of-type(1){margin-top: 20px;}.opt-5.opt-loggedin-delivery .opc-wrapper .shipping-address-items label{display: none;}.opt-5.opt-loggedin-delivery .opc-wrapper .shipping-address-item .action-select-shipping-item{display: none;}.opt-5.opt-loggedin-delivery .opc-wrapper .shipping-address-item br:not(:nth-last-child(3)){display: none;}.opt-5.opt-loggedin-delivery .opc-wrapper .shipping-address-item br:nth-of-type(1){display: block;}.opt-5.opt-loggedin-delivery .opc-wrapper .shipping-address-item.selected-item:after,.opt-5.opt-loggedin-delivery .opc-wrapper .shipping-address-item:before{-webkit-transform: translateY(-50%); -ms-transform: translateY(-50%); transform: translateY(-50%); position: absolute; top: 50%; content: ""; display: block;}.opt-5.opt-loggedin-delivery .opc-wrapper .shipping-address-item:before{width: 25px; height: 25px; left: 20px; top: 50%; border-radius: 50%; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; content: ""; background: white; border: 1px solid #c7c7c7 !important; width: 20px !important; height: 20px !important;}.opt-5.opt-loggedin-delivery .opc-wrapper .shipping-address-item.selected-item:before{border-color: #0063ba !important;}.opt-5.opt-loggedin-delivery .opc-wrapper .shipping-address-item.selected-item:after{margin-top: 0px; position: absolute; border-radius: 50%; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; content: ""; left: 25px; width: 10px !important; height: 10px !important; background: none; background-color: #0063ba;}.opt-5.opt-loggedin-delivery .opt-deliver-to-text{font-size: 14px;}.opt-5.opt-loggedin-delivery .opt-add-new-address{font-size: 12px; font-weight: bold; text-align: right; text-decoration: underline; cursor: pointer;}.opt-5.opt-loggedin-delivery .edit-address-link{top: 50%; -webkit-transform: translateY(-50%); -ms-transform: translateY(-50%); transform: translateY(-50%);}.opt-5.opt-loggedin-delivery #opt-delivery.opt-initial .opt-deliver-to-text,.opt-5.opt-loggedin-delivery #opt-delivery.opt-initial .opt-add-new-address,.opt-5.opt-loggedin-delivery #opt-delivery.opt-initial .edit-address-link,.opt-5.opt-loggedin-delivery .opc-wrapper #opt-delivery.opt-initial .shipping-address-item.selected-item:after,.opt-5.opt-loggedin-delivery .opc-wrapper #opt-delivery.opt-initial .shipping-address-item:before{display: none;}.opt-5.opt-loggedin-delivery #opt-delivery.opt-initial .shipping-address-item:not(.selected-item){display: none;}.opt-5.opt-loggedin-delivery .opc-wrapper #opt-delivery.opt-initial .shipping-address-item{padding-left: 0; font-size: 14px; border: none; margin-top: 0; padding-bottom: 0;}.opt-5.opt-loggedin-delivery .opc-wrapper #opt-delivery .shipping-address-item br:nth-of-type(4){display: block !important;}.opt-5.opt-loggedin-delivery .opc-wrapper #opt-delivery.opt-initial .field.addresses{margin-bottom: 0;}.opt-5 .postcode-change-container .message-container.warning{background: none; color: #E02E2B; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif; text-transform: initial; font-size: 14px; font-weight: bold; line-height: 18px; padding: 0; padding-left: 10px; padding-right: 10px; margin: 0;}.opt-5 .postcode-change-container{display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; padding-bottom: 10px;}.opt-5 .postcode-change-container .actions{height: 50px !important; margin: 0 !important;}.opt-5 .postcode-change-container button{width: 110px; border: 1px solid #E02E2B; color: #E02E2B; height: 50px !important; padding: 0 10px;}.opt-5 .postcode-change-container button:hover,.opt-5 .postcode-change-container button:active{background: #E02E2B; color: white;}.opt-5 .opc-wrapper .edit-address-link > .svg-icon-edit{width: 15px;}/** Logged in Delivery Modal **/.opt-5.opt-modal-open._has-modal .opt-add-new-address{display: none;}.opt-5.opt-modal-open._has-modal{overflow: initial; position: relative;}.opt-5.opt-modal-open.checkout-index-index #opt-delivery .modal-slide,.opt-5.opt-modal-open.checkout-index-index #opt-delivery .modal-popup{position: static; padding-top: 20px;}.opt-5.checkout-index-index .opt-initial #opt-delivery .modal-popup,.opt-5.checkout-index-index:not(.opt-modal-open) #opt-delivery .modal-popup,.opt-5.checkout-index-index:not(._has-modal) #opt-delivery .modal-popup{display: none;}.opt-5.opt-modal-open.checkout-index-index #opt-delivery .modal-popup .modal-inner-wrap{width: initial; margin: 0; left: 0; position: static;}.opt-5.opt-modal-open.checkout-index-index .modals-overlay{display: none;}.opt-5.opt-modal-open.checkout-index-index #opt-delivery .modal-slide .modal-inner-wrap,.opt-5.opt-modal-open.checkout-index-index #opt-delivery .modal-popup .modal-inner-wrap{-webkit-box-shadow: none; box-shadow: none; background: #E5EFF8; -webkit-transition: none; transition: none;}.opt-5.opt-modal-open.checkout-index-index #opt-delivery .modal-popup .modal-header{padding-left: 0; padding-right: 0; padding-top: 0; position: relative;}.opt-5.opt-modal-open.checkout-index-index #opt-delivery .modal-popup .modal-header .action-close{background: none; width: initial; top: 0px; right: 0px;}.opt-5.opt-modal-open.checkout-index-index #opt-delivery .modal-popup .modal-header .action-close:hover{color: #0063ba;}.opt-5.opt-modal-open.checkout-index-index #opt-delivery .modal-popup .modal-header .action-close:after{content: "Close"; text-transform: initial; font-weight: bold; font-size: 12px; text-decoration: underline;}.opt-5.opt-modal-open.checkout-index-index #opt-delivery .modal-popup .modal-title{font-size: 18px; font-weight: bold; text-align: left; padding-bottom: 0; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif;}.opt-5.opt-modal-open.checkout-index-index #opt-delivery .modal-popup .modal-content{padding: 0;}.opt-5.opt-modal-open.checkout-index-index #opt-delivery .modal-popup .modal-footer{padding: 0; text-align: left;}.opt-5.opt-modal-open.checkout-index-index #opt-delivery .modal-popup .modal-footer button{float: none; margin-left: 0;}.opt-5.opt-modal-open.checkout-index-index #opt-delivery .modal-popup .modal-footer button:nth-of-type(2){font-size: 14px; margin-left: 10px; text-transform: initial;}.opt-5.opt-modal-open.checkout-index-index #opt-delivery .modal-popup .form-shipping-address .label[for="shipping-save-in-address-book"]{font-size: 14px; font-weight: initial; padding-bottom: 0; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif;}/** PAYMENT SECTION **/.opt-5 .checkout-payment-method .discount-code._collapsible .payment-option-title,.opt-5 .checkout-payment-method .giftcardaccount._collapsible .payment-option-title,.opt-5 .paypal-review.view .discount-code._collapsible .payment-option-title,.opt-5 .paypal-review.view .giftcardaccount._collapsible .payment-option-title{display: none;}.opt-5 .opt-open.opt-giftCardSection{padding: 17px 22px; background: white;}.opt-5 .opt-giftCardSection{padding: 17px 22px; border: 1px solid white; background: none; margin-bottom: 20px;}.opt-5 .opc-wrapper .fieldset .opt-billing-toggle-cont .field:not(.choice) > .label{line-height: 25px;}.opt-5 .opc-wrapper .fieldset .opt-billingaddresstoggle .opt-billing-header .opt-title{font-size: 14px; font-weight: bold;}.opt-5 .opc-wrapper .fieldset .opt-billing-toggle-cont .field{margin-bottom: 0px;}.opt-5 .opt-giftCardSection .opt-giftTitle{margin-bottom: 0; cursor: pointer; position: relative;}.opt-5 .opt-giftCardSection .opt-giftTitle:after{background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI5LjYwMyIgaGVpZ2h0PSI1LjkyMiIgdmlld0JveD0iMCAwIDkuNjAzIDUuOTIyIj48cGF0aCBmaWxsPSIjMDA2M0JBIiBkPSJNOS42MDMgMS4xMjFMOC40ODEgMGwtMy42OCAzLjY4TDEuMTIyIDAgMCAxLjEyMWw0LjgwMSA0LjgwMXoiLz48L3N2Zz4=); -webkit-background-size: 15px 15px; background-size: 15px 15px; background-position: center center; content: ""; width: 30px; height: 25px; position: absolute; right: -15px; top: 0; background-repeat: no-repeat;}.opt-5 .opt-open.opt-giftCardSection .opt-giftTitle:after{-webkit-transform: rotate(180deg); -ms-transform: rotate(180deg); transform: rotate(180deg);}.opt-5 .opt-open .opt-giftTitle{font-weight: bold; margin-bottom: 5px;}.opt-5 .checkout-payment-method .giftcardaccount,.opt-5 .checkout-payment-method .discount-code{margin-bottom: 0; padding: 0; background: white;}.opt-5 .checkout-payment-method .discount-code .gift-hint,.opt-5 .checkout-payment-method .giftcardaccount .gift-hint,.opt-5 .paypal-review.view .discount-code .gift-hint,.opt-5 .paypal-review.view .giftcardaccount .gift-hint{margin-bottom: 0; font-size: 14px; border-bottom: 0; padding-bottom: 0;}.opt-5 .checkout-payment-method .discount-code .gift-hint{margin-top: 20px; margin-bottom: 10px;}.opt-5 .checkout-payment-method .giftcardaccount .gift-hint{margin-top: 10px;}.opt-5 .checkout-payment-method .discount-code > .payment-option-content > .form .input-text,.opt-5 .checkout-payment-method .giftcardaccount > .payment-option-content > .form .input-text,.opt-5 .paypal-review.view .discount-code > .payment-option-content > .form .input-text,.opt-5 .paypal-review.view .giftcardaccount > .payment-option-content > .form .input-text{border: 1px solid #CFCFCF;}.opt-5 .checkout-payment-method .discount-code .detail-label:first-child,.opt-5 .checkout-payment-method .giftcardaccount .detail-label:first-child,.opt-5 .paypal-review.view .discount-code .detail-label:first-child,.opt-5 .paypal-review.view .giftcardaccount .detail-label:first-child{display: none;}.opt-5 #opt-payment .tooltip.wrapper{display: none;}.opt-5 #opt-payment .opt-giftCardSection ._left,.opt-5 #opt-payment .opt-giftCardSection ._right{float: none !important; margin: 0; padding: 0; min-width: initial; width: initial; display: block; margin-top: 5px;}.opt-5 #opt-payment .opt-giftCardSection ._left{display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: justify; -webkit-justify-content: space-between; -ms-flex-pack: justify; justify-content: space-between;}.opt-5 #opt-payment .opt-giftCardSection .giftcardaccount ._right{display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: horizontal; -webkit-box-direction: reverse; -webkit-flex-direction: row-reverse; -ms-flex-direction: row-reverse; flex-direction: row-reverse; -webkit-box-pack: justify; -webkit-justify-content: space-between; -ms-flex-pack: justify; justify-content: space-between;}.opt-5 #opt-payment .opt-giftCardSection .giftcardaccount ._right > ._right.primary{width: 266px;}.opt-5 #giftcard-code::-webkit-input-placeholder,.opt-5 #giftcard-pin::-webkit-input-placeholder,.opt-5 #discount-code::-webkit-input-placeholder{color: #434343 !important;}.opt-5 #giftcard-code::-moz-placeholder,.opt-5 #giftcard-pin::-moz-placeholder,.opt-5 #discount-code::-moz-placeholder{color: #434343 !important;}.opt-5 #giftcard-code:-ms-input-placeholder,.opt-5 #giftcard-pin:-ms-input-placeholder,.opt-5 #discount-code:-ms-input-placeholder{color: #434343 !important;}.opt-5 #giftcard-code::placeholder,.opt-5 #giftcard-pin::placeholder,.opt-5 #discount-code::placeholder{color: #434343 !important;}.opt-5 #giftcard-code{width: 266px;}.opt-5 #giftcard-pin{max-width: 180px;}.opt-5 #discount-code{width: 100%;}.opt-5 .checkout-payment-method .discount-code .actions-toolbar .action,.opt-5 .checkout-payment-method .giftcardaccount .actions-toolbar .action{width: 100%; margin: 0;}.opt-5 #opt-payment .opt-giftCardSection .discount-code ._left .field.custom.ready{width: 100%;}/*.opt-5.opt-loggedin .checkout-payment-method .payment-method-billing-address .billing-address-details{padding-left: 0; display: block;}*/.opt-5 .actions-toolbar:before,.opt-5 .actions-toolbar:after{display: none;}.opt-5 .checkout-payment-method .actions-toolbar button.action.primary{color: #fff; background-color: #0063ba;}.opt-5 .checkout-payment-method .actions-toolbar button.action.primary:hover{color: #0063ba; border-color: #0063ba; background-color: white;}.opt-5 .checkout-payment-method .discount-code #discount-form .actions-toolbar > .primary{width: 266px; margin-top: 5px;}.opt-5 .opt-open .giftcardaccount .payment-option-content,.opt-5 .opt-open .discount-code .payment-option-content{display: block !important;}.opt-5 .checkout-payment-method .payment-method-billing-address{margin-bottom: 0;}.opt-5 .payment-method-billing-address .billing-address-details{display: none;}.opt-5 .opt-open .opt-bluesep{display: block;}/** Payment Method **/.opt-5 .payment-method{background: #e3eff9; border-bottom: 1px solid white; padding: 0 22px;}.opt-5 .payment-method._active{background: white; padding: 17px 22px;}.opt-5 .opc-wrapper .payment-method-content .form .fieldset .field:not(.choice) > .label{display: none;}.opt-5 #credit-card-number{display: none;}.opt-5 .payment-method > .payment-method-title > label:before,.opt-5 .opc-wrapper .fieldset .opt-billing-toggle-cont .field:not(.choice) > .label:before,.mobile .opt-5 .payment-method > .payment-method-title > .title label:before{background: white; border-color: #0263BA !important; width: 20px !important; height: 20px !important; margin-top: -12.5px !important;}.opt-5 .payment-method > .payment-method-title > label:after,.opt-5 .opc-wrapper .fieldset .opt-billing-toggle-cont .field:not(.choice) > .label:after,.mobile .opt-5 .payment-method > .payment-method-title > .title label:after{width: 10px !important; height: 10px !important; top: 17.5px !important; left: 5px !important;}.opt-5 .checkout-payment-method .payment-method-content .fieldset > .field{margin-bottom: 5px;}.opt-5 .checkout-payment-method .payment-method-content #billing-new-address-form.fieldset > .field{margin-bottom: 20px;}.opt-5 #payment_form_braintree{width: 100%; position: relative;}.opt-5 .payment-method.payment-method-braintree .payment-method-title > .label strong{font-weight: normal; font-size: 14px; margin-left: 10px;}.opt-5 .form-braintree #payment_form_braintree .hosted-control{border-color: #CFCFCF;}.opt-5 #payment_form_braintree > div.field.number:nth-of-type(3),.opt-5 #payment_form_braintree > .field#braintree_cc_type_cvv_div{position: absolute;}.opt-5 #payment_form_braintree > div.field.number:nth-of-type(3){left: 0; width: 266px;}.opt-5 #payment_form_braintree > .field#braintree_cc_type_cvv_div{width: 180px; right: 0;}.opt-ccvtooltip{display: inline-block; text-align: right; float: right; font-size: 12px; color: #434343; cursor: pointer; z-index: 99; position: relative;}.opt-5 #braintree_cc_type_cvv_div .hosted-error{margin-top: 25px; z-index: -1;}/** Afterpay **/.opt-5 label[for="afterpaypayovertime"] > .text{font-weight: 400; font-size: 14px;}.opt-5 .payment-method.afterpay-payment-method .payment-method-title > .payment-method-note{display: none;}.opt-5 .afterpay-payment-method .afterpay-checkout-note ul{padding: 0;}.afterpay-payment-method .afterpay-checkout-note{background: white;}.opt-5 .afterpay-payment-method .afterpay-note-tilte{text-align: left; font-size: 13px; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif; text-transform: capitalize; margin: 0; margin-top: 10px;}.opt-5 .checkout-payment-method .payment-method._active .payment-method-content{margin: initial;}.opt-5 .afterpay-payment-method .afterpay-checkout-redirect{max-width: initial; text-align: left; font-size: 13px; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif;}.opt-5 .afterpay-payment-method .afterpay-checkout-redirect br{display: none;}.afterpay-payment-method .afterpay-checkout-note .cost li{margin-bottom: 2px;}.afterpay-payment-method .afterpay-checkout-note ul.instalment li{font-size: 12px; margin-bottom: 0;}/** Paypal **/.opt-5 .payment-method.item > small{display: none !important;}.opt-5 .payment-method.item > .payment-method-title .label._left strong{font-weight: normal; font-size: 14px; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif; text-transform: initial; margin-left: 10px; line-height: 25px;}.opt-5 .payment-method.item .types-list > .item{max-width: initial; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; margin-top: -5px; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center;}.opt-5 .payment-method.item .types-list > .item img{max-width: 100px; margin-top: 3px;}.opt-5 ._active.payment-method.item .opt-Paypal-message{display: block;}.opt-5 .payment-method.item .opt-Paypal-message{font-size: 14px; display: none;}/** Shipping Check **/.opt-5 .billing-address-same-as-shipping-block span{font-weight: normal; font-size: 14px; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif;}.opt-5.opt-loggedin-delivery .checkout-payment-method .payment-method-content .payment-method-billing-address .fieldset > .field{margin-bottom: 20px;}/**BUTTONS FOR PAYMENT **/.opt-5 .opt-button .opt-blue-Button{margin-top: 20px;}.opt-5 .payment-method:not(._active) .opt-button{display: none;}@media (max-width: 1125px){.opt-5 .opt-back-link{right: 0; margin-right: 30px;}.opt-5 .header > .logo{padding-right: 100px;}}@media (max-width: 1025px){.opt-5 .main-wrapper{max-width: 500px; padding-left: 10px;}.opt-5 .sidebar{padding-right: 10px;}}@media (max-width: 1025px) and (min-width: 760px){.opt-5 .postcode-change-container{-webkit-box-orient: vertical; -webkit-box-direction: normal; -webkit-flex-direction: column; -ms-flex-direction: column; flex-direction: column; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center;}.opt-5 .postcode-change-container .actions{margin-top: 10px !important; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: vertical; -webkit-box-direction: normal; -webkit-flex-direction: column; -ms-flex-direction: column; flex-direction: column; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; height: initial !important;}.opt-5 .postcode-change-container .actions button{display: block; margin: 10px 0 0 !important; -webkit-box-flex: 1; -webkit-flex-grow: 1; -ms-flex-positive: 1; flex-grow: 1; width: 160px;}}@media (max-width: 968px){.opt-5 .main-wrapper{max-width: 450px; padding-left: 10px;}.opt-5 .sidebar{padding-right: 10px;}}@media (max-width: 905px){.opt-5 .checkout-container .opc-sidebar{padding: 40px 20px 40px 20px;}.opt-5 .opt-step-container{padding: 40px 20px 40px 20px;}.opt-5 .main-wrapper{max-width: 480px; padding-left: 10px;}.opt-5 .sidebar{width: 350px;}}@media (max-width: 845px){.opt-5 .main-wrapper{max-width: 400px; padding-left: 10px;}.opt-5 .sidebar{padding-right: 10px;}}@media only screen and (max-width: 768px){.opt-5 .custom-slide *{-webkit-box-shadow: none; box-shadow: none;}}@media (max-width: 760px){.opt-modal .opt-modal-buttons button{margin: 0 5px;}html{min-width: initial;}.opt-5 .checkout-container{margin-bottom: 0; max-width: initial;}.opt-5 .main-wrapper{max-width: initial; width: 100%; padding-right: 0; padding-left: 0; padding-bottom: 0; border-bottom: 0;}.opt-5 .sidebar{width: initial; padding-left: 0px; padding-right: 0px;}.opt-5 .sidebar .checkout-container{border: 1px solid rgba(151, 151, 151, 0.11); margin-top: 20px; border-left: 0; border-right: 0; margin-bottom: 20px;}.opt-5 .opt-step-container.opt-initial#opt-payment{margin-bottom: 0;}/* .opt-5 .payment-method._active.payment-method-braintree{padding-bottom: 44px;}.opt-5 .opt-step-container#opt-payment .opt-button{display: none;}*/ .opt-5 .checkout-sidebar .place-order-container{display: block; max-width: 290px; margin: auto; margin-top: 20px;}}@media (max-width: 640px){.opt-5.opt-loggedin-delivery .opc-wrapper .shipping-address-item{margin: 0; margin-bottom: -1px;}}@media (max-width: 540px){.opt-5 #payment_form_braintree > div.field.number:nth-of-type(3){width: 60%;}.opt-5 #payment_form_braintree > .field#braintree_cc_type_cvv_div{width: 35%;}.opt-5 #giftcard-pin{margin-left: 5px;}}@media (max-width: 470px){.opt-modal{padding: 20px; width: 90%;}.opt-modal .opt-modal-buttons{-webkit-box-orient: vertical; -webkit-box-direction: normal; -webkit-flex-direction: column; -ms-flex-direction: column; flex-direction: column; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center;}.opt-modal .opt-modal-buttons button:nth-of-type(1){margin-bottom: 10px;}}@media (max-width: 450px){.opt-5 #giftcard-code{width: 100%; max-width: initial;}.opt-5 #giftcard-pin{width: 100%; max-width: initial; margin-left: 5px;}.opt-5 #opt-payment .opt-giftCardSection .giftcardaccount ._right > ._right.primary, .opt-5 .checkout-payment-method .discount-code #discount-form .actions-toolbar > .primary{width: 50%; padding-right: 5px;}.opt-5 .checkout-payment-method .giftcardaccount .actions-toolbar .action{min-width: initial;}.opt-5 .postcode-change-container{-webkit-box-orient: vertical; -webkit-box-direction: normal; -webkit-flex-direction: column; -ms-flex-direction: column; flex-direction: column;}.opt-5 .postcode-change-container .button{margin-top: 5px; width: 180px;}.opt-5 .postcode-change-container .actions{margin-top: 10px !important; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: vertical; -webkit-box-direction: normal; -webkit-flex-direction: column; -ms-flex-direction: column; flex-direction: column; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; height: initial !important;}.opt-5 .postcode-change-container .actions button{display: block; margin: 10px 0 0 !important; -webkit-box-flex: 1; -webkit-flex-grow: 1; -ms-flex-positive: 1; flex-grow: 1; width: 160px;}}/** MOBILE STYLING **/.mobile .opt-5 .header.trust{display: none;}.mobile .opt-5 .page-wrapper{padding-top: 80px !important;}.mobile .opt-5 .page-main{margin: 0;}.mobile .opt-5 .page-title-wrapper{border-bottom: 0;}.mobile .opt-5.checkout-index-index .opc-wrapper{padding: 0;}.mobile .opt-5 .opt-step-container{padding: 15px 0; margin: 0 15px 10px; margin-bottom: 10px;}.mobile .opt-5 .opt-step-container:not(.opt-initial):not(#opt-delivery){border-bottom: 2px solid black;}/** Mobile Header **/.mobile .opt-5 .header-item.minicart-wrapper,.mobile .opt-5 .header-item.menu-toggle{display: none;}.mobile .opt-5 .header > .logo{padding: 0;}.mobile .opt-5 .opt-back-link{left: 0;}.mobile .opt-5 .opt-back-link a{display: block; color: white;}.mobile .opt-5 .opt-back-link a:after{top: 50%; -webkit-transform: translatey(-50%)rotate(90deg); -ms-transform: translatey(-50%)rotate(90deg); transform: translatey(-50%)rotate(90deg); margin-top: 0;}.mobile .opt-5 .header > .logo img{width: initial;}.mobile .opt-5 .opt-benefit{font-size: 14px;}.mobile .opt-5 .page-main > .columns{padding-top: 25px;}.mobile .opt-5 .opt-step-container:not(.opt-initial) .opt-deliverynote-container,.opt-5 .opt-step-container:not(.opt-initial) .opt-deliverynote-container{display: none;}.mobile .opt-5 .opt-step-container.opt-initial .opt-initialemail,.mobile .opt-5 .opt-step-container.opt-initial .opt-initialdetails,.mobile .opt-5 .opt-step-container.opt-initial .opt-delivery-container,.mobile .opt-5 .opt-step-container.opt-initial .opt-deliverynote-container,.mobile .opt-5 .opt-step-container.opt-initial .opt-show,.mobile .opt-5 .opt-DeliveryMethod-Cont p{font-size: 12px;}.mobile .opt-5 .opt-step-container .opt-form-title .opt-title,.opt-5 .checkout-container .checkout-summary > .items .opt-title{font-size: 18px; font-weight: bold; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif;}/** Mobile Account **/.mobile .opt-5 .opc-wrapper #opt-account .field.newsletter-signup{display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex;}.mobile .opt-5 .opc-wrapper #opt-account.opt-initial .field.newsletter-signup{display: none;}/** Mobile Summary **/.mobile .opt-5 #opt-mobile-summary{border-left: none; border-right: none; margin-top: 10px; padding: 20px 15px;}.mobile .opt-5 #opt-mobile-summary .place-order-container button{margin-top: 10px;}.mobile .opt-5 .checkout-container .checkout-summary > .items{margin-bottom: 10px;}/** Mobile Payment **/.mobile .opt-5.checkout-index-index .promo-gift,.mobile .opt-5 .paypal-review .promo-gift{border: 1px solid white; padding: 17px 15px; margin-top: 20px;}.mobile .opt-5.checkout-index-index .promo-gift.active{background: white;}.mobile .opt-5.checkout-index-index .promo-gift > .content > .block.giftcard .field > .control:first-child{margin-right: 0;}.mobile .opt-5.checkout-index-index .promo-gift > .title{font-weight: bold; margin: 0; margin-bottom: 10px;}.mobile .opt-5.checkout-cart-index .promo-gift > .title,.mobile .opt-5 .paypal-review .promo-gift > .title{margin: 0; font-weight: normal; text-transform: initial; font-size: 14px;}.mobile .opt-5.checkout-index-index .promo-gift > .content > .text{font-size: 12px;}.mobile .opt-5.checkout-index-index .promo-gift > .content > .block.discount .label,.mobile .opt-5 .checkout-cart-index .promo-gift > .content > .block.discount .label,.mobile .opt-5 .paypal-review .promo-gift > .content > .block.discount .label,.mobile .opt-5.checkout-index-index .promo-gift > .content > .block.giftcard .label,.mobile .opt-5 .checkout-cart-index .promo-gift > .content > .block.giftcard .label,.mobile .opt-5 .paypal-review .promo-gift > .content > .block.giftcard .label{display: none;}.mobile .opt-5 .helper_clearfix:after,.mobile .opt-5 ._clearfix:after{display: none;}.mobile .opt-5.checkout-index-index .promo-gift > .content{display: -webkit-box !important; display: -webkit-flex !important; display: -ms-flexbox !important; display: flex !important; -webkit-flex-wrap: wrap; -ms-flex-wrap: wrap; flex-wrap: wrap;}.mobile .opt-5.checkout-index-index .promo-gift > .title{text-transform: initial; font-weight: normal;}.mobile .opt-5.checkout-index-index .promo-gift.active > .title{font-weight: bold;}.mobile .opt-5.checkout-index-index .promo-gift:not(.active) > .content{display: none !important;}.mobile .opt-5.checkout-index-index .promo-gift:not(.active) > .title{margin: 0; text-transform: initial; font-weight: normal;}.mobile .opt-5.checkout-index-index .promo-gift > .content > .block.discount .field{display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-flex-wrap: wrap; -ms-flex-wrap: wrap; flex-wrap: wrap; width: 100%; -webkit-box-orient: vertical; -webkit-box-direction: normal; -webkit-flex-direction: column; -ms-flex-direction: column; flex-direction: column;}.mobile .opt-5 .fieldset .promo-gift .field.custom input{border: 1px solid #CFCFCF;}.mobile .opt-5 .fieldset .promo-gift .field.custom input::-webkit-input-placeholder{color: #CFCFCF !important;}.mobile .opt-5 .fieldset .promo-gift .field.custom input::-moz-placeholder{color: #CFCFCF !important;}.mobile .opt-5 .fieldset .promo-gift .field.custom input:-ms-input-placeholder{color: #CFCFCF !important;}.mobile .opt-5 .fieldset .promo-gift .field.custom input::placeholder{color: #CFCFCF !important;}.mobile .opt-5.checkout-index-index .promo-gift > .content > .block.discount .field .control{-webkit-box-flex: 1 !important; -webkit-flex: 1 !important; -ms-flex: 1 !important; flex: 1 !important; width: 100%;}.mobile .opt-5 .promo-gift .content > .block{width: 100%;}.mobile .opt-5 .payment-method{padding: 0 10px;}.mobile .opt-5 #giftcard-code{width: initial;}.mobile .opt-5 .form.giftcard .field:not(.custom){display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: vertical; -webkit-box-direction: normal; -webkit-flex-direction: column; -ms-flex-direction: column; flex-direction: column;}.mobile .opt-5 .form.giftcard .field:not(.custom) .control{text-align: right; margin: 0 !important; margin-bottom: 10px !important;}.mobile .opt-5 .form.giftcard .field:not(.custom) .control span{font-size: 12px; font-weight: bold; text-transform: initial;}.mobile .opt-5.checkout-index-index .promo-gift > .content > .block.giftcard .action.as-link,.mobile .opt-5.checkout-index-index .promo-gift > .content > .block.discount .action.as-link{width: 100%; color: #fff; border-color: #0063ba; background-color: #0063ba; line-height: 2.3rem; padding: 14px 17px; font-size: 1.7rem; text-decoration: none; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif;}.mobile .opt-5 .promo-gift .content > .block{margin-bottom: 0 !important;}.mobile .opt-5.checkout-index-index .promo-gift > .content > .block.discount .action.as-link{margin-top: 10px;}.mobile .opt-5 .opt-bluesep{display: block; border-width: 1px; margin: 15px 0;}/** Payment Methods **/.mobile .opt-5 .checkout-payment-method .credit-card-types .item{height: 20px; margin: 0;}.mobile .opt-5 .checkout-payment-method .credit-card-types .item img{max-width: 35px; width: 100%}.mobile .opt-5 .opc-wrapper .fieldset .opt-billing-toggle-cont .field:not(.choice) > .label{position: relative;}.mobile .opt-5 .checkout-payment-method .payment-method-content #billing-new-address-form.fieldset > .field{margin-bottom: 10px;}.mobile .opt-5 .afterpay-payment-method .afterpay-note-tilte{margin-top: 25px;}.mobile .opt-5 .paypal-payment-method{display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: vertical; -webkit-box-direction: normal; -webkit-flex-direction: column; -ms-flex-direction: column; flex-direction: column;}.mobile .opt-5 .payment-method.paypal-payment-method.item .types-list > .item img{max-width: 50px;}.mobile .opt-5 .payment-method.paypal-payment-method.item .types-list > .item{margin-top: -5px;}.mobile .opt-5 .payment-method{padding-bottom: 10px;}.mobile .secure-pay-wrapper:not(.opt-securePay){display: none;}.mobile .secure-pay-wrapper.opt-securePay{background: white;}.mobile .opt-5.checkout-index-index .checkout-container{padding-bottom: 0;}.mobile .opt-5 .secure-pay-wrapper .trust-block p{display: block; font-size: 12px; padding: 0 20px;}.mobile .opt-5.checkout-index-index .secure-pay-wrapper .trust-block > .trust-label{margin-top: 20px; font-family: "SuperGroteskA", "Helvetica Neue", Helvetica, Arial, sans-serif; font-size: 14px;}.mobile .opt-5.checkout-index-index .secure-pay-wrapper .info-block{margin-bottom: 20px;}.mobile .opt-5.ze-fixed .page-footer{padding: 0; padding-bottom: 20px;}.mobile .opt-5 .footer-block .footer-newsletter,.mobile .opt-5 .footer-block .footer-navigation,.mobile .opt-5 .footer-block .paysecure{display: none;}.mobile .opt-5.ze-fixed .footer.content{border-top: 0;}.mobile .opt-5.checkout-index-index .checkout-summary .product-item{border-bottom: 0;}.mobile .opt-5 .opc-wrapper #opt-delivery > .field.addresses{margin-bottom: 10px;}.mobile .opt-5 .opt-DeliveryMethod-Cont{margin-top: 10px;}</style>');
}

defermob(function () {
    desktopcss();
    desktopjs();
}, '.grand', function () {
    mobilejs();
    mobilecss();
}, 'html.mobile');