/** HELPERS */

function defer(method, selector) {
    if (window.jQuery && window.checkoutConfig) {
        if (jQuery(selector).length > 0) {
            method();
        } else {
            setTimeout(function () {
                defer(method, selector);
            }, 50);
        }
    } else {
        setTimeout(function () {
            defer(method, selector);
        }, 50);
    }
}

function elementDefer(method, arrayOfSelectors) {
    var valid = 0;
    for (var i = 0; i < arrayOfSelectors.length; i++) {
        if (jQuery(arrayOfSelectors[i]).length > 0) {
            valid++;
        }
    }
    if (valid == arrayOfSelectors.length) {
        method();
    } else {
        setTimeout(function () {
            elementDefer(method, arrayOfSelectors);
        }, 50);
    }
}

function isCustomerLoggedIn() { // checks if customer is logged in
    return checkoutConfig.isCustomerLoggedIn;
}

function addButton(selector, name, id) { // Adds a checkout button
    var html = '<div class="opt-button"><button class="opt-blue-Button button large primary inverted expanded" id="' + id + '">' + name + '</button></div>';
    jQuery(selector).after(html);
    jQuery('#' + id).click(function (e) {
        e.preventDefault();
        jQuery('.place-order-container button').click();
    });
}

function addsubTitle (selector, title) {
    jQuery(selector).prepend('<div class="opt-deliverto"><p>'+title+'</p></div>');
}

function updateonchange(target, dest, opt) { // Takes the value of the input and applies it to a target
    var opt = opt || '';
    jQuery(target).change(function () {
        jQuery(dest).text(jQuery(this).val() + opt);
        jQuery('.opt-nodata-hidden').removeClass('opt-nodata-hidden');
    });
}

function getDataFromLocal (storage) {
    return JSON.parse(localStorage.getItem(storage));
}

function updateonsubmit(data, dest, opt) { // Takes the value of the input and applies it to a target
    var opt = opt || '';
        jQuery(dest).text(data + opt);
        jQuery('.opt-nodata-hidden').removeClass('opt-nodata-hidden');
}

function updateInitialDetails() {
    var data = getDataFromLocal('mage-cache-storage'),
        custData = data["checkout-data"]["shippingAddressFromData"];
    
        updateonsubmit(custData.firstname, '#opt-detailsfirstname');
        updateonsubmit(custData.lastname, '#opt-detailslastname');
        updateonsubmit(custData.company, '#opt-detailscomapny');
        updateonsubmit(custData.street[0], '#opt-detailsstreet', ',');
        updateonsubmit(custData.street[1], '#opt-detailsstreet1');
        updateonsubmit(custData.telephone, '#opt-detailsphone');
        updateonsubmit(custData.postcode, '#opt-detailspost');
        updateonsubmit(custData.city, '#opt-detailssuburb', ',');
        updateonsubmit(data["directory-data"]["AU"]["regions"][custData.region_id].code, '#opt-detailsstate', ',');
}

function updateonchangestate(target, dest, opt) { // Takes the value of the input and applies it to a target
    var opt = opt || '';
    jQuery(dest).text(jQuery(target).attr('data-title') + opt);
    jQuery('.opt-nodata-hidden').removeClass('opt-nodata-hidden');
}

function scrollTo (element) {
    //console.log('Scrolling to: ', element);
    if (jQuery('body').hasClass('opt-scrolltoready')) {
        jQuery([document.documentElement, document.body]).animate({
            scrollTop: jQuery('.opt-scrolltoready ' +element).offset().top
        }, 1000);
    }
}

function showHideAreas(toShow) {
    jQuery('.opt-initial').removeClass('opt-initial');
    jQuery('.opt-addopacity').removeClass('opt-addopacity');
    //console.log('>>>> ' + toShow);
    switch (toShow) {
        case "opt-account":
            jQuery('#opt-delivery, #opt-deliveryNote, #opt-payment').addClass('opt-initial');
            jQuery('#opt-delivery, #opt-deliveryNote, #opt-payment').addClass('opt-addopacity');
            scrollTo('#opt-account');
            break;
        case "opt-delivery":
            jQuery('#opt-account, #opt-payment').addClass('opt-initial');
            jQuery('#opt-payment').addClass('opt-addopacity');
            scrollTo('#opt-delivery');
            break;
        case "opt-deliveryNote":
            jQuery('#opt-account, #opt-payment').addClass('opt-initial');
            jQuery('#opt-payment').addClass('opt-addopacity');
            scrollTo('#opt-delivery');
            break;
        case "opt-payment":
            jQuery('#opt-delivery, #opt-deliveryNote, #opt-account').addClass('opt-initial');
            scrollTo('#opt-payment');
            setTimeout(function () {
                console.log('here');
                if (jQuery('.loading-mask').css('display') != 'none') {
                    showHideAreas('opt-payment');
                }
            },1000);
            break;
    }
}

function addEditItems(container) {
    jQuery(container + ' .opt-form-title').append('<div class="opt-Edit-Button"><p class="opt-a-Edit">Edit</p></div>');
    jQuery(container + ' .opt-a-Edit').click(function () {
        if (jQuery('body').hasClass('opt-loggedin') && !jQuery('body').hasClass('opt-loggedin-delivery')) {
            document.location = document.location.origin + '/customer/account/';
        } else {
            jQuery('.opt-initial').removeClass('opt-initial');
            showHideAreas(jQuery(this).closest('.opt-step-container').attr('id'));
        }
    });
}

/** Header Changes */

function updateHeader() {
    if (jQuery('.opt-back-link').length <= 0) {
        jQuery('.header.content').prepend('<div class="opt-back-link"><a href="/checkout/cart/">BACK</a></div>');
    }
}

/** Header Banner */

function addBenefitBar() {
    if (jQuery('.opt-benefit').length <= 0 && !jQuery('html').hasClass('mobile')) {
        var benefit = "<div class='opt-benefit'><p>Free delivery on orders over $99 + easy 30 day returns</p></div>";
        jQuery('.page-header').append(benefit);
    } else if (jQuery('.opt-benefit').length <= 0 && jQuery('html').hasClass('mobile')) {
        var benefit = "<div class='opt-benefit'><p>Free delivery over $99 + easy returns</p></div>";
        jQuery('.page-header').append(benefit);
    }
}

/** Form Your Details Functions */

function addTitle(titlenum, titlename) {
    return '<div class="opt-form-title"><div class="opt-step-number"><span>' + titlenum + '</span></div><strong class="opt-title"><span>' + titlename + '</span></strong></div>';
}

function addSignin() {
    elementDefer(function () {
        jQuery('#customer-email-fieldset').before('<div class="opt-signin-cont"><p>Continue as a guest below or <span><a href="opt-signin">Sign In</a></span></div>');
    }, '#customer-email-fieldset');
}

function updateNote() {
    elementDefer(function () {
        var note = 'We’ll send your order confirmation here.';
        jQuery('#opt-account #customer-email-fieldset > div.field.required.custom.ready > div > span.note').before('<div class="opt-note"><span>' + note + '</span></div>');
    }, [
        '#opt-account #customer-email-fieldset > div.field.required.custom.ready > div > span.note'
    ]);
}

function updateNewsletter() {
    elementDefer(function () {
        var news = '<span>Keep my updated with the latest Platypus news.</span>';
        jQuery('.checkout-container .newsletter-signup .label[for="checkout-newsletter-signup"]').html(news);
    }, [
        '.checkout-container .newsletter-signup .label[for="checkout-newsletter-signup"]'
    ]);
}

function addDetailsButton(selector, name, id) {
    var html = '<div class="opt-button"><button class="opt-blue-Button button large primary inverted expanded" id="' + id + '">' + name + '</button></div>';
    jQuery(selector).after(html);
    if (jQuery('#customer-email').val() != undefined && jQuery('#customer-email').val().length > 0) {
        jQuery('#customer-email').addClass('valid');
    }
    jQuery('#' + id).click(function (e) {
        e.preventDefault();
        var local = getDataFromLocal('mage-cache-storage');
        if (jQuery('body').hasClass('opt-showloginstate') && local != undefined &&  local["checkout-data"] != undefined && local["checkout-data"]["validatedEmailValue"] != local["checkout-data"]["checkedEmailValue"] && jQuery('.loading-mask').css('display') == 'none') {
            if (jQuery('#opt-account .opt-email-error').length <= 0) {
                jQuery('#opt-account .opt-button').before('<div class="opt-email-error" style="display: none; color: red;"><span>It looks like your email is incorrect please try a different one or continue as a guest.</span></div>');
                jQuery('.opt-email-error').fadeIn('medium');
                setTimeout(function () {
                    jQuery('.opt-email-error').fadeOut(function () {
                        jQuery('.opt-email-error').remove();
                    });
                }, 4000);
            }
        } else {
            if (jQuery('body').hasClass('opt-showloginstate')) {
                jQuery('.opt-5.opt-showloginstate #opt-account #customer-email-fieldset .actions-toolbar .primary button').click();
            } else {
                if (jQuery('#customer-email').hasClass('valid') && jQuery('#customer-email').val().length > 0) {
                    if (jQuery('.opt-step-container#opt-delivery').length <= 0) {
                        showHideAreas('opt-payment');
                    } else {
                        showHideAreas('opt-delivery');
                    }
                    initialstateshowemail(true);
                }
            }
        }
    });
}

function initialstateshowemail(email) {
    if (jQuery('.opt-initialemail').length <= 0) {
        elementDefer(function () {
            jQuery('#opt-account > .newsletter-signup').after('<div class="opt-initialemail"><span>' + jQuery('#customer-email').val() + '</span></div>');
        }, [
            '#opt-account > .newsletter-signup',
        ]);
    }

    if (email == true) {
        jQuery('.opt-initialemail span').text(jQuery('#customer-email').val());
    } else if (email == undefined) {
        jQuery('#opt-account .opt-form-title').addClass('opt-Tick');
    } else if (email && email.length > 0) {
        jQuery('.opt-initialemail span').text(email);
    } else {
        jQuery('.opt-initialemail span').text(jQuery('#customer-email').val());
    }
}

function createLoginswitch() {
    elementDefer(function () {
        jQuery('.opc-wrapper .form-login').wrapAll('<div class="opt-logincontainer"><div>');
        jQuery('#opt-account .opt-logincontainer').append('<div class="opt-form-toggle"><p>Already have an account? <span class="opt-underline opt-link-text">Sign in here</span></p></div>');
        jQuery('.opt-form-toggle').click(function () {
            if (jQuery('body').hasClass('opt-showloginstate')) {
                jQuery('body').removeClass('opt-showloginstate');
                jQuery('.opt-form-toggle').html('<p>Already have an account? <span class="opt-underline opt-link-text">Sign in here</span></p>');
            } else {
                jQuery('body').addClass('opt-showloginstate');
                jQuery('.opt-form-toggle').html('<p><span class="opt-underline opt-link-text">Return to Guest Checkout</span></p>');
            }
        });
    }, [
        "#opt-account > .opt-form-title"
    ]);
}

/** Init Your Details Sections */
function setupYourDetails() {
    elementDefer(function () {
        //console.log('>>>> Running setupdetails()');
        jQuery('#checkout-step-shipping > button, #checkout-step-shipping .form-login, #checkout-step-shipping > .newsletter-signup').wrapAll('<div class="opt-step-container" id="opt-account"></div>');
        jQuery('.or-separator').remove();
        jQuery('#opt-account').prepend(addTitle('1', 'YOUR DETAILS')); // Add step 1 Your Details area
        addEditItems('#opt-account'); // Add Edit Items Link
        addSignin(); // Add signin area
        // updateNote();
        updateNewsletter();
        addDetailsButton('#opt-account > .newsletter-signup', 'Continue', 'opt-detailcontinue');
        initialstateshowemail();
        createLoginswitch();
        jQuery('#customer-email').on('keydown', function (e) {
            if(e.keyCode === 13 && !jQuery('body').hasClass('opt-showloginstate')) {
                e.preventDefault();
                return false;
            }
        });
    }, [
        "#checkout-step-shipping > button",
        "#checkout-step-shipping .form-login",
        "#checkout-step-shipping > .newsletter-signup"
    ]);
}

function setupYourDetailsCandC(email) {
    elementDefer(function () {
        //console.log('>>>> Running setupdetails()');
        jQuery('#checkout-step-shipping > .newsletter-signup').wrapAll('<div class="opt-step-container" id="opt-account"></div>');
        jQuery('.or-separator').remove();
        jQuery('#opt-account').prepend(addTitle('1', 'YOUR DETAILS')); // Add step 1 Your Details area
        addEditItems('#opt-account'); // Add Edit Items Link
        addSignin(); // Add signin area
        // updateNote();
        updateNewsletter();
        addDetailsButton('#opt-account > .newsletter-signup', 'Continue', 'opt-detailcontinue');
        initialstateshowemail(email);
        createLoginswitch();
        jQuery('#customer-email').on('keydown', function (e) {
            if(e.keyCode === 13 && !jQuery('body').hasClass('opt-showloginstate')) {
                e.preventDefault();
                return false;
            }
        });
        jQuery('#customer-email').click().focus();
    }, [
        '#checkout-step-shipping > .newsletter-signup'
    ]);
}

function setupLoggedInDetails() {
    elementDefer(function () {
        //console.log('>>>> Running Setuploggedindetails()');
        jQuery('#checkout-step-shipping > button, #checkout-step-shipping .form-login, #checkout-step-shipping > .newsletter-signup').wrapAll('<div class="opt-step-container" id="opt-account"></div>');
        jQuery('#opt-account').prepend(addTitle('1', 'YOUR DETAILS')); // Add step 1 Your Details area
        addEditItems('#opt-account'); // Add Edit Items Link
        addSignin(); // Add signin area
        // updateNote();
        //updateNewsletter();
        var email = checkoutConfig.customerData.email || 'tick';
        initialstateshowemail(email);
        // Start Step 2
        showHideAreas('opt-delivery');
        jQuery('#customer-email').click().focus();
    }, [
        '#checkout-step-shipping > button, #checkout-step-shipping .form-login',
        '#opt-delivery',
        '.opt-loggedin-delivery'
    ]);
}

function setupLoggedInNoAddressDetails() {
    elementDefer(function () {
        //console.log('>>>> Running setupLoggedInNoAddressDetails()');
        jQuery('#checkout-step-shipping > button, #checkout-step-shipping .form-login, #checkout-step-shipping > .newsletter-signup').wrapAll('<div class="opt-step-container" id="opt-account"></div>');
        jQuery('#opt-account').prepend(addTitle('1', 'YOUR DETAILS')); // Add step 1 Your Details area
        addEditItems('#opt-account'); // Add Edit Items Link
        addSignin(); // Add signin area
        // updateNote();
        //updateNewsletter();
        var email = checkoutConfig.customerData.email || 'tick';
        initialstateshowemail(email);
        // Start Step 2
        showHideAreas('opt-delivery');
        jQuery('#customer-email').click().focus();
    }, [
        '#checkout-step-shipping > .newsletter-signup',
    ]);
}

function setupClickAndCollect() {
    jQuery('#checkout-step-shipping > button, #checkout-step-shipping .form-login, #checkout-step-shipping > .newsletter-signup').wrapAll('<div class="opt-step-container" id="opt-account"></div>');
}

/** DELIVERY FUNCTION */
function addPhoneNumberSubText() {
    var text = '<small class="opt-textright">For delivery information and updates.</small>';
    jQuery('.field[name="shippingAddress.telephone"] ._with-tooltip > small').before(text);
}

function deliveryingToCompany() {
    elementDefer(function () {
        jQuery('.field[name="shippingAddress.company"]').toggleClass('opt-hide');
        var deliverying = '<div class="opt-delivering"><span>Delivering to a company? </span><a tabindex="-1" id="opt-companyDelivery" href="#" class="opt-underline">Click here</a></div>';
        jQuery('.field[name="shippingAddress.company"]').before(deliverying);
        jQuery('#opt-companyDelivery').click(function () {
            jQuery('.opt-delivering').remove();
            jQuery('.field[name="shippingAddress.company"]').toggleClass('opt-hide');
        });
    }, [
        '.field[name="shippingAddress.company"]'
    ]);
}

function deliveryMethodHTML() {
    var optdelivery = {};
    for (var x = 0; x < jQuery('.checkout-summary:eq(0) .product-item .delivery-info').length; x++) {
        var item = jQuery('.checkout-summary .product-item .delivery-info:eq(' + x + ')').find('.method > span:eq(1)'),
            qty = jQuery('.checkout-summary .product-item:eq(' + x + ') .options-list > .value').text();
            //console.log(qty);
        if (optdelivery[item.text()] && optdelivery[item.text()] > 0) {
            optdelivery[item.text()] = optdelivery[item.text()] + parseInt(qty);
        } else {
            optdelivery[item.text()] = parseInt(qty);
        }

    }

    var html = '';

    for (var x in optdelivery) {
        var items = '';
        if (optdelivery[x] > 1) {
            items = "s";
        }
        html += '<p>' + optdelivery[x] + ' item' + items + ' ' + x + '</p>';
    }
    //console.log(html);
    return html;
}

function deliveryMethodHide() {
    var optdelivery = {};
    jQuery('.opt-delivery-line').addClass('opt-hide');
    for (var x = 0; x < jQuery('.checkout-summary:eq(0) .product-item .delivery-info').length; x++) {
        var item = jQuery('.checkout-summary .product-item .delivery-info:eq(' + x + ')').find('.method > span:eq(1)'),
            qty = jQuery('.checkout-summary .product-item:eq(' + x + ') .options-list > .value').text();
            console.log(item.text().indexOf('Standard'));
        if (item.text().indexOf('Standard') >= 0) {
            jQuery('#opt-method-standard').removeClass('opt-hide');
        }
        if (item.text().indexOf('Next') >= 0) {
            jQuery('#opt-method-Next').removeClass('opt-hide');
        }
        if (item.text().indexOf('Click') >= 0) {
            jQuery('#opt-method-click').removeClass('opt-hide');
        }
    }
}


function addCarefuldontlose (selector, clickselector, showid, type) {
    var html = '<div class="opt-carefulcontainer" id="'+showid+'" style="display: none;"><div class="opt-topcontainer"><img src="//placehold.it/25x25" /><p><strong>Careful! Don\'t lose your pair.</strong></p></div><div class="opt-bottom"><p>To update your delivery method you will be redirected to the cart. <a href="/checkout/cart/">Change Delivery Method</a></p></div></div>';
    switch (type) {
        case "append":
            jQuery(selector).append(html);
            break;
        case "before":
            jQuery(selector).before(html);
            break;
    }
    
    jQuery('body').on('click', clickselector, function (e) {
        e.preventDefault();
        jQuery('#'+showid).toggle();
    });
}


function createDeliveryMethod() {
    jQuery('#opc-delivery_notes').prepend('<div class="opt-DeliveryMethod-Cont"><div class="opt-delivery-container"></div></div>');
    jQuery('.opt-DeliveryMethod-Cont').prepend('<div class="opt-heading opt-title"><span class="opt-title">Delivery Method</span><span id="opt-DeliveryMethod-Edit">Edit</span></div>');
    elementDefer(function () {
        jQuery('.opt-DeliveryMethod-Cont .opt-delivery-container').append('<div class="opt-delivery-line" id="opt-method-standard"><img src="//placehold.it/41x41" ><p><strong>Free</strong> Standard Shipping</p></div><div class="opt-delivery-line" id="opt-method-Next"><img src="//placehold.it/41x41"><p><strong>Next Day</strong> Shipping</p></div><div class="opt-delivery-line" id="opt-method-click"><img src="//placehold.it/41x41"><p><strong>Free</strong> Click and Collect</p></div>');
        deliveryMethodHide();
        addCarefuldontlose('.opt-DeliveryMethod-Cont', '.opt-DeliveryMethod-Cont #opt-DeliveryMethod-Edit', 'opt-deliverymethod-show', "append");
    }, [
        '.checkout-summary .product-item .delivery-info'
    ]);
}

function createDeliveryNote() {
    if (jQuery('.opt-DeliveryNote-Cont').length <= 0) {
        jQuery('.opt-DeliveryMethod-Cont').after('<div class="opt-DeliveryNote-Cont"><div class="opt-show"><span><strong>Delivery Note: </strong></span></div><div class="opt-deliverynote-container"></div></div>');
    }
    
    elementDefer(function () {
        if (jQuery('.opt-deliverynote-container > p').length <= 0) {
            jQuery('.opt-deliverynote-container').append('<p id="opt-deliveryNote-sel"></p>');
            jQuery('#opc-delivery_notes select').change(function () {
                var selectedNote = jQuery(this).val();
                if (selectedNote != '') {
                    jQuery('.opt-DeliveryNote-Cont #opt-deliveryNote-sel').text(selectedNote);
                    jQuery('.opt-DeliveryNote-Cont').addClass('opt-showDeliveryNote');
                } else {
                    jQuery('.opt-DeliveryNote-Cont').removeClass('opt-showDeliveryNote');
                }
            });
        }
    }, [
        '.opt-DeliveryMethod-Cont'
    ]);
}

function initialstateshowdelivery() {
    elementDefer(function () {
        jQuery('#opt-delivery > form').after('<div class="opt-initialdetails opt-nodata-hidden"><div><p class="opt-detailsname"><span><strong>Name: </strong></span><span id="opt-detailsfirstname"></span><span> </span><span id="opt-detailslastname"></span></p><p class="opt-detailscomapny"><p class="opt-detailsaddress"><span><strong>Address: </strong></span><span id="opt-detailsstreet"></span> <span id="opt-detailsstreet1"></span> <span id="opt-detailssuburb"></span> <span id="opt-detailsstate"></span> <span id="opt-detailspost"></span><br><span><strong>Phone: </strong></span><span id="opt-detailsphone"><span><strong>Name: </strong></span></span></p><span id="opt-detailscomapny"></span></p></div></div>');
        jQuery('.field[name="shippingAddress.street.0"] > label > span').text('Address');
        jQuery('.field[name="shippingAddress.city"] > label > span').text('Suburb');
        jQuery('.field[name="shippingAddress.region_id"] > label > span').text('State');
        jQuery('.opt-5.checkout-index-index .fieldset.address .field[name="shippingAddress.telephone"]').appendTo(jQuery('#shipping-new-address-form'));
        jQuery('.opt-5.checkout-index-index .fieldset.address .field[name="shippingAddress.region_id"]').insertBefore(jQuery('.opt-5.checkout-index-index .fieldset.address .field[name="shippingAddress.postcode"]'));
        setTimeout(function () {
            addPhoneNumberSubText();
        },1000);
        jQuery('.input-text[name="telephone"]').click(function () {
            jQuery(this).get(0).setSelectionRange(4,4)
        });
    }, [
        'div[name="shippingAddress.telephone"] input'
    ]);
}

function removeErrors (count) {
    var count = count || 0;
    setTimeout(function () {
        count++;
        if (count <= 20) {
            jQuery('.hosted-control').removeClass('braintree-hosted-fields-invalid');
            removeErrors(count);
        }
    },50);
}

function AddDeliveryButton(selector, name, id) { // Adds a checkout button
    var html = '<div class="opt-button"><button class="opt-blue-Button button large primary inverted expanded" id="' + id + '">' + name + '</button></div>';
    jQuery(selector).after(html);
    jQuery('#' + id).click(function (e) {
        e.preventDefault();
        removeErrors();
        jQuery('body').addClass('opt-removeerror');
        setTimeout(function () {
            jQuery('body').removeClass('opt-removeerror');
        },1000);
        setTimeout(function () {
            jQuery('#braintree, #afterpaypayovertime, #paypal_express').prop('checked',false);
            // jQuery('#sameship1, #sameship0').prop('checked', false).focus().blur().click();
            jQuery('.billing-address-same-as-shipping-block input').each(function () {
                if (jQuery(this).prop('checked') == false) {
                    jQuery(this).focus().blur().click();
                }
            });
            jQuery('.payment-method-title').on('click', function () {
                jQuery('.billing-address-same-as-shipping-block input').each(function () {
                    if (jQuery(this).prop('checked') == false) {
                        jQuery(this).focus().blur().click();
                    }
                });
            })
            jQuery('.billing-address-same-as-shipping-block input').on('click', function () {
                if (jQuery(this).prop('checked') == false) {
                    jQuery('body').addClass('opt-showinformation');
                } else {
                    jQuery('body').removeClass('opt-showinformation');
                }
            });
            jQuery('.billing-address-same-as-shipping-block span').text('Use Shipping Address');
            jQuery('.payment-method').removeClass('_active');
            jQuery('#braintree').prop('checked', true).click();
            jQuery('.payment-method.payment-method-braintree').addClass('_active');
            if (jQuery('body').hasClass('opt-loggedin')) {
                if (jQuery('#shipping-new-address-form .ready._error').length <= 0 && jQuery('.shipping-address-item.selected-item').length > 0) {
                    jQuery('.place-order-container button').click();
                }
            } else {
                jQuery('.place-order-container button').click();
            }
        },200);
    });
}

function checkDisplays () {
    var check = true;
    jQuery('.validation-container.postcode-change-container').each(function () {
        if (jQuery(this).css('display') != 'none') {
            check = false;
        }
    })
    return check;
}

function setupDeliveryArea() {
    elementDefer(function () {
        //console.log('>>>> Running setupDeliveryArea()');
        jQuery('#co-shipping-form').wrapAll('<div class="opt-step-container" id="opt-delivery"></div>');
        if (jQuery('#opt-deliveryNote').length <= 0){
            jQuery('#opc-delivery_notes').wrapAll('<div class="opt-step-container" id="opt-deliveryNote"></div>');
        }
        jQuery('#opt-delivery').prepend(addTitle('2', 'SHIPPING'));
        addEditItems('#opt-delivery'); // Add Edit Items Link
        elementDefer(function () {
            jQuery('div[name="shippingAddress.postcode"]').insertBefore(jQuery('div[name="shippingAddress.region_id"]'));
        }, [
            'div[name="shippingAddress.region_id"]',
            'div[name="shippingAddress.postcode"]'
        ]);
        setTimeout(function () {
            addsubTitle ('#co-shipping-form', 'Deliver To');
        },1000);
        deliveryingToCompany();
        AddDeliveryButton('#opc-delivery_notes', 'Continue', 'opt-Delivery-Continue');
        // jQuery('body').on('click', '#braintree, #afterpaypayovertime, #paypal_express', function(e) {
        //     if (jQuery(this).attr('id') == 'afterpaypayovertime' || jQuery(this).attr('id') == 'braintree') {
        //         jQuery('#billing-address-same-as-shipping-braintree').click();
        //         jQuery('#billing-address-same-as-shipping-braintree').click();
        //     }
        //     // jQuery(this).parent('.payment-method').addClass('_active');
        //     // jQuery(this).closest('.payment-method').find('.opt-billing-toggle-cont input').prop('checked', false);
        //     // jQuery(this).closest('.payment-method').find('.opt-billing-toggle-cont input:eq(0)').prop('checked', true);
        //     // jQuery(this).closest('.payment-method').find('.billing-address-same-as-shipping-block input').prop('checked', false).focus().blur().click();
        // });
        jQuery('#opt-Delivery-Continue').click(function () {
            if (jQuery('#opt-delivery div.mage-error[generated]').length <= 0 && jQuery('#opt-delivery .field[name="shippingAddress.postcode"] input').val().length > 0 && checkDisplays() && jQuery('.field[name="shippingAddress.telephone"] input').val().match(/\d/g).length==11 && jQuery('.field[name="shippingAddress.street.0"] input').val().length > 0 && jQuery('.field[name="shippingAddress.postcode"] input').val().length > 0 && jQuery('.field[name="shippingAddress.city"] input').val().length > 0) {
                updateInitialDetails();
                showHideAreas('opt-payment');
                jQuery('body').removeClass('opt-initial-for-edit');
                jQuery('body').removeClass('opt-initial-for-deliverymethod');
            } else {
                if (jQuery('.opt-noneselected').length < 0) {
                    jQuery('#opt-Delivery-Continue').parent().before('<div class="opt-noneselected"><p>Please select an address to continue.</p></div>');
                }
                setTimeout(function () {
                    jQuery('.opt-noneselected').remove();
                },4000);
            }
        });
        createDeliveryMethod();
        createDeliveryNote();

        initialstateshowdelivery();
    }, [
        '#co-shipping-form'
    ]);
}

function addDeliverToText() {
    jQuery('#opt-delivery .opt-form-title').after('<div class="opt-deliver-to-text"><span>Deliver to</span></div>');
}

function AddnewAddress() {
    jQuery('#opt-delivery .field.addresses').after('<div class="opt-add-new-address"><span>Add new address</span></div>');
    jQuery('.opt-add-new-address').click(function () {
        jQuery('#opt-account > button').triggerHandler('click');
        jQuery('body').addClass('opt-modal-open');
        if (!jQuery('body').hasClass('opt-modal-moved')) {
            jQuery('body').addClass('opt-modal-moved');
            setTimeout(function () {
                jQuery('#opc-new-shipping-address').closest('.modal-popup').insertAfter(jQuery('.opt-add-new-address'));
                //jQuery(button).closemodaltrigger();
            }, 100);
        }
    });
}

function deliveryFunctionality() {
    jQuery('#opt-delivery').on('click', '.shipping-address-item:not(.opt-added-click)', function (e) {
        e.stopPropagation();
        jQuery(this).find('button.action-select-shipping-item').triggerHandler('click');
    }).addClass('opt-added-click');

}

function setupLoggedInDelivery() {
    elementDefer(function () {
        //console.log('>>>> Running setupLoggedinDelivery()');
        jQuery('#checkout-step-shipping > .field.addresses').wrapAll('<div class="opt-step-container" id="opt-delivery"></div>');
        if (jQuery('#opt-deliveryNote').length <= 0){
            jQuery('#opc-delivery_notes').wrapAll('<div class="opt-step-container" id="opt-deliveryNote"></div>');
        }
        jQuery('#opt-delivery').prepend(addTitle('2', 'SHIPPING'));
        addEditItems('#opt-delivery'); // Add Edit Items Link
        setTimeout(function () {
            addsubTitle ('#co-shipping-form', 'Deliver To');
        },1000);
        deliveryingToCompany();
        AddDeliveryButton('#opc-delivery_notes', 'Continue', 'opt-Delivery-Continue');
        jQuery('#opt-Delivery-Continue').click(function () {
            if (jQuery('body').hasClass('opt-loggedin')) {
                if (jQuery('#shipping-new-address-form .ready._error').length <= 0 && jQuery('.shipping-address-item.selected-item').length > 0 && checkDisplays() ) {
                    showHideAreas('opt-payment');
                    jQuery('body').removeClass('opt-initial-for-edit');
                    jQuery('body').removeClass('opt-initial-for-deliverymethod');
                } else {
                    jQuery('#opt-Delivery-Continue').parent().before('<div class="opt-noneselected"><p>Please select an address to continue.</p></div>');
                    setTimeout(function () {
                        jQuery('.opt-noneselected').remove();
                    },4000);
                }
            } else {
                if (jQuery('#shipping-new-address-form .read._error').length <= 0 && checkDisplays()) {
                    showHideAreas('opt-payment');
                    jQuery('body').removeClass('opt-initial-for-edit');
                    jQuery('body').removeClass('opt-initial-for-deliverymethod');
                } else {
                    jQuery('#opt-Delivery-Continue').parent().before('<div class="opt-noneselected"><p>Please select an address to continue.</p></div>');
                    setTimeout(function () {
                        jQuery('.opt-noneselected').remove();
                    },4000);
                }
            }
        });

        addDeliverToText();
        AddnewAddress();
        deliveryFunctionality();

        initialstateshowdelivery();
        createDeliveryMethod();
        createDeliveryNote();
    }, [
        '#checkout-step-shipping > .field.addresses',
        '#opc-delivery_notes'
    ]);
}

/** Payment Area */

function updatePromoPlaceholds(selector, name) {
    jQuery(selector).attr('placeholder', name);
}

function updateAfterPay() {
    jQuery('.opt-5 .afterpay-payment-method .afterpay-checkout-redirect .instalment-footer > span').text('You will be redirected to the Afterpay website. ');
    jQuery('.opt-5 .afterpay-payment-method .afterpay-note-tilte').text(jQuery('.opt-5 .afterpay-payment-method .afterpay-note-tilte').text().toLowerCase());
}

function toggleOn($selector) {
    //console.log('on ',$selector);
    $selector.find('.billing-address-same-as-shipping-block input').prop('checked', false).focus().blur().click();
}

function toggleOff($selector) {
    //console.log('off ',$selector);
    $selector.find('.billing-address-same-as-shipping-block input').prop('checked', true).focus().blur().click();
}


// NOT USED CURRENTLY AS CONTROL FUNCTIONALITY IS NOT CONDUCIVE
function billingToggleFunctionality() {
    jQuery('.opt-billing-toggle-cont input').click(function () {
        var val = jQuery(this).attr('id');

        var $el = jQuery(this).closest('.opt-billingaddresstoggle').parent();
        if (val.indexOf('sameship') >= 0) {
            // needs to be toggled on
            toggleOn($el);
        } else {
            toggleOff($el);
        }
    });
}

// NOT USED CURRENTLY AS CONTROL FUNCTIONALITY IS NOT CONDUCIVE
function addBillingToggle() {
    for (var i = 0; i < jQuery('.billing-address-same-as-shipping-block').length; i++) {
        jQuery('.billing-address-same-as-shipping-block:eq(' + i + ')').after('<div class="opt-billingaddresstoggle"><div class="opt-billing-header"><span class="opt-title">Billing Address</span></div><div class="opt-billing-toggle-cont"><div class="field"> <input type="radio" name="optshippingtoggle' + i + '" class="radio" id="sameship' + i + '""> <label class="label" for="sameship' + i + '">Same as shipping address</label> </div><div class="field"> <input type="radio" name="optshippingtoggle' + i + '" class="radio" id="diffship' + i + '""> <label class="label" for="diffship' + i + '">Use a different billing address</label> </div></div></div>');
        jQuery('#sameship' + i).prop('checked', true);
    }
    billingToggleFunctionality();

    setTimeout(function () {
        if (jQuery('.opt-billingaddresstoggle').length <= 0) {
            addBillingToggle();
        }
    },1000);
    // jQuery('.payment-method-title > input').click(function () {
    //     jQuery('#sameship1, #sameship0').prop('checked', false).focus().blur().click();
    // });
}

function addCCVTooltip() {
    jQuery('.payment-method.payment-method-braintree .payment-method-content .checkout-agreements-block').after('<div class="opt-ccvtooltip"><span>What is my CCV?</span></div>');
    jQuery('.opt-ccvtooltip').click(function () {
        jQuery('.cvv .field-tooltip .field-tooltip-content').toggleClass('_active').toggle();
    });
}

function updatePayPal() {
    jQuery('.payment-method.item .payment-method-title').after('<div class="opt-Paypal-message"><span>You will be redirected to the PayPal website.</span></div>');
    jQuery('.payment-method.item .payment-method-title .payment-icon').attr('src', '//cdn.optimizely.com/img/6092490016/2ecf5aa213c1429e9717b9e0019371a8.png');
}

function setupPaymentArea(step) {
    elementDefer(function () {
        var placeordermessage = '<div class="opt-tnc-message">By clicking Place Order, you agree to the <span></span><a target="_blank" href="https://www.skechers.com.au/terms-and-conditions">Terms & Conditions</a></div>';
        //console.log('>>>> Running SetupPaymentArea()');
        jQuery('#payment').wrapAll('<div class="opt-step-container" id="opt-payment"></div>');
        jQuery('#opt-payment').prepend(addTitle(step, 'PAYMENT'));
        jQuery('.payment-option.discount-code, .payment-option.giftcardaccount ').wrapAll('<div class="opt-giftCardSection"></div>');
        jQuery('.opt-giftCardSection').prepend('<div class="opt-giftTitle"><span>Apply Gift Card or Promo Code</span></div>');
        jQuery('.opt-giftTitle').click(function () {
            jQuery('.opt-giftCardSection').toggleClass('opt-open');
        });
        jQuery('.payment-option.discount-code').appendTo('.opt-giftCardSection');
        jQuery('.payment-option.discount-code').before('<div class="opt-bluesep"></div>');

        updatePromoPlaceholds('#giftcard-code', 'Gift Card Number');
        updatePromoPlaceholds('#giftcard-pin', 'Pin');
        updatePromoPlaceholds('#discount-code', 'Promo Code');
        
        

        elementDefer(function () {
            addButton('.payment-method.item .payment-method-title', 'Pay Now With PayPal', 'opt-paypalbutton');
            //jQuery('#opt-paypalbutton').after(placeordermessage);
        }, [
            '.payment-method.item .payment-method-title'
        ]);
        elementDefer(function () {
            addButton('.checkout-payment-method .afterpay-payment-method .payment-method-content .payment-method-content', 'Pay Now With Afterpay', 'opt-paynowwithafterpay');
            //jQuery('#opt-paynowwithafterpay').after(placeordermessage);
        }, [
            '.checkout-payment-method .afterpay-payment-method .payment-method-content .payment-method-content'
        ]);
        elementDefer(function () {
            addButton('.payment-method.payment-method-braintree .payment-method-content .checkout-agreements-block', 'Place Order', 'opt-placeorderbutton');
            jQuery('#opt-placeorderbutton').after(placeordermessage);
        }, [
            '.payment-method.payment-method-braintree .payment-method-content .checkout-agreements-block'
        ]);

        addCCVTooltip();

        updateAfterPay();

        updatePayPal();
        
        elementDefer(function () {
            jQuery('div[name="billingAddressbraintree.postcode"]').insertBefore(jQuery('div[name="billingAddressbraintree.region_id"]'));
        }, [
            'div[name="billingAddressbraintree.region_id"]',
            'div[name="billingAddressbraintree.postcode"]'
        ]);
        elementDefer(function () {
            jQuery('div[name="billingAddressafterpaypayovertime.postcode"]').insertBefore(jQuery('div[name="billingAddressafterpaypayovertime.region_id"]'));
        }, [
            'div[name="billingAddressafterpaypayovertime.region_id"]',
            'div[name="billingAddressafterpaypayovertime.postcode"]'
        ]);

    }, [
        '#payment',
        '#co-transparent-form-braintree #payment_form_braintree'
    ]);
}

function mobilePaymentSectionUpdates() {
    elementDefer(function () {
        jQuery('.block.discount').appendTo('.mobile .opt-5.checkout-index-index .promo-gift>.content');
        jQuery('.block.discount').before('<div class="opt-bluesep"></div>');
        jQuery('.mobile .opt-5 .form.giftcard .field:not(.custom) .control span').text('Check Balance');
        var giftCardText = 'Enter your gift card number & pin and click apply to update your order.',
            promoCodeText = 'Enter your promotional code and click apply to update your order.';

        jQuery('.mobile .opt-5.checkout-index-index .promo-gift > .content > p').text(giftCardText);
        jQuery('.mobile .opt-5 .promo-gift .content > .block.discount').before('<p class="text"><span>' + promoCodeText + '</span></p>');
        setTimeout(function () {
            addsubTitle ('#checkout-payment-method-load', 'Select Payment Method');
        },1000);
        jQuery('.mobile .opt-5.checkout-index-index .promo-gift.active > .title span').text('Apply Gift Card or Promo Code');
    }, [
        ".block.discount",
        ".mobile .opt-5 .form.giftcard .field:not(.custom) .control span"
    ]);
}

function mobileFooter() {
    jQuery('.checkout-index-index .secure-pay-wrapper').before('<div class="secure-pay-wrapper opt-securePay"><div class="info-block"><strong class="block-title">Secure ways to pay</strong> <img src="//cdn.optimizely.com/img/6092490016/4ef88c891f864f38967caffd7de895bd.png" "="" alt="Secure pay"></div><div class="trust-block"><img src="//cdn.optimizely.com/img/6092490016/39606060f8244231aa2b13d529603735.png" alt=""> <strong class="trust-label">SHOP IN CONFIDENCE</strong><p>We take credit card security very seriously. <br>All transactions are transmitted via a secure certificate and we never store your credit card details.</p></div></div>');
}


/** Form Setup */



/** Sidebar */

function addSidebarTitle() {

    elementDefer(function () {
        if (jQuery('.opt-sidebar-header').length <= 0) {
            jQuery('.checkout-container .checkout-summary>.items>.title').before('<div class="opt-sidebar-header"><strong class="opt-title"><span>Summary</span><a href="/checkout/cart/" class="link _right">Edit Cart</span></strong></div>');
        }
        jQuery('.opt-5.checkout-index-index .secure-pay-wrapper .info-block img').attr('src', '//cdn.optimizely.com/img/6092490016/4ef88c891f864f38967caffd7de895bd.png');
        jQuery('.opt-5.checkout-index-index .secure-pay-wrapper .trust-block > img').attr('src', '//cdn.optimizely.com/img/6092490016/39606060f8244231aa2b13d529603735.png');

    }, [
        ".checkout-container .checkout-summary>.items>.title span"
    ]);
}

function modal() {
    elementDefer(function () {
        jQuery("body").append("<div class='opt-overlay' style='display: none;'><div class='opt-modal'><div class='opt-modal-header'><div class='opt-modal-head'></div><span>This will take you back to your cart<span class='opt-hide'> to change your delivery method</span>.</span></div><div class='opt-modal-buttons'><button class='btn btn-primary opt-stay-here'>STAY HERE</button><button href='/checkout/cart/' class='btn btn-primary opt-modal-back-to-cart'>BACK TO CART</button></div></div></div></div>");

        jQuery('.opt-modal-back-to-cart').click(function () {
            document.location = document.location.origin + jQuery(this).attr('href');
        });

        jQuery(".opt-modal").append("<div class='opt-close'></div> ");

        jQuery(".opt-modal").click(function (event) {
            event.stopPropagation();
        });

        jQuery('body').on('click', 'div.link > a.link[title="Edit"], .opt-sidebar-header .opt-title a.link, #opt-DeliveryMethod-Edit', function (e) {
            e.preventDefault();
            jQuery('body').removeClass('opt-editcartclick');
            if (e.target.classList.value == 'link _right') {
                jQuery('body').addClass('opt-editcartclick');
            }
            jQuery(".opt-overlay").fadeIn("slow");
            jQuery("body").css({
                "height": "100%",
                "overflow": "hidden"
            });
        });

        jQuery(".opt-close, .opt-overlay, .opt-stay-here").click(function () {
            jQuery(".opt-overlay").fadeOut("slow", function () {
                jQuery('body').removeClass('opt-editcartclick');
            });
            jQuery("body").css({
                "height": "auto",
                "overflow": "inherit"
            });
        });
    }, [
        '#opt-payment',
        '#co-transparent-form-braintree #payment_form_braintree'
    ]);
}

function updateSubTotalArea() {
    elementDefer(function () {
        jQuery('.opt-5 .checkout-container .table-totals .shipping .mark span').text('Shipping');
        jQuery('.opt-5 .checkout-container .table-totals .totals.sub .mark').text(jQuery('.opt-5 .checkout-container .checkout-summary>.items>.title').text().trim().split('in')[0]);
        jQuery('.opt-5 .checkout-container .table-totals .grand .mark strong').text('Total');
    }, [
        '.opt-5 .checkout-container .table-totals .shipping .mark span',
        '.opt-5 .checkout-container .table-totals .totals.sub .mark',
        '.opt-5 .checkout-container .table-totals .grand .mark strong',
        '.opt-5 .checkout-container .checkout-summary>.items>.title'
    ]);
}

function updateSidebar() {
    addSidebarTitle();
    updateSubTotalArea();
    // modal();
}


/** Init */

// Need to detect if its click and collect and show all content 

function detectStep() {
    var step = 'none';
    jQuery('#checkoutSteps').find(' > li').each(function () {
        if (jQuery(this).css('display') != 'none') {
            step = jQuery(this).attr('id');
        }
    });
    return step;
}

function detectCandC() {
    var data = checkoutConfig.quoteItemData,
        CandCOnly = true;
    for (var i = 0; i < data.length; i++) {
        if (data[i].shipping_method != 'collect_collect') {
            CandCOnly = false;
            return CandCOnly;
        }
    }
    return CandCOnly;
}

function mainJs() {
    // First detect the type of checkout we are working on.. ie. Click and collect, logged in etc.
    var loggedIn = checkoutConfig.isCustomerLoggedIn,
        step = detectStep(),
        CandC = detectCandC(),
        stepNums = {
            "Your Details": 1,
            "Delivery": 2,
            "Payment": 3
        };
    //console.log('Loading In', loggedIn, step);
    if (step == 'none') {
        setTimeout(function () {
            mainJs();
        }, 50);
        return;
    }
    jQuery('body').addClass('opt-initial-for-deliverymethod');
    switch (loggedIn) {
        case true:
            jQuery('body').addClass('opt-loggedin');
            if (step == 'shipping' || step == 'opc-delivery_notes') {
                //console.log('>> Running Logged in starting at your Delivery');
                if (CandC == false && jQuery('.shipping-address-items').length <= 0) {
                    setupLoggedInNoAddressDetails();
                    //console.log(">>> Not Click and Collect and Shipping details 0. Adding Delivery");
                    jQuery('body').addClass('opt-loggedin-delivery');
                    
                    setupLoggedInDelivery();
                    window.optCheckerCount = 0;
                    var loadingchecker = setInterval(function () {
                        if (jQuery('.loading-mask').css('display') == 'none') {
                            if (jQuery('.shipping-address-items .shipping-address-item').length <= 0) {
                                setupDeliveryArea();
                                clearInterval(loadingchecker);
                            }
                        } else if (optCheckerCount == 100) {
                            clearInterval(loadingchecker);
                        }
                    },100);
                } else if (CandC == false) {
                    setupLoggedInDetails();
                    //console.log(">>> Not Click and Collect Only. Adding Delivery");
                    jQuery('body').addClass('opt-loggedin-delivery');
                    setupLoggedInDelivery();
                } else {
                    setupYourDetailsCandC();
                    //console.log(">>> Click and Collect Only. Not Adding Delivery");
                    stepNums.Payment = 2;
                }
                setupPaymentArea(stepNums.Payment);
                elementDefer(function () {
                    showHideAreas('opt-delivery');
                    //addBillingToggle();
                    jQuery('body').addClass('opt-scrolltoready');
                }, [
                    '#opt-delivery',
                    '#opt-payment'
                ]);
            } else if (step == 'payment') {
                //console.log('>> Running Logged in starting at your Payment');
                setupLoggedInDetails();
                if (CandC == false) {
                    //console.log(">>> Not Click and Collect Only. Adding Delivery");
                    setupLoggedInDelivery();
                } else {
                    setupYourDetailsCandC(JSON.parse(localStorage.getItem('mage-cache-storage')).customer.email);
                    //console.log(">>> Click and Collect Only. Not Adding Delivery");
                    stepNums.Payment = 2;
                }
                setupPaymentArea(stepNums.Payment);
                elementDefer(function () {
                    showHideAreas('opt-account');
                    jQuery('#opt-delivery').addClass('opt-Tick');
                    jQuery('#opt-account').addClass('opt-Tick');
                    //addBillingToggle();
                    jQuery('body').addClass('opt-scrolltoready');
                }, [
                    '#opt-payment',
                ]);
            }
            break;
        case false:
            jQuery('body').addClass('opt-not-loggedin');
            if (step == 'shipping' || step == 'opc-delivery_notes') {
                //console.log('>> Running Not Logged in starting at your Details');
                setupYourDetails();
                if (CandC == false) {
                    //console.log(">>> Not Click and Collect Only. Adding Delivery");
                    setupDeliveryArea();
                } else {
                    //console.log(">>> Click and Collect Only. Not Adding Delivery");
                    stepNums.Payment = 2;
                }
                setupPaymentArea(stepNums.Payment);
                elementDefer(function () {
                    showHideAreas('opt-account');
                    //addBillingToggle();
                    jQuery('body').addClass('opt-scrolltoready');
                }, [
                    '#opt-account',
                    '#co-transparent-form-braintree #payment_form_braintree'
                ]);
            } else if (step == 'payment') {
                //console.log('>> Running Not Logged in starting at your Payment');
                setupYourDetails();
                if (CandC == false) {
                    //console.log(">>> Not Click and Collect Only. Adding Delivery");
                    setupDeliveryArea();
                } else {
                    //console.log(">>> Click and Collect Only. Not Adding Delivery");
                    stepNums.Payment = 2;
                }
                setupPaymentArea(stepNums.Payment);
                elementDefer(function () {
                    showHideAreas('opt-account');
                    //addBillingToggle();
                    jQuery('body').addClass('opt-scrolltoready');
                }, [
                    '#opt-payment',
                    '#opt-account',
                    '#co-transparent-form-braintree #payment_form_braintree'
                ]);
            }
            break;
    }
}

/** MOBILE FUNCTIONS */
function updatePaymentSummary() {
    elementDefer(function () {
        jQuery('#opt-payment .checkout-summary, #opt-payment .place-order-container').wrapAll('<div class="opt-summary-cont" id="opt-mobile-summary"></div>');
        jQuery('#opt-mobile-summary .place-order-container button').text('Place Order');
        jQuery('#opt-payment #opc-sidebar').insertAfter(jQuery('.mobile .opt-5.checkout-index-index .opc-wrapper'));
        jQuery('.paypal-payment-method > .title').wrapAll('<div class="payment-method-title field choice"></div>');
        if (jQuery('.paypal-payment-method > small').length > 0) {
            jQuery('.paypal-payment-method > small').prependTo(jQuery('.paypal-payment-method .payment-method-content'));
        }
    }, [
        ".opt-5 #opt-payment .checkout-summary",
        ".opt-5 #opt-payment .place-order-container"
    ]);
}

function mobileJs() {
    updatePaymentSummary();
}

function init() {
    if (!window.opt_running) {
        window.opt_running = true;
        jQuery('body').addClass('opt-5 opt-5-1');
        // Always on changes
        updateHeader();
        updateSidebar();
        elementDefer(function () {
            jQuery('body').on('click', '.validation-container.postcode-change-container button.button.primary', function () {
                var checking = setInterval(function () {
                    if (jQuery('.loading-mask').css('display') == 'none') {
                        clearInterval(checking);
                        jQuery('.opt-DeliveryMethod-Cont').remove();
                        createDeliveryMethod();
                    }
                },100);
            })
        }, [
            ".validation-container.postcode-change-container"
        ])
        if (jQuery('html').hasClass('mobile')) {
            if (window.location.hash != '#payment') {
                window.location = window.location + '#payment';
            }
            elementDefer(function () {
                mainJs();
                mobileJs();
                mobilePaymentSectionUpdates();
                mobileFooter();
            }, [
                "#checkoutSteps > li"
            ]);
        } else {
            elementDefer(function () {
                mainJs();
            }, [
                "#checkoutSteps > li"
            ]);
        }
    }
}

defer(function () {
    init();
  	// window.location.hash = "#payment";
}, 'body');