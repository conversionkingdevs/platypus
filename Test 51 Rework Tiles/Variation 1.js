$ = jQuery.noConflict();


/**************************************
    LAYOUT
**************************************/
$('body').addClass('OptimizelyTest'); 
jQuery(".OptimizelyTest .amasty-label-image").attr('src','//cdn.optimizely.com/img/6092490016/f58d8cdfef174c6eb117b6f225e35c36.png');

jQuery('window').ajaxComplete(function () {
	jQuery(".OptimizelyTest .amasty-label-image").attr('src','//cdn.optimizely.com/img/6092490016/f58d8cdfef174c6eb117b6f225e35c36.png');
});


/**************************************
    STYLES
**************************************/
$('head').append(`
<style type="text/css">
    .OptimizelyTest .products-grid .product-item .product-item-info {height:290px; padding-top:10px;}
    .OptimizelyTest .products-grid .product-item .listing-swatch {display:none !important;}
    .OptimizelyTest .products-grid .product-item .product-item-name {margin:0; padding:0; border:none;}
    .OptimizelyTest .products-grid .product-item .product-item-details {border:none;}
    .OptimizelyTest .products-grid .product-item .product-item-photo {min-height:209px;}
    .OptimizelyTest .products-grid .product-item .product-image-photo {top:-30px; height:100%; width:auto;}
    .OptimizelyTest .products-grid .product-item .amasty-label-image {width:135px !important; float:left;}
    .OptimizelyTest .products-grid .product-item.cms-item .product-item-info {padding-top:0px;}
    .OptimizelyTest .products-grid .product-item.cms-item img {height:170px; object-fit:cover;}
    .OptimizelyTest .products-grid .product-item .cms-product-wrapper p:first-of-type {font-size:20px !important; padding:0 !important; margin-bottom:4px;}
    
    
    @media only screen and (max-width: 1100px){
        .OptimizelyTest .products-grid .product-item .product-item-info {height:226px;}
        .OptimizelyTest .products-grid .product-item .product-item-photo {min-height:149px;}
        .OptimizelyTest .products-grid .product-item .cms-product-wrapper p:first-of-type {font-size:16px !important; margin-bottom:4px; margin-top:3px;}
        .OptimizelyTest .products-grid .product-item.cms-item img {height:140px;}
        .OptimizelyTest .products-grid .product-item.cms-item br {display:none;}
    }
    
    @media only screen and (max-width: 750px){
        .OptimizelyTest .products-grid .product-item .product-item-info {height:217px; padding-top:35px;}
        .OptimizelyTest .products-grid .product-item .product-image-photo {top:-56px; height:180px; left:auto; position:relative; transform:none !important; max-height:none !important; padding:0 !important;}
        .OptimizelyTest .products-grid .product-item .product-item-photo {min-height:0; height:100px; display:block; float:none; padding:0; margin:0; overflow:visible;}
        .OptimizelyTest .products .product-item .product-item-details {padding-top:0;}    
        .OptimizelyTest .products .product-item .product-item-name {min-height:47px;}
        .OptimizelyTest .products-grid .product-item .amasty-label-image {width:100px !important; margin-top:-30px;}
    }
</style>
`);