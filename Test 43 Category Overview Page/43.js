function start() {
    jQuery(".OptimizelyTest #amasty-shopby-product-list").html('');
    
    
    jQuery(".OptimizelyTest #amasty-shopby-product-list").append('<div class="top-categories" />');
    jQuery(".OptimizelyTest #amasty-shopby-product-list").append('<div class="brands"><div class="flexslider"><a href="#" class="prev-arrow" title="Previous"><svg xmlns="http://www.w3.org/2000/svg" class="svg-icon svg-icon-arrow-left"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-arrow-left"></use></svg></a><ul class="brands-list slides"></ul><a href="#" class="next-arrow" title="Next"><svg xmlns="http://www.w3.org/2000/svg" class="svg-icon svg-icon-arrow-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-arrow-right"></use></svg></a></div></div>');
    jQuery(".OptimizelyTest #amasty-shopby-product-list").append('<div class="middle-categories" />');
    jQuery(".OptimizelyTest #amasty-shopby-product-list .middle-categories").append('<div class="column column-1" /><div class="column column-2" /><div class="column column-3" />');
    jQuery(".OptimizelyTest #amasty-shopby-product-list").append('<div class="bottom-categories" />');
    
    
          
          
    /**************************************
        CATEGORIES DATA
    **************************************/
    categories_data = [
        {
            'title' : 'New Arrivals',
            'link'  : 'https://www.platypusshoes.com.au/mens/new-arrivals.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/new-arrivals.jpg'    
        },
        {
            'title' : 'Shop Sale',
            'link'  : 'https://www.platypusshoes.com.au/mens/sale.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/shop-sale.png'    
        },
        {
            'title' : 'Sneakers',
            'link'  : 'https://www.platypusshoes.com.au/mens/sneakers.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/sneakers.jpg'    
        },
        {
            'title' : 'Boots',
            'link'  : 'https://www.platypusshoes.com.au/mens/boots.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/boots.jpg'    
        },
        {
            'title' : 'Slides',
            'link'  : 'https://www.platypusshoes.com.au/mens/slides-sandals.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/slides.jpg'    
        },
        {
            'title' : 'Shoes',
            'link'  : 'https://www.platypusshoes.com.au/mens/shoes.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/shoes.jpg'    
        },
        {
            'title' : 'T-SHIRTS',
            'link'  : 'https://www.platypusshoes.com.au/mens/t-shirts.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/tshirt.jpg'    
        },
        {
            'title' : 'All Products',
            'link'  : 'https://www.platypusshoes.com.au/mens.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/all-products.jpg'    
        }
    ];
    
    CategoriesData(categories_data);
    
    
    
    
    /**************************************
        CATEGORIES MODULE
    **************************************/
    function CategoriesData(data){
        var i=1, append_to;
        jQuery.each(data, function( key, item ){
            if(jQuery(window).width() > 1100){
                if(i == 1 || i == 2){
                    append_to = '.top-categories';
                } else if(i == 3){
                    append_to = '.middle-categories .column-1';
                } else if(i == 4 || i == 5){
                    append_to = '.middle-categories .column-2';
                } else if (i == 6){
                    append_to = '.middle-categories .column-3';
                } else if (i == 7 || i == 8){
                    append_to = '.bottom-categories';
                };
            } else {
                if(i == 1 || i == 2){
                    append_to = '.top-categories';
                } else if(i == 3){
                    append_to = '.middle-categories .column-1';
                } else if(i == 4 || i == 5){
                    append_to = '.middle-categories .column-1';
                } else if (i == 6){
                    append_to = '.middle-categories .column-1';
                }
                 else if (i == 7 || i == 8){
                    append_to = '.bottom-categories';
                };
            };
            
            jQuery(".OptimizelyTest #amasty-shopby-product-list " + append_to).append('<div class="category-item category-item-'+i+'"><a href="'+item['link']+'"><img src="'+item['image']+'" /><span class="title">'+item['title']+'</span></a></div>');
                
            i++;
        });
    };
    
    
    
    
    /**************************************
        BRANDS DATA 
    **************************************/
    brands_data = [
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/adidas.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/ADIDAS.PNG'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/asics_tiger.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/AsicsTiger.PNG'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/birkenstock.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/birkenstock.png'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/boxfresh.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/boxfresh-logo.PNG'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/converse.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/converse.PNG'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/dr_martens.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/dr martens.png'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/flex_fit.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/flexfit.png'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/lacoste.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/lacoste.png'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/majestic.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/majestic.png'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/mitchell_ness.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/mitchell.png'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/new_balance.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/new balance.png'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/nike.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/nike.png'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/onitsuka_tiger.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/onitsuka tiger.png'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/palladium.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/palladium.png'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/puma.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/puma.png'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/reebok.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/rebook.png'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/skechers.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/skechers.png'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/sperry.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/sperry.png'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/stance_socks.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/stance.png'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/supra.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/supra.png'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/timberland.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/timberland.png'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/undefeated.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/undefeated.png'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/vans.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/vans.png'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/vans_apparel_and_accessories.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/vans.png'
        },
        {
            'link'  : 'https://www.platypusshoes.com.au/mens/clae_1.html',
            'image' : 'https://www.platypusshoes.com.au/media/wysiwyg/platypusshoes-categories%202/platypusshoes-categories/mens/images/brands/clae.png'
        }   
    ];
    
    BrandsData(brands_data);
    
    /**************************************
        BRANDS MODULE
    **************************************/
    function BrandsData(data){
        jQuery.each(data, function( key, item ){
            jQuery(".OptimizelyTest #amasty-shopby-product-list .brands .brands-list").append('<li><a class="link" href="'+item['link']+'"><img src="'+item['image']+'" /></a></li>');
        });
    };
    
    
    
    
    /**************************************
        BRANDS SLIDER
    **************************************/
    setTimeout(function(){
        var jQuerywindow = jQuery(window),
            flexslider = { vars:{} };
     
        function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
               (window.innerWidth < 770) ? 4 :
               (window.innerWidth < 1030) ? 8 :
               (window.innerWidth < 1290) ? 10 :
               (window.innerWidth < 1610) ? 12 :
               (window.innerWidth < 1930) ? 14 : 16;
        }
          
        jQuery('.flexslider').flexslider({
            animation: "slide",
            animationLoop: true,
            controlNav: false,
            directionNav: false,
            itemWidth: 210,
            itemMargin: 0,
            move: 1,
            minItems: getGridSize(),
            maxItems: getGridSize(),
            start:function(slider){
                jQuery('.next-arrow').click(function(event){
                    event.preventDefault();
                    slider.flexAnimate(slider.getTarget("next"));
                });
                jQuery('.prev-arrow').click(function(event){
                    event.preventDefault();
                    slider.flexAnimate(slider.getTarget("previous"));
                });
            }
        });
         
        jQuery(window).resize(function() {
            var gridSize = getGridSize();
            flexslider.vars.minItems = gridSize;
            flexslider.vars.maxItems = gridSize;
        });    
    },500);
    
    }
    
    function getScript(src, callback) {
        var s = document.createElement('script');
        s.src = src;
        s.async = true;
        s.onreadystatechange = s.onload = function() {
          if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
            callback.done = true;
            callback();
          }
        };
        document.querySelector('head').appendChild(s);
      }
    
    function defer(method, selector) {
        if (window.jQuery) {
            if (jQuery(selector).length > 0){
                method();
            } else {
                setTimeout(function() { defer(method, selector); }, 50);
            }  
        } else {
             setTimeout(function() { defer(method, selector); }, 50);
        }    
    }
    
    function startIt() {
      getScript("https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.4/jquery.flexslider-min.js", start);
    }
    
    jQuery(window).ajaxComplete(function () {
      if (!jQuery('body').hasClass('OptimizelyTest')) {
    
    /**************************************
        LAYOUT
    **************************************/
    jQuery('body').addClass('OptimizelyTest'); 
        
            text = {
          "mens":"Platypus brings you a wide variety of men’s footwear and apparel, including Timberland and Dr Martens boots for adventurers, Vans and Converse for sneaker freaks and Adidas and Nike for those on-the-go.",
          "womens":"Find the best fit for you with our wide variety of women’s footwear. Platypus stock Timberland and Dr Martens boots for festival-goers, Vans and Converse to pair with every outfit and adidas and Nike for a sport-inspired finish, plus heaps more.",
          "kids":"It’s their time to shine with our wide variety of kid’s footwear. Platypus brings you the best including Vans and Converse for standout street-wear, Timberland and Dr Martens boots for mini wanderers, and adidas and Nike for ultimate comfort."
      };
      if (jQuery('.category-description').length == 0) {
          if (document.location.href.indexOf('kids.html') >= 0 && jQuery('.optText').length == 0) {
              jQuery('.page-title-wrapper').append("<div class='optText'>"+text["kids"]+"</div>");
          } else if (document.location.href.indexOf('womens.html') >= 0 && jQuery('.optText').length == 0) {
              jQuery('.page-title-wrapper').append("<div class='optText'>"+text["womens"]+"</div>");
          } else if (document.location.href.indexOf('mens.html') >= 0 && jQuery('.optText').length == 0) {
              jQuery('.page-title-wrapper').append("<div class='optText'>"+text["mens"]+"</div>");
          }
      }
    
    
    
      var mobile = false;
      jQuery(window).resize(function(){
          if(mobile == false && jQuery(window).width() <= 1100){
              mobile = true;
                      start();
          }
    
          if(mobile == true && jQuery(window).width() > 1100){
              mobile = false;
              start();
          }
    
      });
        
      defer(startIt, '.product-item');
      
      }
    });