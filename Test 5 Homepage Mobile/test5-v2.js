function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();  
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(function() {
    jQuery('body').addClass('opt5');

    jQuery('.opt5 .header.search, .opt5 .page-main').hide();
    jQuery('.page-wrapper').append('<div class="opt5-main" />');

    jQuery('<div class="opt5-delivery"><span>Same day delivery</span><img src="//cdn.optimizely.com/img/6092490016/994e1b11b52d479aa90fe4fc03cae066.png" /></div>').appendTo('.opt5-main');
    jQuery('<div class="opt5-banner"></div>').appendTo('.opt5-main');
    jQuery('<div class="opt5-menu container"></div>').appendTo('.opt5-main');
    


    // BANNER
    jQuery('<div class="container flexslider-banner"><ul class="slides"></ul></div>').appendTo('.opt5-banner');

    var bannerimages = [
        {
            image: "//placehold.it/250x250",
            url: "#"
        },
        {
            image: "//placehold.it/250x250",
            url: "#"
        },
        {
            image: "//placehold.it/250x250",
            url: "#"
        }        
    ]

    bannerimages.forEach(function (el, i) {
        jQuery('<li><a href="'+el.url+'"><img src="'+el.image+'" ><button>SHOP NOW</button></a></li>').appendTo('.opt5-banner ul');
    });

    var menuLayout = [
        {
            text: 'New',
            link: 'https://www.platypusshoes.com.au/new-arrivals.html',
            img: '//placehold.it/200x200'
        }, {
            text: 'Womens',
            link: 'https://www.platypusshoes.com.au/womens.html',
            img: '//placehold.it/200x200'
        }, {
            text: 'Mens',
            link: 'https://www.platypusshoes.com.au/mens.html',
            img: '//placehold.it/200x200'
        }, {
            text: 'Kids',
            link: 'https://www.platypusshoes.com.au/kids.html',
            img: '//placehold.it/200x200'
        }, {
            text: 'Accessories',
            link: 'https://www.platypusshoes.com.au/accessories.html',
            img: '//placehold.it/200x200'
        }, {
            text: 'Sale',
            link: '/sale.html',
            img: '//placehold.it/200x200'
        }
    ];

    menuLayout.forEach(function(element, index) {
        var c = (index === (menuLayout.length - 1) || index === (menuLayout.length - 2)) ? 'no-border-bottom' : '';
        jQuery('<div class="'+c+'" style="background-image:url('+element.img+');"><a  href="'+element.link+'">'+element.text+'</a></div>').appendTo('.opt5-menu');
    });

    function slider() {
        jQuery(".flexslider").flexslider({
            animation: "carousel",
            animationLoop: false,
            itemWidth: 260,
            itemMargin: 15,
            minItems: 2,
            move: 1,
            initDelay: 2000
        });
        jQuery(".flexslider-banner").flexslider({
            animation: "slide",
            initDelay: 5000,
            controlNav: true
        });   
        jQuery(".opt-brands").flexslider({
            animation: "slide",
            initDelay: 5000,
            controlNav: true,
            minItems: 3,
            itemWidth: 100,
            itemMargin: 10,
            maxItems: 4,
            move: 1
        });   
    }

    jQuery('<div class="opt5-title"><span>New Arrivals</span></div>').appendTo('.opt5-main');
    jQuery('<div class="opt5-arrivals container flexslider"><ul class="slides"></ul></div>').appendTo('.opt5-main');

    var arrivals = [
        {
            image: 'https://via.placeholder.com/210',
            price: '$150.00',
            brand: 'Adidas',
            name: 'Mens Yung 96',
            link: 'https://www.platypusshoes.com.au/mens-yung-96.html'
        }, {
            image: 'https://via.placeholder.com/210',
            price: '$150.00',
            brand: 'Adidas',
            name: 'Womens Falcon',
            link: 'https://www.platypusshoes.com.au/womens-falcon-1-1-1.html'
        }, {
            image: 'https://via.placeholder.com/210',
            price: '$50.00',
            brand: 'Puma',
            name: 'Leadcat Suede',
            link: 'https://www.platypusshoes.com.au/leadcat-suede-1-1.html'
        }, {
            image: 'https://via.placeholder.com/210',
            price: '$109.99',
            brand: 'Superga',
            name: '2750 Cotmetu',
            link: 'https://www.platypusshoes.com.au/2750-cotmetu.html'
        }
    ];

    arrivals.forEach(function(element, index) {
        jQuery('<li><img src="'+element.image+'" ><span class="opt5-brand">'+element.brand+'</span><span class="opt5-name">'+element.name+'</span><span class="opt5-price">'+element.price+'</span></li>').appendTo('.opt5-arrivals ul');
    });

    jQuery('<div class="opt5-viewMore"><a href="https://www.platypusshoes.com.au/shop/new-arrivals">View more</a></div>').appendTo('.opt5-main');


    // BRANDS
    jQuery('<div class="opt5-brands container"></div>').appendTo('.opt5-main');

    jQuery('<div class="container opt-brands"><ul class="slides"></ul></div>').appendTo('.opt5-brands');

    var brandimages = [
        {
            image: "//placehold.it/75x50",
            url: "#"
        },
        {
            image: "//placehold.it/100x50",
            url: "#"
        },
        {
            image: "//placehold.it/80x30",
            url: "#"
        },
        {
            image: "//placehold.it/80x30",
            url: "#"
        },
        {
            image: "//placehold.it/75x50",
            url: "#"
        },
        {
            image: "//placehold.it/100x50",
            url: "#"
        },
        {
            image: "//placehold.it/80x30",
            url: "#"
        },
        {
            image: "//placehold.it/80x30",
            url: "#"
        },
        {
            image: "//placehold.it/75x50",
            url: "#"
        },
        {
            image: "//placehold.it/100x50",
            url: "#"
        },
        {
            image: "//placehold.it/80x30",
            url: "#"
        },
        {
            image: "//placehold.it/80x30",
            url: "#"
        }
    ]

    brandimages.forEach(function (el, i) {
        jQuery('<li><a href="'+el.url+'"><img src="'+el.image+'" ></a></li>').appendTo('.opt5-brands ul');
    });

    /** SHOP INSTAGRAM */
    jQuery('<div class="opt5-shopinstagram container"></div>').appendTo('.opt5-main');

    jQuery('.opt5-shopinstagram').append('<div class="opt-instatitle"><p>SHOP INSTAGRAM</p><p class="opt-smaller">Follow <strong>@PLATYPUS_SNEAKERS</strong></p></div><div class="opt-instagrid"></div>');

    var images = [
        '//placehold.it/170x170',
        '//placehold.it/170x170',
        '//placehold.it/170x170',
        '//placehold.it/170x170'
    ]

    images.forEach(function (el, i) {
        jQuery('.opt-instagrid').append('<img src="'+el+'"/>');
    })

    function getScript(src, callback) {
        var s = document.createElement('script');
        s.src = src;
        s.async = true;
        s.onreadystatechange = s.onload = function() {
            if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
                callback.done = true;
                callback();
            }
        };
        document.querySelector('head').appendChild(s);
    };

    getScript('https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/jquery.flexslider-min.js', slider);
    jQuery('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/flexslider.min.css" />');

    function addFooter(){
        jQuery("body").append('<div class="opt-footer"></div>');
        jQuery(".opt-footer").append('<div class="footer-social small-12"><div class="list"><div class="list-item"> <a class="link" href="https://www.instagram.com/platypus_sneakers/"> <svg xmlns="http://www.w3.org/2000/svg" class="svg-icon instagram"> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-instagram"></use> </svg> </a></div><div class="list-item"> <a class="link" href="https://www.facebook.com/platypusshoes"> <svg xmlns="http://www.w3.org/2000/svg" class="svg-icon facebook"> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-facebook"></use> </svg> </a></div></div></div>'); jQuery(".opt-footer").append('<div class="opt-signup"><div class="block newsletter"><h5 class="title">Get offers direct to you</h5><div class="description"> Sign up to receive must-have style news, updates and promotions before anyone else</div><div class="content"><form class="form subscribe" novalidate="novalidate" action="https://www.platypusshoes.com.au/newsletter/subscriber/new/" method="post" id="newsletter-validate-detail" _lpchecked="1"><div class="field newsletter"><div class="control"> <input name="email" type="email" id="newsletter" class="input" placeholder="Enter your email address for updates" data-validate="{required:true, \'validate-email\':true}" autocomplete="off"></div></div><div class="actions"> <button class="action subscribe inverted" title="Sign up" type="submit"> Sign up </button></div></form></div></div></div>');jQuery(".opt-footer").append('<div class="menu opt-list small-12"> <div class="opt-greenline"></div><div class="opt-accordion"> <div class="opt-title"> <p>HELP AND INFO</p></div><div class="list-elem"> <a class="link" href="https://help.platypusshoes.com.au/hc/en-us" target="_blank">Help &amp; Contact Us</a> </div><div class="list-elem"> <a class="link" href="https://www.platypusshoes.com.au/delivery">Delivery<br></a> </div><div class="list-elem"> <a class="link" href="https://www.platypusshoes.com.au/returns">Returns</a> </div><div class="list-elem"> <a class="link" href="https://www.platypusshoes.com.au/terms-and-conditions">Terms</a> </div><div class="list-elem"> <a class="link" href="https://www.platypusshoes.com.au/privacy-policy">Privacy<br></a> </div><div class="list-elem"> <a class="link" href="https://www.platypusshoes.com.au/afterpay">Afterpay</a> </div><div class="list-elem"> <a class="link" href="https://www.platypusshoes.com.au/size-guide">Size Guide</a> </div></div><div class="opt-greenline"></div><div class="opt-accordion"> <div class="opt-title"> <p>ABOUT PLATYPUS</p></div><div class="list-elem"> <a class="link" href="https://www.platypusshoes.com.au/about-us">About us</a> </div><div class="list-elem"> <a class="link" href="https://www.platypusshoes.com.au/storelocator">Find a Store<br></a> </div><div class="list-elem"> <a class="link" href="https://www.platypusshoes.com.au/careers">Join the Team</a> </div></div><div class="opt-greenline"></div><div class="opt-accordion"> <div class="opt-title"> <p>MORE</p></div><div class="list-elem"> <a class="link" href="https://www.platypusshoes.com.au/student-discount">UNiDAYS</a> </div><div class="list-elem"> <a class="link" href="https://www.platypusshoes.com.au/instagram-wall">Shop Instagram</a> </div><div class="list-elem"> <a class="link" href="https://www.platypusshoes.com.au/blog">Blog<br></a> </div><div class="list-elem"> <a class="link" href="https://www.platypusshoes.com.au/click-collect">Click &amp; Collect<br></a> </div></div><div class="opt-greenline"></div></div>');
        jQuery(".opt-footer").append('<div class="opt-payment-types"><svg xmlns="http://www.w3.org/2000/svg" class="svg-icon americanexpress"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-americanexpress"></use> </svg> <svg xmlns="http://www.w3.org/2000/svg" class="svg-icon visacard"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-visacard"></use> </svg> <svg xmlns="http://www.w3.org/2000/svg" class="svg-icon mastercard"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-mastercard"></use> </svg> <svg xmlns="http://www.w3.org/2000/svg" class="svg-icon paypal"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-paypal"></use> </svg><div> <img class="icon afterpay" src="https://www.platypusshoes.com.au/media/wysiwyg/80xNxafterpay-icon.png.pagespeed.ic.1HgS9W8g2V.png" alt="afterpay" width="80" data-pagespeed-url-hash="3065460486" onload="pagespeed.CriticalImages.checkImageForCriticality(this);"></div></div>');
        jQuery(".opt-footer").append('<div class="opt-footer-logo small-12"><a class="logo" href="https://www.platypusshoes.com.au/" title="Platypus Shoes"><img src="https://www.platypusshoes.com.au/static/version1539767911/frontend/Ewave/platypus/en_AU/images/footer-logo.svg" alt="Platypus Shoes" width="182" height="34" data-pagespeed-url-hash="1088197133" onload="pagespeed.CriticalImages.checkImageForCriticality(this);"></a></div>');
        jQuery(".opt-footer").append('<div class="opt-secure"><small class="copyright"><span>Copyright © 2018 Platypus Shoes. All rights reserved.</span></small></div>');
        jQuery('.opt-title').click(function () {
            jQuery(this).closest('.opt-accordion').toggleClass('opt-show');
        })
    }
    addFooter();

}, '.page-main');