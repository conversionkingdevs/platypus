function showModal () {
    var html = "<div class='opt-overlay'></div><div class='opt-modal-wrap'><div class='opt-modal'> <div class='opt-close'>X</div><div class='opt-modal-content'> <h1>RETURNS</h1> <div id=\"tab-delivery-times\" class=\"data item content\" data-role=\"content\" aria-labelledby=\"tab-delivery-times\" role=\"tabpanel\" aria-hidden=\"false\"><p>If your order is placed before 2pm AEST on a business day (Mon-Fri), it leaves our warehouse the same day! Orders placed after 2pm AEST or on weekends, it will be dispatched the next business day.</p><p>The delivery times below are estimates, and can vary depending on your location.</p><br><table><thead><tr><th>Delivery Option</th><th>Delivery TIME</th><th>Order over $99</th><th>Order under $99</th></tr></thead><tbody><tr><td>Australian Metropolitan Areas</td><td>1-3 business days</td><td style=\"text-align: center;\"><strong>FREE</strong></td><td style=\"text-align: center;\">$10</td></tr><tr><td>Australian Rural Areas</td><td>4-7 business days</td><td style=\"text-align: center;\"><strong>FREE</strong></td><td style=\"text-align: center;\">$10</td></tr></tbody></table></div></div></div>";

    jQuery('body').append(html);
    jQuery('.opt-close,.opt-overlay').click(function () {
        jQuery('.opt-overlay, .opt-modal-wrap').remove();
    });
    jQuery('.opt-modal #tab-delivery-times').after('<button id="optReadMore" class="action tocart primary">READ MORE</button>');
    jQuery('#optReadMore').click(function (e) {
        e.preventDefault();
        window.location = "https://www.platypusshoes.com.au/delivery";
    });
}

function showTitle () {
  var message = 'FAST DELIVERY, PLACE YOUR ORDER NOW';
    
  jQuery('.top-container .list').append('<li class="large-12 medium-12 small-12 optSplit"><img src="//cdn.optimizely.com/img/6092490016/bc1b023ebad448a4bd64a6c9ee198bb8.png">'+message+'</li>');
  jQuery('.top-container .list li').removeClass('large-12 medium-12 small-12').addClass('large-6 medium-6 small-6');

  if (jQuery('.top-container ul li').length == 1) {
      jQuery('.top-container ul li').addClass('optCenter');
  }

  if (jQuery('.header.trust').length >= 0) {
      jQuery('.header.trust').append('<span class="optUnder"><img src="//cdn.optimizely.com/img/6092490016/bc1b023ebad448a4bd64a6c9ee198bb8.png">'+message+'</span>');
  }

  jQuery('.optSplit, .optUnder').click(function () {
      showModal();
  });

}

// function showUnder () {
//     var html = ' <div class="optContainer"> <div class="optTop"> <img src="//placehold.it/28x24"> <p>Simple Returns</p></div><div class="optBottom"> <p>FAST DELIVERY, PLACE YOUR ORDER NOW</p></div></div>';

//     jQuery('.product-add-form').after(html);
//     jQuery('.optContainer').click(function () {
//         showModal();
//     });
// }  

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

function start() {

    // if (jQuery('body').hasClass('catalog-product-view')) {
    //     defer(showUnder, '.product-add-form');
    // }

    showTitle();

}

defer(start, 'body');