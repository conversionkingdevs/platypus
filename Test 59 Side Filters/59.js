
function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector) }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector) }, 50);
    }    
}

function postLoadingScripts(){
    bindFilter();
}
function checkForLoadingMethod(method){
    if(jQuery(".loading-mask").css("display") == "block"){
        setTimeout(function(){
            checkForLoading();
        }, 200);
    } else {

        method();
    }
}
function checkForLoading(){
    if(jQuery(".loading-mask").css("display") == "block"){
        setTimeout(function(){
            checkForLoading();
        }, 200);
    } else {
        postLoadingScripts();
        if(!jQuery("#opt-filter .items").hasClass("opt-binded")){
            setTimeout(function(){
                checkForLoading();
            }, 200);
        }
    }
}

function pxToInt(text){
    return parseInt(text.split("px")[0]);
}

function bindFilter() {
    jQuery("#opt-filter .items").addClass("opt-binded");
    jQuery("#opt-filter .items .item a").click(function(){
        jQuery(".js-apply-filter").first().click();
    });
    jQuery("#opt-filter .swatch-link").click(function(){
        console.log(jQuery(this).parents("size"));
        //jQuery(".js-apply-filter").first().click();
    });
}

function bindFixedFilter(){
    console.log("binding");
    jQuery(".js-apply-filter").click(function(){
        setTimeout(function(){
            checkForLoading();
        },500);
    });
    bindFilter();
}

defer(function(){
    checkForLoadingMethod(function(){
        console.log("run");
        jQuery("body").addClass("opt-59");
            if(jQuery(window).width() > 768){
                jQuery("#opt-filter").append('<div id="opt-afterpay-container"><img src="//cdn.optimizely.com/img/6092490016/70d70cdbb6f84eec896cca6f4f5b89db.png" alt="afterpay"></div>');
                
                jQuery("#amasty-shopby-product-list").prepend('<div id="opt-filter" class="filter"></div>');
                jQuery("#opt-filter").append(jQuery(".block.filter"));
                jQuery("#opt-filter").append(jQuery(".toolbar-products"));
                defer(function(){
                    jQuery("#narrow-by-list .actions.dropdown-container.filter.js-filters-group.-down .js-filters:eq(0)").prepend(jQuery("#narrow-by-list .actions.dropdown-container.filter.js-filters-group.-down .js-filters:eq(0) > .size"));            
                }, "#narrow-by-list .actions.dropdown-container.filter.js-filters-group.-down .js-filters:eq(0) > .size");
                
            }
        });
    //bindFixedFilter();
}, ".block.filter");